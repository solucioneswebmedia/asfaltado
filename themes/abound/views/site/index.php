<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>

<?php if(!(Yii::app()->user->isGuest)) { ?>
<div class="page-header">
  <h1>Alcald&iacute;a de San Crist&oacute;bal - <small>Sistema de Gesti&oacute;n de Asfaltado</small></h1>
</div>

<div class="row-fluid">
    <!-- <p>Bienvenido(a), <?php //echo Yii::app()->user->getNombreCompleto(); ?></p> -->
  
  <div class="span9">
    <?php
      $this->beginWidget('zii.widgets.CPortlet', array(
        'title'=>'<span class="icon-th-large"></span>Asfalto desplegado en el año actual',
        'titleCssClass'=>''
      ));
      ?>
          
          <div class="asfaltado-grafico" style="height: 230px;width:100%;margin-top:15px; margin-bottom:15px;">
            <?php $this->renderPartial('_grafico', true);?>
          </div>
          
    <?php $this->endWidget(); ?>  
  </div>
  <div class="span3">
    <?php
    $this->beginWidget('zii.widgets.CPortlet', array(
      'title'=>"<i class='fa fa-bar-chart'></i> Inventario de Asfalto",
    ));
    
  ?>
    <div class="summary">
      <ul>   
        <?php
          foreach ($inventarios as $inventario) {
        ?>
          <li>
            <span class="summary-icon">
              <img src="<?php echo $baseUrl ;?>/img/assignment.png" width="36" height="36" alt="">    
            </span>
            <span class="summary-number"><?php echo $inventario->cantidad?> ton.</span>
            <span class="summary-title"> <?php echo $inventario->proveedor->nombre?></span> 
          </li>
        <?php  
          }
        ?>
          
      </ul>
    </div>
    <?php $this->endWidget();?>
  </div>
  </div>
  <div class="row-fluid">
    <div class="span3 ">
    <div class="stat-block">
      <ul>
      
      <li class="stat-count"><span>Total Asfalto</span></li>
       <li class="stat-percent"><span class="summary-number"><?php echo $asfaltoAnual; ?> ton.</span></li>
      </ul>
    </div>
    </div>
    <div class="span3 ">
    <div class="stat-block">
      <ul>
      
      <li class="stat-count"><span>Inspecciones Hechas</span></li>
      <li class="stat-percent"><span class="text-info stat-percent"><?php echo $inspeccionAnual; ?></span></li>
      </ul>
    </div>
    </div>
    <div class="span3 ">
    <div class="stat-block">
      <ul>
      
      <li class="stat-count"><span>Obras Terminadas</span></li>
      <li class="stat-percent"><span class="text-success stat-percent"><?php echo $obrasAnual; ?></span></li>
      </ul>
    </div>
    </div>
    <div class="span3 ">
    <div class="stat-block">
      <ul>
      <li></li>
      <li class="stat-count"><span>Ver más estadísticas</span></li>
      <li class="stat-percent"><a href="<?php echo $this->createUrl('//site/datos');?>" target="_self" ><img src="<?php echo $baseUrl ;?>/img/forward.png" width="36" height="36"></a></li>
      </ul>
    </div>
    </div>
  </div>
<?php } 
  if(Yii::app()->user->isGuest) {
?>
  <div class="row-fluid">
  <div class="hero-unit">
  <h1>Alcald&iacute;a de San Crist&oacute;bal</h1> 
  <p>Sistema de Gesti&oacute;n de Asfaltado</p> 
  <br /> 
    <p class="lead">Bienvenido al Sistema de Gestión de Asfaltado.</p>
  	<p>Debes iniciar sesión haciendo <?php echo CHtml::link(CHtml::encode("click aquí"), array('//site/login') ); ?></p>
  <?php } else if (Yii::app()->user->isOficina()){ 
        // Sesion iniciada oficina
      $url=$this->createUrl('//solicitudesInspeccion');
      $this->redirect($url);
      }
  	?>
</div>
</div>