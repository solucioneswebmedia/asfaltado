<script type="text/javascript">
              // document ready function
$(document).ready(function() {

  var divElement = $('div'); //log all div elements
  //------------- Visitor chart -------------//

  $(function () {
    //some data
    //var d1 = [[0,50],[1,23],[2,24],[3,24],[4,35],[5,21],[6,65]];
    //var d2 = [[1, randNum()-5], [2, randNum()-4], [3, randNum()-4], [4, randNum()],[5, 4+randNum()],[6, 4+randNum()],[7, 5+randNum()],[8, 5+randNum()],[9, 6+randNum()],[10, 6+randNum()],[11, 6+randNum()],[12, 2+randNum()],[13, 3+randNum()],[14, 4+randNum()],[15, 4+randNum()],[16, 4+randNum()],[17, 5+randNum()],[18, 5+randNum()],[19, 2+randNum()],[20, 2+randNum()],[21, 3+randNum()],[22, 3+randNum()],[23, 3+randNum()],[24, 2+randNum()],[25, 4+randNum()],[26, 4+randNum()],[27,5+randNum()],[28, 2+randNum()],[29, 2+randNum()], [30, 3+randNum()]];
    //define placeholder class
    var placeholder = $(".asfaltado-grafico");
    //graph options
    var options = {
        grid: {
          show: true,
            aboveData: true,
            color: "#3f3f3f" ,
            labelMargin: 5,
            axisMargin: 0, 
            borderWidth: 0,
            borderColor:null,
            minBorderMargin: 5 ,
            clickable: true, 
            hoverable: true,
            autoHighlight: true,
            mouseActiveRadius: 20
        },
            series: {
              grow: {
                active: false,
                stepMode: "linear",
                steps: 50,
                stepDelay: true
              },
                lines: {
                  show: true,
                  fill: true,
                  lineWidth: 4,
                  steps: false
                  },
                points: {
                  show:true,
                  radius: 5,
                  symbol: "circle",
                  fill: true,
                  borderColor: "#fff"
                }
            },
            legend: { 
              position: "ne", 
              margin: [0,-25], 
              noColumns: 0,
              labelBoxBorderColor: null,
              labelFormatter: function(label, series) {
              // just add some space to labes
              return label+'&nbsp;&nbsp;';
           }
          },
            yaxis: { min: 0 },
            xaxis: {ticks: function(){
                    var arrayGeneral=[ [0, "Ene"],  [1, "Feb"], [2, "Mar"], [3, "Abr"], [4, "May"], [5, "Jun"], [6, "Jul"],  [7, "Ago"],  [8, "Sep"],  [9, "Oct"],  [10, "Nov"],  [11, "Dic"] ];
                    var arrayResult=[];
                    var d = new Date();
                    var n = d.getMonth();
                    if(n<2){ n=2; }
                    for(i=0; i<n+1; i++){
                      arrayResult[i]=arrayGeneral[i];
                    } 
                    return arrayResult;
                  }
              , tickDecimals: 0},
            colors: chartColours,
            shadowSize:1,
            tooltip: true, //activate tooltip
        tooltipOpts: {
          content: "%s : %y.0",
          shifts: {
            x: -30,
            y: -50
          }
        }
        };   
  
          $.plot(placeholder, [ 

            {
              label: "Toneladas de Asfalto", 
              data: <?php 
                $mes=date('n');
                $arrayResult="[[";
                for ($i=0; $i<$mes; $i++ ){
                  $arrayResult.=$i.",";
                  $arrayResult.=PapeletaAsfalto::model()->getCantidadMensual($i+1);
                  if(!($i==$mes-1)){
                    $arrayResult.="],[";  
                  }else{
                    $arrayResult.="]]"; 
                  }
                  
                }
                echo $arrayResult;
              ?>,
              lines: {fillColor: "#f2f7f9"},
              points: {fillColor: "#88bbc8"}
            }, 
            

          ], options);
          
    });
});
</script>