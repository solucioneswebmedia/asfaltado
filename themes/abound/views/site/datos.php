<?php
/* @var $this SiteController */
/* @var $form CActiveForm */
$this->breadcrumbs=array(
	'Estadisticas');

?>

<h1>Estadísticas</h1>
<div class="span3">
	<div class="form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'datos-form',
			'enableAjaxValidation'=>false,
			)); 
		?>

		<div class="row">
			<h4>Fecha inicio:</h4>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'name'=>'fechaInicio',
				'language'=> 'es',
				'value'=>(isset($_POST['fechaInicio']))?$_POST['fechaInicio']:date("d/m/Y"),
				'options'=>array(
					'showAnim'=>'fold',
					),
				'htmlOptions'=>array(
					'style'=>'height:20px;'
					),
					)); 
			?>	
		</div>

		<div class="row">
			<h4>Fecha final:</h4>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',array(
				'name'=>'fechaFin',
				'language'=> 'es',
				'value'=>(isset($_POST['fechaFin']))?$_POST['fechaFin']:date("d/m/Y"),
				'options'=>array(
					'showAnim'=>'fold',
					),
				'htmlOptions'=>array(
					'style'=>'height:20px;'
					),
					)); ?>	
		</div>


		<div class="row buttons">
			<?php echo CHtml::submitButton('Consultar'); ?>
		</div>

		<?php $this->endWidget(); ?>

	</div><!-- form -->
</div>
<?php if($result){
	?>
<div class="span9">
	<div class="portlet">
		<div class="portlet-content">
			<h3>Resultados desde <?php echo $_POST['fechaInicio']; ?> hasta <?php echo $_POST['fechaFin']; ?> </h3>
			<p class="lead">Asfalto desplegado: <?php echo $asfaltoRango; ?> ton.<br/></p>
			<p class="lead">Inspecciones realizadas: <?php echo $inspeccionRango; ?> <br/></p>
			<p class="lead">Obras completadas: <?php echo $obraRango; ?> <br/></p>
			<p class="lead">Total de inversión para obras de administración directa: <?php echo $inversionAdmRango; ?> Bs.<br/></p>
			<p class="lead">Total de inversión para obras de contratadas: <?php echo $inversionContRango; ?> Bs.<br/></p>
		</div>
	</div>
</div>
<?php } ?>
