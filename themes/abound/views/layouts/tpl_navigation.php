<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="<?php echo Yii::app()->baseUrl; ?>">Sistema de Gesti&oacute;n de Asfaltado</a>
          
          <div class="nav-collapse">
			<?php 
            if(Yii::app()->user->isGuest || Yii::app()->user->isOficina()){
                $this->widget('zii.widgets.CMenu',array(
                        'htmlOptions'=>array('class'=>'pull-right nav'),
                        'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
                        'itemCssClass'=>'item-test',
                        'encodeLabel'=>false,
                        'items'=>array(
                            array('label'=>'Inicio', 'url'=>array('/site/index')),                            
                            array('label'=>'Iniciar sesi&oacute;n', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                            array('label'=>'Cerrar sesi&oacute;n de ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                        ),
                    ));
            }else if(Yii::app()->user->isAdministrador() || Yii::app()->user->isJefe()){
                $this->widget('zii.widgets.CMenu',array(
                        'htmlOptions'=>array('class'=>'pull-right nav'),
                        'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
                        'itemCssClass'=>'item-test',
                        'encodeLabel'=>false,
                        'items'=>array(
                            array('label'=>'Inicio', 'url'=>array('/site/index')),
                            //array('label'=>'Gr&aacute;ficos y Diagramas', 'url'=>array('/site/page', 'view'=>'graphs')),
                            //array('label'=>'Figuras', 'url'=>array('/site/page', 'view'=>'forms')),
                            //array('label'=>'Tablas', 'url'=>array('/site/page', 'view'=>'tables')),
                            //array('label'=>'Interfaz Gr&aacute;fica', 'url'=>array('/site/page', 'view'=>'interface')),
                            //array('label'=>'Tipograf&iacute;a', 'url'=>array('/site/page', 'view'=>'typography')),
                            array('label'=>'Acciones', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Solicitudes de inspección', 'url'=>$this->createUrl('//solicitudesInspeccion')),
                                array('label'=>'Inspecciones', 'url'=>$this->createUrl('//inspecciones')),
                                array('label'=>'Trabajos', 'url'=>$this->createUrl('//trabajos')),
                            )),

                            array('label'=>'Materiales', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Material', 'url'=>$this->createUrl('//materiales')),
                                array('label'=>'Papeletas de Asfalto', 'url'=>$this->createUrl('//papeletasAsfalto')),
                                array('label'=>'Compras de Asfalto', 'url'=>$this->createUrl('//comprasAsfalto')),
                                array('label'=>'Asfalto Restante', 'url'=>$this->createUrl('//materialesRestantes')),
                            )),

                            array('label'=>'Datos', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Maquinaria', 'url'=>$this->createUrl('//maquinarias')),
                                array('label'=>'Maquinaria existente', 'url'=>$this->createUrl('//maquinariasExistentes')),
                                array('label'=>'Personal', 'url'=>$this->createUrl('//personal')),
                                array('label'=>'Proveedores', 'url'=>$this->createUrl('//proveedores')),
                                array('label'=>'Unidades', 'url'=>$this->createUrl('//unidades')),
                            )),

                            array('label'=>'Sistema', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Usuarios', 'url'=>$this->createUrl('//usuarios')),
                                array('label'=>'Bitacora', 'url'=>$this->createUrl('//bitacora')),
                                //array('label'=>'Limpiar Base de Datos', 'url'=>$this->createUrl('#')),
                            )),
                            array('label'=>'Iniciar sesi&oacute;n', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                            array('label'=>'Cerrar sesi&oacute;n de ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                        ),
                    ));
                 
            }else{
                $this->widget('zii.widgets.CMenu',array(
                        'htmlOptions'=>array('class'=>'pull-right nav'),
                        'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
    					'itemCssClass'=>'item-test',
                        'encodeLabel'=>false,
                        'items'=>array(
                            array('label'=>'Inicio', 'url'=>array('/site/index')),
                            array('label'=>'Acciones', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                //array('label'=>'Mis Mensajes <span class="badge badge-warning pull-right">26</span>', 'url'=>'#'),
                                array('label'=>'Solicitudes de inspección', 'url'=>$this->createUrl('//solicitudesInspeccion')),
                                array('label'=>'Inspecciones', 'url'=>$this->createUrl('//inspecciones')),
                                array('label'=>'Trabajos', 'url'=>$this->createUrl('//trabajos')),
                            )),
                            
                            array('label'=>'Materiales', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Material', 'url'=>$this->createUrl('//materiales')),
                                array('label'=>'Papeletas de Asfalto', 'url'=>$this->createUrl('//papeletasAsfalto')),
                                array('label'=>'Compras de Asfalto', 'url'=>$this->createUrl('//comprasAsfalto')),
                                array('label'=>'Asfalto Restante', 'url'=>$this->createUrl('//materialesRestantes')),
                            )),

                            array('label'=>'Datos', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Maquinaria', 'url'=>$this->createUrl('//maquinarias')),
                                array('label'=>'Maquinaria existente', 'url'=>$this->createUrl('//maquinariasExistentes')),
                                array('label'=>'Personal', 'url'=>$this->createUrl('//personal')),
                                array('label'=>'Proveedores', 'url'=>$this->createUrl('//proveedores')),
                                array('label'=>'Unidades', 'url'=>$this->createUrl('//unidades')),
                            )),

                            array('label'=>'Iniciar sesi&oacute;n', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                            array('label'=>'Cerrar sesi&oacute;n de ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                        ),
                    ));
                } 
                ?>
    	</div>
    </div>
	</div>
</div>

<!-- <div class="subnav navbar navbar-fixed-top">
    <div class="navbar-inner">
    	<div class="container">
        
        	<!--<div class="style-switcher pull-left">
                <a href="javascript:chooseStyle('none', 60)" checked="checked"><span class="style" style="background-color:#0088CC;"></span></a>
                <a href="javascript:chooseStyle('style2', 60)"><span class="style" style="background-color:#7c5706;"></span></a>
                <a href="javascript:chooseStyle('style3', 60)"><span class="style" style="background-color:#468847;"></span></a>
                <a href="javascript:chooseStyle('style4', 60)"><span class="style" style="background-color:#4e4e4e;"></span></a>
                <a href="javascript:chooseStyle('style5', 60)"><span class="style" style="background-color:#d85515;"></span></a>
                <a href="javascript:chooseStyle('style6', 60)"><span class="style" style="background-color:#a00a69;"></span></a>
                <a href="javascript:chooseStyle('style7', 60)"><span class="style" style="background-color:#a30c22;"></span></a>
          	</div> 
           <form class="navbar-search pull-right" action="">
           	 
           <input type="text" class="search-query span2" placeholder="Search">
           
           </form>
    	</div><!-- container 
    </div><!-- navbar-inner 
</div><!-- subnav -->