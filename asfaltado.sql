/*
SQLyog Community v12.07 (32 bit)
MySQL - 5.5.24-log : Database - asfaltado
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bitacora` */

DROP TABLE IF EXISTS `bitacora`;

CREATE TABLE `bitacora` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tabla` char(25) DEFAULT NULL,
  `registro` char(25) DEFAULT NULL,
  `usuario` char(25) DEFAULT NULL,
  `tipo` char(25) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `bitacora` */

insert  into `bitacora`(`id`,`tabla`,`registro`,`usuario`,`tipo`,`timestamp`) values (1,'inspeccion','12','carlos','Test','2015-07-16 17:26:06'),(2,'solicitud_inspeccion','23','carlos','Modificacion','2015-07-16 17:52:46'),(3,'solicitud_inspeccion',NULL,'demo','Insercion','2015-07-16 17:53:49'),(4,'solicitud_inspeccion','B-4','elohim','Eliminacion','2015-07-16 17:57:21'),(5,'inspecciones','A-001','ernesto','Eliminacion','2015-07-17 17:18:24'),(6,'papeleta_asfalto','003642','carlos','Modificacion','2015-07-20 10:16:32'),(7,'material_restante','1','carlos','Modificacion','2015-07-20 10:26:41'),(8,'material_restante','1','carlos','Modificacion','2015-07-20 10:35:45'),(9,'compra_asfalto',NULL,'carlos','Insercion','2015-07-20 10:35:45'),(10,'material_restante','2','carlos','Modificacion','2015-07-20 10:37:14'),(11,'compra_asfalto',NULL,'carlos','Insercion','2015-07-20 10:37:14'),(12,'material_restante','1','carlos','Modificacion','2015-07-20 10:46:58'),(13,'compra_asfalto','4;1;2015-07-20;135','carlos','Eliminacion','2015-07-20 10:46:58'),(14,'computo_metrico','24;;10;2;2;0.08;;;;','ernesto','Modificacion','2015-07-21 10:57:18'),(15,'computo_metrico','25;;5;3;3;0.08;;;;','ernesto','Modificacion','2015-07-21 10:57:18'),(16,'estimacion_maquinaria','17;;5;3','ernesto','Modificacion','2015-07-21 10:57:18'),(17,'estimacion_maquinaria','18;;2;5','ernesto','Modificacion','2015-07-21 10:57:18'),(18,'estimacion_personal','14;;8;15;3','ernesto','Modificacion','2015-07-21 10:57:19'),(19,'estimacion_personal','15;;7;1;3','ernesto','Modificacion','2015-07-21 10:57:19'),(20,'computo_metrico','24;40;10;2;2;0.08;;;;','ernesto','Modificacion','2015-07-21 11:09:00'),(21,'computo_metrico','25;40;5;3;3;0.08;;;;','ernesto','Modificacion','2015-07-21 11:09:00'),(22,'estimacion_maquinaria','17;40;5;3','ernesto','Modificacion','2015-07-21 11:09:00'),(23,'estimacion_maquinaria','18;40;2;5','ernesto','Modificacion','2015-07-21 11:09:00'),(24,'estimacion_personal','14;40;8;15;3','ernesto','Modificacion','2015-07-21 11:09:00'),(25,'estimacion_personal','15;40;7;1;3','ernesto','Modificacion','2015-07-21 11:09:00'),(26,'usuario','elohim','carlos','Modificacion','2015-07-21 12:10:28'),(27,'computo_metrico','27;42;10;2;2;0.08;;;;','carlos','Modificacion','2015-07-23 09:45:46'),(28,'computo_metrico','6;3;4;0.08;42;;;;;','carlos','Insercion','2015-07-23 09:45:46'),(29,'detalle_trabajo','1;2;5;0.06;2015-07-03;5;','carlos','Insercion','2015-07-23 11:21:57'),(30,'detalle_maquinaria','1;2;5;2;2015-05-11;2','carlos','Modificacion','2015-07-23 11:21:57'),(31,'detalle_maquinaria','1;2015-07-02;;;5;','carlos','Insercion','2015-07-23 11:21:57'),(32,'detalle_personal','1;5;7;1;2015-07-16','carlos','Modificacion','2015-07-23 11:21:57'),(33,'detalle_trabajo','4;3;3;0.06;2015-07-23;12;','carlos','Insercion','2015-07-23 11:25:50'),(34,'detalle_maquinaria','1;2015-07-23;;;12;','carlos','Insercion','2015-07-23 11:25:50'),(35,'detalle_personal','7;1;2015-07-23;12;','carlos','Insercion','2015-07-23 11:25:51'),(36,'detalle_personal','8;15;2015-07-23;12;','carlos','Insercion','2015-07-23 11:25:51'),(37,'estimacion_maquinaria','4;1;1;3','carlos','Modificacion','2015-08-21 07:41:51'),(38,'estimacion_maquinaria','5;1;3;2','carlos','Modificacion','2015-08-21 07:41:51'),(39,'dia_inspeccion','12;1;2015-05-04','carlos','Modificacion','2015-08-21 07:41:51'),(40,'dia_inspeccion','13;1;2015-05-05','carlos','Modificacion','2015-08-21 07:41:51'),(41,'dia_inspeccion','14;1;2015-05-06','carlos','Modificacion','2015-08-21 07:41:52'),(42,'dia_inspeccion','25;1;2015-05-07','carlos','Modificacion','2015-08-21 07:41:52'),(43,'dia_inspeccion','26;1;2015-05-08','carlos','Modificacion','2015-08-21 07:41:52'),(44,'usuario','carlos',NULL,'Modificacion','2015-08-22 15:43:24'),(45,'usuario','carlos',NULL,'Modificacion','2015-08-22 15:46:07');

/*Table structure for table `compra_asfalto` */

DROP TABLE IF EXISTS `compra_asfalto`;

CREATE TABLE `compra_asfalto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) unsigned NOT NULL,
  `fecha` date DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compra_asfalto_FKIndex1` (`proveedor_id`),
  CONSTRAINT `compra_asfalto_ibfk_1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `compra_asfalto` */

insert  into `compra_asfalto`(`id`,`proveedor_id`,`fecha`,`cantidad`) values (1,2,'2015-02-03',1000),(2,1,'2015-03-01',500),(3,1,'2015-03-23',500),(5,2,'2015-07-20',1000);

/*Table structure for table `computo_metrico` */

DROP TABLE IF EXISTS `computo_metrico`;

CREATE TABLE `computo_metrico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `cantidad` int(10) DEFAULT NULL,
  `ancho` double DEFAULT NULL,
  `largo` double DEFAULT NULL,
  `espesor` double DEFAULT NULL,
  `m2` double DEFAULT NULL,
  `m3` double DEFAULT NULL,
  `ton` double DEFAULT NULL,
  `lts` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `computo_metrico_FKIndex1` (`inspecciones_id`),
  CONSTRAINT `computo_metrico_ibfk_1` FOREIGN KEY (`inspecciones_id`) REFERENCES `inspecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

/*Data for the table `computo_metrico` */

insert  into `computo_metrico`(`id`,`inspecciones_id`,`cantidad`,`ancho`,`largo`,`espesor`,`m2`,`m3`,`ton`,`lts`) values (3,2,12,2,2,0.07,NULL,NULL,NULL,NULL),(4,3,5,3,2,0.06,NULL,NULL,NULL,NULL),(5,3,2,1,3,0.06,NULL,NULL,NULL,NULL),(6,18,10,2,2,0.08,NULL,NULL,NULL,NULL),(7,18,15,3,3,0.08,NULL,NULL,NULL,NULL),(8,18,1,9.13,178.5,0.06,NULL,NULL,NULL,NULL),(9,18,1,9.66,44,0.06,NULL,NULL,NULL,NULL),(10,18,1,11.5,100,0.06,NULL,NULL,NULL,NULL),(24,40,10,2,2,0.08,NULL,NULL,NULL,NULL),(25,40,5,3,3,0.08,NULL,NULL,NULL,NULL),(26,41,1,4,4,0.08,NULL,NULL,NULL,NULL),(28,43,1,2,10,0.08,NULL,NULL,NULL,NULL);

/*Table structure for table `detalle_maquinaria` */

DROP TABLE IF EXISTS `detalle_maquinaria`;

CREATE TABLE `detalle_maquinaria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned DEFAULT NULL,
  `trabajo_id` int(10) unsigned NOT NULL,
  `maquinaria_existente_id` int(10) unsigned NOT NULL,
  `dia_trabajo` date DEFAULT NULL,
  `extra` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_maquinaria_FKIndex1` (`maquinaria_existente_id`),
  KEY `detalle_maquinaria_FKIndex2` (`trabajo_id`),
  CONSTRAINT `detalle_maquinaria_ibfk_1` FOREIGN KEY (`maquinaria_existente_id`) REFERENCES `maquinaria_existente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_maquinaria_ibfk_2` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_maquinaria` */

insert  into `detalle_maquinaria`(`id`,`unidades_id`,`trabajo_id`,`maquinaria_existente_id`,`dia_trabajo`,`extra`) values (6,0,1,1,'2015-05-04',NULL),(7,0,1,2,'2015-05-04',NULL),(8,0,2,3,'2015-05-04',NULL),(9,0,1,4,'2015-05-04',NULL),(10,0,1,4,'2015-05-05',NULL),(11,0,2,1,'2015-05-04',NULL),(12,0,2,1,'2015-05-06',NULL),(14,NULL,9,2,'2015-06-26',NULL),(15,NULL,9,3,'2015-06-26',NULL);

/*Table structure for table `detalle_personal` */

DROP TABLE IF EXISTS `detalle_personal`;

CREATE TABLE `detalle_personal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trabajo_id` int(10) unsigned NOT NULL,
  `personal_id` int(10) unsigned NOT NULL,
  `cantidad` int(10) DEFAULT NULL,
  `dia_trabajo` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_personal_FKIndex1` (`personal_id`),
  KEY `detalle_personal_FKIndex2` (`trabajo_id`),
  CONSTRAINT `detalle_personal_ibfk_1` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `detalle_personal_ibfk_2` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_personal` */

insert  into `detalle_personal`(`id`,`trabajo_id`,`personal_id`,`cantidad`,`dia_trabajo`) values (2,1,1,1,'2015-05-04'),(3,1,5,1,'2015-05-04'),(4,1,8,13,'2015-05-04'),(5,1,5,1,'2015-05-05'),(6,1,8,13,'2015-05-05'),(7,2,7,1,'2015-05-04'),(8,2,8,17,'2015-05-04'),(9,2,5,1,'2015-05-06'),(10,2,8,17,'2015-05-06'),(13,9,1,1,'2015-06-26'),(14,9,8,15,'2015-06-26');

/*Table structure for table `detalle_trabajo` */

DROP TABLE IF EXISTS `detalle_trabajo`;

CREATE TABLE `detalle_trabajo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trabajo_id` int(10) unsigned NOT NULL,
  `dia_trabajo` date DEFAULT NULL,
  `cantidad` int(10) DEFAULT NULL,
  `ancho` double DEFAULT NULL,
  `largo` double DEFAULT NULL,
  `espesor` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_trabajo_FKIndex1` (`trabajo_id`),
  CONSTRAINT `detalle_trabajo_ibfk_1` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `detalle_trabajo` */

insert  into `detalle_trabajo`(`id`,`trabajo_id`,`dia_trabajo`,`cantidad`,`ancho`,`largo`,`espesor`) values (1,1,'2015-05-04',3,11,12,0.06),(2,1,'2015-05-04',2,2,2,0.05),(3,2,'2015-05-04',5,2,3,0.06),(7,1,'2015-05-05',4,3,3,0.06),(8,2,'2015-05-06',4,4,4,0.07),(11,9,'2015-06-26',10,5,5,0.08);

/*Table structure for table `dia_inspeccion` */

DROP TABLE IF EXISTS `dia_inspeccion`;

CREATE TABLE `dia_inspeccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dia_inspeccion_FKIndex1` (`inspecciones_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `dia_inspeccion` */

insert  into `dia_inspeccion`(`id`,`inspecciones_id`,`fecha`) values (9,3,'2015-04-15'),(10,3,'2015-04-16'),(11,3,'2015-04-18'),(12,1,'2015-05-04'),(13,1,'2015-05-05'),(14,1,'2015-05-06'),(16,2,'2015-05-05'),(17,2,'2015-05-06'),(20,15,'2015-05-04'),(21,15,'2015-05-05'),(22,15,'2015-05-06'),(23,15,'2015-05-07'),(24,15,'2015-05-08'),(25,1,'2015-05-07'),(26,1,'2015-05-08');

/*Table structure for table `estimacion_maquinaria` */

DROP TABLE IF EXISTS `estimacion_maquinaria`;

CREATE TABLE `estimacion_maquinaria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `maquinaria_id` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estimacion_maquinaria_FKIndex1` (`inspecciones_id`),
  KEY `estimacion_maquinaria_FKIndex3` (`maquinaria_id`),
  CONSTRAINT `estimacion_maquinaria_ibfk_1` FOREIGN KEY (`inspecciones_id`) REFERENCES `inspecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `estimacion_maquinaria_ibfk_2` FOREIGN KEY (`maquinaria_id`) REFERENCES `maquinaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `estimacion_maquinaria` */

insert  into `estimacion_maquinaria`(`id`,`inspecciones_id`,`maquinaria_id`,`cantidad`) values (4,1,1,3),(5,1,3,2),(6,3,4,11),(7,3,1,6),(8,18,3,15),(9,18,7,15),(10,18,5,15),(11,18,8,15),(12,18,4,15),(17,40,5,3),(18,40,2,5),(19,41,5,1),(20,41,3,1),(21,41,4,1),(24,43,2,1),(25,43,5,1);

/*Table structure for table `estimacion_personal` */

DROP TABLE IF EXISTS `estimacion_personal`;

CREATE TABLE `estimacion_personal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `personal_id` int(10) unsigned NOT NULL,
  `cantidad` int(10) unsigned DEFAULT NULL,
  `num_dias` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estimacion_personal_FKIndex1` (`inspecciones_id`),
  KEY `estimacion_personal_FKIndex2` (`personal_id`),
  CONSTRAINT `estimacion_personal_ibfk_1` FOREIGN KEY (`inspecciones_id`) REFERENCES `inspecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `estimacion_personal_ibfk_2` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `estimacion_personal` */

insert  into `estimacion_personal`(`id`,`inspecciones_id`,`personal_id`,`cantidad`,`num_dias`) values (4,3,1,2,NULL),(5,3,4,2,NULL),(6,18,5,1,15),(9,18,6,1,15),(10,18,7,1,15),(11,18,9,2,15),(12,18,8,17,15),(14,40,8,15,3),(15,40,7,1,3),(16,41,1,1,1),(17,41,8,16,1),(18,41,9,1,1),(20,43,7,1,1),(21,43,8,15,1);

/*Table structure for table `foto_inspeccion` */

DROP TABLE IF EXISTS `foto_inspeccion`;

CREATE TABLE `foto_inspeccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `ruta` char(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `foto_inspeccion_FKIndex1` (`inspecciones_id`),
  CONSTRAINT `foto_inspeccion_ibfk_1` FOREIGN KEY (`inspecciones_id`) REFERENCES `inspecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `foto_inspeccion` */

insert  into `foto_inspeccion`(`id`,`inspecciones_id`,`ruta`) values (1,56,'images/inspeccion/INS_F002_20150630_160759.jpg'),(2,56,'images/inspeccion/INS_F002_20150630_160832.jpg'),(3,56,'images/inspeccion/INS_F002_20150705_193649.jpg'),(19,40,'images/inspeccion/onclients.png'),(20,18,'images/inspeccion/2015-05-18 14.19.51.jpg'),(21,18,'images/inspeccion/INS_F001_20150814_075720.jpg');

/*Table structure for table `foto_trabajo` */

DROP TABLE IF EXISTS `foto_trabajo`;

CREATE TABLE `foto_trabajo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trabajo_id` int(10) unsigned NOT NULL,
  `ruta` char(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `foto_trabajo_FKIndex1` (`trabajo_id`),
  CONSTRAINT `foto_trabajo_ibfk_1` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `foto_trabajo` */

insert  into `foto_trabajo`(`id`,`trabajo_id`,`ruta`) values (2,1,'images/trabajo/TRA_014-15_20150817_190053.jpg'),(3,1,'images/trabajo/TRA_014-15_20150817_190117.jpg'),(4,1,'images/trabajo/TRA_014-15_20150817_190158.jpg');

/*Table structure for table `inspecciones` */

DROP TABLE IF EXISTS `inspecciones`;

CREATE TABLE `inspecciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(10) unsigned NOT NULL,
  `numero` char(10) NOT NULL,
  `solicitud_inspeccion_id` int(10) unsigned DEFAULT '0',
  `parroquia` char(30) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `asunto` char(200) DEFAULT NULL,
  `tipo_obra` char(50) NOT NULL,
  `problematica` char(200) DEFAULT NULL,
  `descripcion` char(200) DEFAULT NULL,
  `recomendaciones` char(200) DEFAULT NULL,
  `pto_ref` char(100) DEFAULT NULL,
  `material_bacheo` char(50) DEFAULT NULL,
  `croquis` char(100) DEFAULT NULL,
  `tipo_adm` char(30) DEFAULT NULL,
  `latitud` char(100) DEFAULT NULL,
  `longitud` char(100) DEFAULT NULL,
  `semana` int(10) unsigned DEFAULT '0',
  `cuadrilla` int(11) DEFAULT NULL,
  `realizada` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `inspecciones_unique1` (`numero`),
  KEY `inspeccion_FKIndex2` (`usuario_id`),
  CONSTRAINT `inspecciones_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

/*Data for the table `inspecciones` */

insert  into `inspecciones`(`id`,`usuario_id`,`numero`,`solicitud_inspeccion_id`,`parroquia`,`fecha`,`asunto`,`tipo_obra`,`problematica`,`descripcion`,`recomendaciones`,`pto_ref`,`material_bacheo`,`croquis`,`tipo_adm`,`latitud`,`longitud`,`semana`,`cuadrilla`,`realizada`) values (0,1,'A-099',24,'SAN JUAN BAUTISTA','2015-07-12',NULL,'BACHEO SUPERFICIAL',NULL,'REPARACION DE VIA CON BACHES','','','','','ADMINISTRACION DIRECTA','','',21,2,0),(1,1,'A-006',7,'SAN JUAN BAUTISTA','2015-05-04',NULL,'Trabajos Unidad TP4',NULL,'COLOCACIÓN DE CARPETA CORRIDA','ccccc','ddddd','Asfalto caliente','','ADMINISTRACION DIRECTA','','',21,NULL,0),(2,1,'A-002',2,'SAN JUAN BAUTISTA','2015-03-06',NULL,'Asfaltado',NULL,'Lorem ipsum ad his scripta blandit partiendo.','Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu.','Qui ut wisi vocibus.','Asfalto caliente','','ADMINISTRACION DIRECTA','-1',NULL,20,2,0),(3,1,'A-004',9,'SAN JUAN BAUTISTA','2015-02-05',NULL,'',NULL,'','','','','','ADMINISTRACION DIRECTA','',NULL,20,3,1),(4,1,'A-005',5,'San Juan Bautista','2015-03-24',NULL,'',NULL,'','','','','','','',NULL,20,2,1),(15,1,'c1',9,'SAN JUAN BAUTISTA','2015-04-07',NULL,'',NULL,'Trabajo para cuadrilla 2','','','','','ADMINISTRACION DIRECTA','',NULL,20,3,1),(17,1,'d1',3,'San Juan Bautista','2014-04-11',NULL,'Bacheo profundo',NULL,'','','','','','','',NULL,20,3,1),(18,1,'1',8,'SAN JUAN BAUTISTA','2015-04-20','Inspeccion referente a pavimento flexible (asfalto caliente)','BACHEO PROFUNDO, SUPERFICIAL Y CARPETA CORRIDA','PRESENTA DETERIORO EN LA CAPA ASFALTICA FALLAS COMO BACHES, DISGREGACION Y PIEL DE COCODRILO ESTO PRODUCTO AL TIEMPO DE DURAVILIDAD EN EL MATERIAL ASFALTICO','REALIZAR LOS DIFERENTES BACHES Y APLICAR UNA CAPA DE BACHEO SUPERFICAL PARA EL MEJOR ACABADO Y LA CONSTRUCCION DE CARPETA CORRIDA','','','Asfalto caliente','images/inspeccion/INS_F003_20150629_093813.jpg','ADMINISTRACION DIRECTA','7.77626199676969','-72.2412010654807',20,2,1),(31,1,'D01',17,'SAN JUAN BAUTISTA','2015-06-21','INSPECCI?N DE FALLA','BACHEO','BACHES','OBRA DE BACHEO','LOREM IPSUM','','ASFALTO FRIO',NULL,'ADMINISTRACION DIRECTA','','',21,4,1),(40,3,'B-2',9,'FRANCISCO ROMERO LOBO','2015-06-22','ALEA IACTA EST','BACHEO PROFUNDO','VIA CON BACHES','TRABAJOS A REALIZAR EN LA AV LOS AGUSTINOS','REEMPLAZAR CAPA ASF?LTICA','NINGUNO','ASFALTO CALIENTE','','ADMINISTRACION DIRECTA','','',21,3,1),(41,1,'C99',18,'SAN JUAN BAUTISTA','2015-06-22','INSPECCION DE BACHE','BACHEO SUPERFICIAL','IPSUM','LOREM','GENS','','ASFALTO FRIO',NULL,'ADMINISTRACION DIRECTA','7.77626199676969','-72.2412010654807',21,3,1),(43,1,'F001',20,'PEDRO MARIA MORANTES','2015-06-26','ASFALTADO RESTANTE','BACHEO SUPERFICIAL','','LOREM IPSUM','','','ASFALTO CALIENTE',NULL,'ADMINISTRACION DIRECTA','7.7647148119818','-72.2138200700283',0,NULL,1),(54,1,'F003',21,'SAN JUAN BAUTISTA','2015-06-28','INSPECCION DE BACHES','BACHEO SUPERFICIAL','','','','','ASFALTO CALIENTE','images/inspeccion/INS_F003_20150629_093813.jpg','ADMINISTRACION DIRECTA','7.77518070741536','-72.2466127574444',0,NULL,1),(56,1,'F002',22,'FRANCISCO ROMERO LOBO','2015-06-28','INSPECCION DE FALLA DE BORDE','RECONSTRUCCION DE VIA','','REALIZAR LOS DIFERENTES BACHES Y APLICAR UNA CAPA DE BACHEO SUPERFICAL PARA EL MEJOR ACABADO Y LA CONSTRUCCION DE CARPETA CORRIDA','','','ASFALTO CALIENTE','images/inspeccion/INS_F002_20150630_161928.jpg','ADMINISTRACION DIRECTA','7.7647148119818','-72.2138200700283',22,NULL,1);

/*Table structure for table `maquinaria` */

DROP TABLE IF EXISTS `maquinaria`;

CREATE TABLE `maquinaria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `codigo` char(20) DEFAULT NULL,
  `descripcion` char(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maquinaria_general_FKIndex1` (`unidades_id`),
  CONSTRAINT `maquinaria_ibfk_1` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `maquinaria` */

insert  into `maquinaria`(`id`,`unidades_id`,`codigo`,`descripcion`,`precio`) values (1,1,'MAQ001','Retroexcavadora',7500),(2,1,'MAQ002','Payloader',6250),(3,1,'MAQ003','Vibrocompactadora',8000),(4,1,'MAQ004','Finisher',9000),(5,1,'MAQ005','Camion 350',1066),(6,1,'MAQ006','Camion 8000',1296),(7,1,'MAQ007','Compresor',2500),(8,1,'MAQ008','Camion 750',860),(9,1,'MAQ009','Minishovel',5000);

/*Table structure for table `maquinaria_existente` */

DROP TABLE IF EXISTS `maquinaria_existente`;

CREATE TABLE `maquinaria_existente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `codigo` char(20) DEFAULT NULL,
  `descripcion` char(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `extra` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maquinaria_existente_FKIndex1` (`unidades_id`),
  CONSTRAINT `maquinaria_existente_ibfk_1` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `maquinaria_existente` */

insert  into `maquinaria_existente`(`id`,`unidades_id`,`codigo`,`descripcion`,`precio`,`extra`) values (1,1,'MAQE001','Camión 350 Placa AA123ZZ',2500,1),(2,1,'MAQE002','Retroexcavador JCB 3C',7000,0),(3,1,'MAQE003','Retroexcavador CAT 416',7000,0),(4,1,'MAQE004','Vibrocompactador 10 ton.',5000,0);

/*Table structure for table `material_restante` */

DROP TABLE IF EXISTS `material_restante`;

CREATE TABLE `material_restante` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) unsigned NOT NULL,
  `cantidad` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `asfalto_restante_FKIndex1` (`proveedor_id`),
  CONSTRAINT `material_restante_ibfk_1` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `material_restante` */

insert  into `material_restante`(`id`,`proveedor_id`,`cantidad`) values (1,1,1227),(2,2,1865),(3,3,900);

/*Table structure for table `materiales` */

DROP TABLE IF EXISTS `materiales`;

CREATE TABLE `materiales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proveedor_id` int(10) unsigned NOT NULL,
  `descripcion` char(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materiales_FKIndex1` (`proveedor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `materiales` */

insert  into `materiales`(`id`,`proveedor_id`,`descripcion`,`precio`) values (1,1,'Asfalto Caliente (ton)',980),(2,2,'Asfalto Caliente (ton)',1081),(3,4,'ASFALTO LIQUIDO RC250',9);

/*Table structure for table `papeleta_asfalto` */

DROP TABLE IF EXISTS `papeleta_asfalto`;

CREATE TABLE `papeleta_asfalto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trabajo_id` int(10) unsigned NOT NULL,
  `proveedor_id` int(10) unsigned NOT NULL,
  `codigo` char(10) NOT NULL,
  `fecha` date DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `procedencia` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `papeleta_asfalto_FKIndex1` (`trabajo_id`),
  KEY `papeleta_asfalto_FKIndex2` (`proveedor_id`),
  CONSTRAINT `papeleta_asfalto_ibfk_1` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `papeleta_asfalto_ibfk_2` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `papeleta_asfalto` */

insert  into `papeleta_asfalto`(`id`,`trabajo_id`,`proveedor_id`,`codigo`,`fecha`,`cantidad`,`procedencia`) values (1,1,1,'003642','2015-05-02',56,1),(3,2,2,'00456','2015-02-27',35,2),(4,2,3,'09876','2015-03-18',10,2),(6,1,4,'123456','2015-05-18',70,1),(7,9,2,'000123','2015-06-25',35,3),(8,9,1,'000124','2015-06-26',28,1),(9,11,1,'1','2015-06-26',28,1),(10,1,1,'456','2015-07-13',10,1),(11,1,1,'1234564789','2015-07-13',50,1),(12,2,4,'33452','2015-07-01',300,1);

/*Table structure for table `personal` */

DROP TABLE IF EXISTS `personal`;

CREATE TABLE `personal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidades_id` int(10) unsigned NOT NULL,
  `codigo` char(50) DEFAULT NULL,
  `descripcion` char(100) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `personal_FKIndex2` (`unidades_id`),
  CONSTRAINT `personal_ibfk_1` FOREIGN KEY (`unidades_id`) REFERENCES `unidades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `personal` */

insert  into `personal`(`id`,`unidades_id`,`codigo`,`descripcion`,`precio`) values (1,1,'PERS001','Inspector de Obra',248.95),(2,1,'PERS002','Cuadrilla 1',1170.34),(3,1,'PERS003','Cuadrilla 2',1170.34),(4,1,'PERS004','Cuadrilla TP4',1210.3),(5,1,'PERS005','Caporal',192.96),(6,1,'PER006','Fiscal de Obra',150),(7,1,'PER007','Ingeniero',230),(8,1,'PER008','Obrero(s)',186.44),(9,1,'PER009','Operador(es)',187);

/*Table structure for table `presupuesto` */

DROP TABLE IF EXISTS `presupuesto`;

CREATE TABLE `presupuesto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trabajo_id` int(10) unsigned NOT NULL,
  `nombre_partida` char(20) DEFAULT NULL,
  `monto` float DEFAULT NULL,
  `tipo` char(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `presupuesto_FKIndex1` (`trabajo_id`),
  CONSTRAINT `presupuesto_ibfk_1` FOREIGN KEY (`trabajo_id`) REFERENCES `trabajo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `presupuesto` */

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id`,`nombre`) values (1,'CAIMTA'),(2,'PAVISUR'),(3,'PAVITACA'),(4,'ASFADIL');

/*Table structure for table `rol` */

DROP TABLE IF EXISTS `rol`;

CREATE TABLE `rol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_usuario` char(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `rol` */

insert  into `rol`(`id`,`tipo_usuario`) values (1,'Administrador'),(2,'Jefe'),(3,'Coordinador'),(4,'Oficina');

/*Table structure for table `solicitud_inspeccion` */

DROP TABLE IF EXISTS `solicitud_inspeccion`;

CREATE TABLE `solicitud_inspeccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `numero` char(10) NOT NULL,
  `nombre_solicitante` char(30) DEFAULT NULL,
  `direccion` char(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  `telefono_contacto` char(15) DEFAULT NULL,
  `procedencia` text,
  `semana` int(10) unsigned DEFAULT '0',
  `prioridad` date DEFAULT NULL,
  `realizada` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `solicitud_inspeccion_unique1` (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `solicitud_inspeccion` */

insert  into `solicitud_inspeccion`(`id`,`numero`,`nombre_solicitante`,`direccion`,`fecha`,`fecha_registro`,`telefono_contacto`,`procedencia`,`semana`,`prioridad`,`realizada`) values (1,'A-001','Andres Almenar','Av. Libertador, frente a Corpoelec','2015-02-01','2015-06-01','02763424243','DMV-012-15',17,'2015-04-20',1),(2,'A-002','Blas Borjas','Av. Carabobo, calle 10','2015-02-04','2015-06-01','04167778899','S/N',16,'2015-04-21',1),(3,'A-003','Carol Colmenares','Av. Ppal de Pueblo Nuevo','2015-02-21','2015-06-01','02763434569','OAP-015',16,'2015-04-22',1),(4,'A-004','Daniel Duran','Pasaje Cumana, con calle 13','2015-02-06','2015-06-02','04145556677','S/N',16,'2015-04-23',1),(5,'A-005','Emilio Espina','La Castra, diagonal al Saime','2015-03-24','2015-06-02','04125678899','S/N',16,'2015-04-23',1),(7,'A-006','Fernando Flores','La Ermita, calle 13 entre carrera 4 y 5ta av.','2015-03-24','2015-06-02','02763445566','OAP-015-008',22,'2015-05-25',0),(8,'B-1','ALCALDESA','URBANIZACION LAS ACACIAS, CALLE 3 CARRERAS 4 Y 5','2015-01-30','2015-06-03','','S/N',22,'2015-05-29',0),(9,'B-2','GERENCIA','AVENIDA LOS AGUSTINOS SENTIDO SUR NORTE','2015-01-09','2015-06-03','','',23,'2015-06-01',0),(10,'B-3','c',NULL,'2015-01-01','2014-05-01',NULL,NULL,23,'2015-06-02',0),(12,'A-010','Gabriel Gonzalez','Puente Real, calle 9 frente al estadio','2015-06-05','2015-06-25','02763441122','OAP-015-010',22,'2015-05-26',0),(13,'A-020','Carlos Mendez','AV PRINCIPAL DE PUEBLO NUEVO','2015-07-01','2015-07-01','04126667788','OAP-015-099',26,'2015-07-02',1),(17,'D01',NULL,'CALLE 9 FRENTE A CANCHA DE PUENTE REAL','2015-01-01','2015-01-01',NULL,NULL,0,NULL,0),(18,'C99',NULL,'AVENIDA MARGINAL, BAJO EL ELEVADO DE PUENTE REAL','2015-01-01','2015-01-01',NULL,NULL,23,'2015-06-04',0),(19,'D-001','Luis','Pueblo nuevo','2015-06-23','2015-06-23','04123334455','OAP',0,NULL,0),(20,'F001',NULL,'LAS ACACIAS CALLE 3','2015-01-01','2015-01-01',NULL,NULL,0,NULL,1),(21,'F003',NULL,'VIA HACIA EL MIRADOR, FRENTE A ENTRADA HACIA PERICOS','2015-01-01','2015-01-01',NULL,NULL,0,NULL,1),(22,'F002',NULL,'CHORRO DEL INDIO','2015-01-01','2015-01-01',NULL,NULL,0,NULL,1),(23,'C-001','Carlos Mendez','Av. Carabobo, esquina con carrera 16','2015-07-10','2015-07-10','04166667788','',0,NULL,1),(24,'A-099','Juan Jimenez','Puente Real','2015-07-16','2015-07-16','','',22,'2015-05-27',0);

/*Table structure for table `trabajo` */

DROP TABLE IF EXISTS `trabajo`;

CREATE TABLE `trabajo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_id` int(10) unsigned NOT NULL,
  `numero` char(10) NOT NULL,
  `inspecciones_id` int(10) unsigned NOT NULL,
  `proveedor_id` int(10) unsigned DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `asunto` char(200) DEFAULT NULL,
  `denominacion` text,
  `direccion` char(200) DEFAULT NULL,
  `problematica` char(200) DEFAULT NULL,
  `tipo_reparacion` char(200) DEFAULT NULL,
  `descripcion` char(200) DEFAULT NULL,
  `beneficiario` char(100) DEFAULT NULL,
  `observaciones` char(200) DEFAULT NULL,
  `inversion` double DEFAULT NULL,
  `suministro` double DEFAULT NULL,
  `transporte` double DEFAULT NULL,
  `colocacion` double DEFAULT NULL,
  `realizada` tinyint(1) DEFAULT '0',
  `semana` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trabajo_unique1` (`numero`),
  KEY `trabajo_FKIndex1` (`inspecciones_id`),
  KEY `trabajo_FKIndex2` (`usuario_id`),
  KEY `trabajo_FKIndex3` (`proveedor_id`),
  CONSTRAINT `trabajo_ibfk_1` FOREIGN KEY (`inspecciones_id`) REFERENCES `inspecciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trabajo_ibfk_2` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trabajo_ibfk_3` FOREIGN KEY (`proveedor_id`) REFERENCES `proveedor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `trabajo` */

insert  into `trabajo`(`id`,`usuario_id`,`numero`,`inspecciones_id`,`proveedor_id`,`fecha`,`fecha_fin`,`asunto`,`denominacion`,`direccion`,`problematica`,`tipo_reparacion`,`descripcion`,`beneficiario`,`observaciones`,`inversion`,`suministro`,`transporte`,`colocacion`,`realizada`,`semana`) values (1,3,'029-14',1,1,'2015-05-04','2015-05-02',' TRABAJOS DE ASFALTADO EN ZONAS CRITICAS','Mantenimiento y rehabilitación de la capa asfáltica de rodamiento en las diferentes vias del municipio.','Av. Libertador, frente a Corpoelec','DESGASTE EN LA CAPA ASFALTICA PRODUCTO DEL DESGASTE  EN EL AGREGADO ASFALTICO, ASI MISMO PRESENTA FALLAS DE AHUELLAMIENTO DEPRECIONES Y PIEL DE COCODRILO','BACHEO SUPERFICIAL Y PROFUNDO EN LAS ZONAS MAS AFECTADAS','LA REMOCION DE LA CAPA ASFALTICA EN SECTORES MAS AFECTADOS PARA SU POSTERIOR REPOSICION Y COMPACTACION DEL PAVIMENTO FLEXIBLE','Avenida Libertador','Obra culminada',22599.44,NULL,NULL,NULL,1,NULL),(2,1,'034-14',2,3,'2015-05-04','2015-05-06',NULL,'REPARACION DE FALLA DE BORDE','Condate redonum et armorica','Aenean commodo ligula eget dolor','','Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','COMUNIDAD LOMA DE PIO','Ninguna',23225.92,0,0,0,1,NULL),(9,1,'10-015',56,NULL,'2015-06-23',NULL,'TRABAJOS DE BACHEO EN LAS VIAS DEL MCPIO','REHABILITACIÓN DE LA CAPA ASFALTICA','AVENIDA LOS AGUSTINOS SENTIDO SUR NORTE','NUMEROSOS BACHES','ASFALTADO SUPERFICIAL','','VECINOS LA ERMITA','',NULL,NULL,NULL,NULL,1,NULL),(11,1,'1-015',3,NULL,'2015-06-23',NULL,'TRABAJOS DE BACHEO EN LAS AVENIDAS','BACHEO EN AMBOS CANALES DE AVENIDA LOS AGUSTINOS','AVENIDA LOS AGUSTINOS SENTIDO SUR NORTE','NUMEROSOS BACHES','ASFALTADO SUPERFICIAL','','COMUNIDAD AV LOS AGUSTINOS','',NULL,NULL,NULL,NULL,1,NULL);

/*Table structure for table `unidades` */

DROP TABLE IF EXISTS `unidades`;

CREATE TABLE `unidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_unidad` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `unidades` */

insert  into `unidades`(`id`,`nombre_unidad`) values (1,'Dia(s)'),(2,'m^2'),(3,'m^3');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` char(15) NOT NULL,
  `rol_id` int(10) unsigned NOT NULL,
  `pass` varchar(80) DEFAULT NULL,
  `cedula` char(12) DEFAULT NULL,
  `nombres` char(50) DEFAULT NULL,
  `apellidos` char(50) DEFAULT NULL,
  `civ` char(10) DEFAULT NULL,
  `token` char(80) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`usuario`),
  KEY `usuario_FKIndex1` (`rol_id`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `usuario` */

insert  into `usuario`(`id`,`usuario`,`rol_id`,`pass`,`cedula`,`nombres`,`apellidos`,`civ`,`token`,`date`) values (1,'carlos',1,'dc599a9972fde3045dab59dbd1ae170b','V17877042','Carlos','Mendez',NULL,'1ea3f6f011c8d708bc7f2c7f9297c1f8','2015-08-23'),(2,'elohim',2,'e10adc3949ba59abbe56e057f20f883e','11223344','Elohim','Maldonado','',NULL,NULL),(3,'ernesto',3,'8f91bdb4de0142710ac1718345b96308','19.975.423','Ernesto','Vega','230.568','d1e5c0843ba3a42372795a16e0efaae8','2015-06-22'),(5,'demo',4,'fe01ce2a7fbac8fafaed7c982a04e229','12345','Cuenta Demo','Sistema',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
