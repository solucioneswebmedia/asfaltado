<?php
/* @var $this FotosTrabajoController */
/* @var $model FotoTrabajo */

$this->breadcrumbs=array(
	'Fotos de Trabajos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nueva Foto de Trabajo', 'url'=>array('create')),
	array('label'=>'Ver Foto de Trabajo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Fotos de Trabajo', 'url'=>array('index')),
);
?>

<h1>Actualizar Foto de Trabajo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>