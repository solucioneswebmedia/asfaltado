<?php
/* @var $this FotosTrabajoController */
/* @var $model FotoTrabajo */

$this->breadcrumbs=array(
	'Fotos de Trabajos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva Foto de Trabajo', 'url'=>array('create')),
	array('label'=>'Actualizar Foto de Trabajo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Foto de Trabajo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar esta foto?')),
	array('label'=>'Administrar Fotos de Trabajo', 'url'=>array('index')),
);
?>

<h1>Ver Foto de Trabajo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'trabajo_id',
		'ruta',
	),
)); ?>
