<?php
/* @var $this FotosTrabajoController */
/* @var $model FotoTrabajo */

$this->breadcrumbs=array(
	'Fotos de Trabajos'=>array('index'),
	'Nueva',
);

$this->menu=array(
	array('label'=>'Administrar Fotos de Trabajo', 'url'=>array('index')),
);
?>

<h1>Nueva Foto de Trabajo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>