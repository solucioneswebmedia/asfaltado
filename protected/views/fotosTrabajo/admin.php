<?php
/* @var $this FotosTrabajoController */
/* @var $model FotoTrabajo */

$this->breadcrumbs=array(
	'Fotos de Trabajos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Foto de Trabajo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#foto-trabajo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Fotos de Trabajos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'foto-trabajo-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'trabajo_id',
		'ruta',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
