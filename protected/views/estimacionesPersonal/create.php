<?php
/* @var $this EstimacionesPersonalController */
/* @var $model EstimacionPersonal */

$this->breadcrumbs=array(
	'Estimaciones de Personal'=>array('index'),
	'Nueva',
);

$this->menu=array(
	array('label'=>'Administrar Estimaciones de Personal', 'url'=>array('index')),
);
?>

<h1>Nueva Estimaci&oacute;n de Personal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>