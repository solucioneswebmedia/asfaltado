<?php
/* @var $this EstimacionesPersonalController */
/* @var $model EstimacionPersonal */

$this->breadcrumbs=array(
	'Estimaciones de Personal'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Crear Estimaci&oacute;n de Personal', 'url'=>array('create')),
	array('label'=>'Actualizar Estimaci&oacute;n de Personal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Estimaci&oacute;n de Personal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Estimaciones de Personal', 'url'=>array('index')),
);
?>

<h1>Ver Estimaci&oacute;n de Personal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'inspecciones_id',
		'personal_id',
		'cantidad',
	),
)); ?>
