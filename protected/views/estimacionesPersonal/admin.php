<?php
/* @var $this EstimacionesPersonalController */
/* @var $model EstimacionPersonal */

$this->breadcrumbs=array(
	'Estimaciones de Personal'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Estimaci&oacute;n de Personal', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estimacion-personal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Estimaciones de Personal</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'estimacion-personal-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'inspecciones_id',
		'personal_id',
		'cantidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
