<?php
/* @var $this EstimacionesPersonalController */
/* @var $model EstimacionPersonal */

$this->breadcrumbs=array(
	'Estimaciones de Personal'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nueva Estimaci&oacute;n de Personal', 'url'=>array('create')),
	array('label'=>'Ver Estimaci&oacute;n de Personal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Estimaciones de Personal', 'url'=>array('index')),
);
?>

<h1>Actualizar Estimaci&oacute;n de Personal <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>