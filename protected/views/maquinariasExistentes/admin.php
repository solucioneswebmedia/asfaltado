<?php
/* @var $this MaquinariasExistentesController */
/* @var $model MaquinariaExistente */

$this->breadcrumbs=array(
	'Maquinarias Existentes'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Maquinaria Existente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#maquinaria-existente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Maquinaria Existente</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'maquinaria-existente-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
		array(
            'name'=>'extra',
            'header'=>'Extra',
            'filter'=>array('1'=>'Si','0'=>'No'),
            'value'=>'($data->extra=="1")?("Si"):("No")'), 
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
