<?php
/* @var $this MaquinariasExistentesController */
/* @var $model MaquinariaExistente */

$this->breadcrumbs=array(
	'Maquinarias Existentes'=>array('index'),
	'Nueva',
);

$this->menu=array(
	array('label'=>'Administrar Maquinaria Existente', 'url'=>array('index')),
);
?>

<h1>Nueva Maquinaria Existente</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>