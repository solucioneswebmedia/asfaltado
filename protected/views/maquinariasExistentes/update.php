<?php
/* @var $this MaquinariasExistentesController */
/* @var $model MaquinariaExistente */

$this->breadcrumbs=array(
	'Maquinarias Existentes'=>array('index'),
	$model->codigo=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Maquinaria Existente', 'url'=>array('create')),
	array('label'=>'Ver Maquinaria Existente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Maquinarias Existentes', 'url'=>array('index')),
);
?>

<h1>Actualizar Maquinaria Existente <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>