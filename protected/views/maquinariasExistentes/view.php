<?php
/* @var $this MaquinariasExistentesController */
/* @var $model MaquinariaExistente */

$this->breadcrumbs=array(
	'Maquinarias Existentes'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Nueva Maquinaria Existente', 'url'=>array('create')),
	array('label'=>'Actualizar Maquinaria Existente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Maquinaria Existente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar esta maquinaria?')),
	array('label'=>'Administrar Maquinarias Existentes', 'url'=>array('index')),
);
?>

<h1>Ver detalles de <?php echo $model->descripcion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
		'extra:boolean',
	),
)); ?>
