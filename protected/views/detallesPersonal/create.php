<?php
/* @var $this DetallesPersonalController */
/* @var $model DetallePersonal */

$this->breadcrumbs=array(
	'Detalles de Personal'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Detalles de Personal', 'url'=>array('index')),
);
?>

<h1>Nuevo Detalle de Personal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>