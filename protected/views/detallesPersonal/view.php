<?php
/* @var $this DetallesPersonalController */
/* @var $model DetallePersonal */

$this->breadcrumbs=array(
	'Detalles de Personal'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Personal', 'url'=>array('create')),
	array('label'=>'Actualizar Detalle de Personal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Detalle de Personal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Detalles de Personal', 'url'=>array('index')),
);
?>

<h1>Ver Detalle de Personal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'trabajo_id',
		'personal_id',
		'dia_trabajo',
	),
)); ?>
