<?php
/* @var $this DetallesPersonalController */
/* @var $model DetallePersonal */

$this->breadcrumbs=array(
	'Detalles de Personal'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Personal', 'url'=>array('create')),
	array('label'=>'Ver Detalle de Personal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Detalles de Personal', 'url'=>array('index')),
);
?>

<h1>Actualizar Detalle de Personal <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>