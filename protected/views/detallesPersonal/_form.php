<?php
/* @var $this DetallesPersonalController */
/* @var $model DetallePersonal */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'detalle-personal-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'trabajo_id'); ?>
		<?php echo $form->dropDownList($model,'trabajo_id', CHtml::listData(Trabajo::model()->findAll(), 'id', 'numero' ) ); ?>
		<?php echo $form->error($model,'trabajo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'personal_id'); ?>
		<?php echo $form->dropDownList($model,'personal_id', CHtml::listData(Personal::model()->findAll(), 'id', 'descripcion' ) ); ?>
		<?php echo $form->error($model,'personal_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dia_trabajo'); ?>
		<?php echo $form->textField($model,'dia_trabajo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'dia_trabajo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->