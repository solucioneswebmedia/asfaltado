<?php
/* @var $this DetallesPersonalController */
/* @var $model DetallePersonal */

$this->breadcrumbs=array(
	'Detalles de Personal'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Personal', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#detalle-personal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Detalles de Personal</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detalle-personal-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'trabajo_id',
		'personal_id',
		'dia_trabajo',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
