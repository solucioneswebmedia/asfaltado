<?php
/* @var $this PresupuestosController */
/* @var $model Presupuesto */

$this->breadcrumbs=array(
	'Presupuestos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Create Presupuesto', 'url'=>array('create')),
	array('label'=>'View Presupuesto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Presupuesto', 'url'=>array('index')),
);
?>

<h1>Modificar Presupuesto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>