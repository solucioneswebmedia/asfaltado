<?php
/* @var $this PresupuestosController */
/* @var $model Presupuesto */

$this->breadcrumbs=array(
	'Presupuestos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Presupuestos', 'url'=>array('index')),
);
?>

<h1>Crear Presupuesto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>