<?php
/* @var $this PresupuestosController */
/* @var $model Presupuesto */

$this->breadcrumbs=array(
	'Presupuestos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Presupuesto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#presupuesto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Presupuestos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'presupuesto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'trabajo.numero',
		'nombre_partida',
		'monto',
		'tipo',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
