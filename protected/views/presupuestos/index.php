<?php
/* @var $this PresupuestosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Presupuestos',
);

$this->menu=array(
	array('label'=>'Crear Presupuesto', 'url'=>array('create')),
	array('label'=>'Administrar Presupuesto', 'url'=>array('index')),
);
?>

<h1>Presupuestos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
