<?php
/* @var $this PresupuestosController */
/* @var $model Presupuesto */

$this->breadcrumbs=array(
	'Presupuestos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Crear Presupuesto', 'url'=>array('create')),
	array('label'=>'Modificar Presupuesto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Administrar Presupuesto', 'url'=>array('index')),
);
?>

<h1>Ver Presupuesto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'trabajo_id',
		'nombre_partida',
		'monto',
		'tipo',
	),
)); ?>
