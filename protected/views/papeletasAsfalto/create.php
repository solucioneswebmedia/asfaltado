<?php
/* @var $this PapeletasAsfaltoController */
/* @var $model PapeletaAsfalto */

$this->breadcrumbs=array(
	'Papeletas de Asfalto'=>array('index'),
	'Nueva',
);

if(!isset($_GET['t'])){
	$this->menu=array(
		array('label'=>'Administrar Papeletas de Asfalto', 'url'=>array('index')),
	);
} else{
	$this->menu=array(
		array('label'=>'Volver', 'url'=>array('papeletasAsfalto/asfalto','id'=>$model->trabajo_id)),
	);
}
?>

<h1>Nueva Papeleta de Asfalto</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 't'=>(isset($_GET['t']))?$_GET['t']:0)); ?>