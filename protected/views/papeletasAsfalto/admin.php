<?php
/* @var $this PapeletasAsfaltoController */
/* @var $model PapeletaAsfalto */

$this->breadcrumbs=array(
	'Papeletas de Asfalto'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Papeleta de Asfalto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#papeleta-asfalto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Papeletas de Asfalto</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'papeleta-asfalto-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'name'=>'trabajo_search',
			'value'=>'$data->trabajo->numero',
			'filter'=>CHtml::activeTextField($model, 'trabajo_search')
			),
		array(
			'name'=>'proveedor_search',
			'value'=>'$data->proveedor->nombre',
			'filter'=>CHtml::activeTextField($model, 'proveedor_search')
			),
		'codigo',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'cantidad',
		array(
			'name'=>'procedencia',
			'type'=>'raw',
			'value'=>'PapeletaAsfalto::model()->checkProcedencia($data->procedencia)',
			'filter'=>array(1=>'Asfalto Propio',2=>'Convenio IVT', 3=>'Convenio Comercial', 4=>'Convenio Ciudadano'),
			),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
