<?php
/* @var $this PapeletasAsfaltoController */
/* @var $model PapeletaAsfalto */

$this->breadcrumbs=array(
	'Papeletas de Asfalto'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

if(!isset($_GET['t'])){
	$this->menu=array(
		array('label'=>'Nueva Papeleta de Asfalto', 'url'=>array('create')),
		array('label'=>'Ver Papeleta de Asfalto', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Administrar Papeletas de Asfalto', 'url'=>array('index')),
	);
} else{
	$this->menu=array(
		array('label'=>'Volver', 'url'=>array('papeletasAsfalto/asfalto','id'=>$model->trabajo_id)),
	);
}
?>

<h1>Actualizar Papeleta de Asfalto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 't'=>(isset($_GET['t']))?1:0)); ?>