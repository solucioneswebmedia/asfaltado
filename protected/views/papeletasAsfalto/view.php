<?php
/* @var $this PapeletasAsfaltoController */
/* @var $model PapeletaAsfalto */

$this->breadcrumbs=array(
	'Papeletas de Asfalto'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Nueva Papeleta de Asfalto', 'url'=>array('create')),
	array('label'=>'Actualizar Papeleta de Asfalto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Papeleta de Asfalto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar esta papeleta?')),
	array('label'=>'Administrar Papeletas de Asfalto', 'url'=>array('index')),
);
?>

<h1>Ver Papeleta de Asfalto #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'trabajo.numero',
		'proveedor.nombre',
		'codigo',
		array(
            'name'=>'fecha',
            'value'=>date("d-m-Y", strtotime( $model->fecha)),
                ),
		'cantidad',
		//'procedencia',
		array(
			'name'=>'procedencia',
			'value'=>PapeletaAsfalto::model()->checkProcedencia($model->procedencia),
			),
	),
)); ?>
