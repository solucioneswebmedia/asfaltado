<?php
/* @var $this PapeletasAsfaltoController */
/* @var $model PapeletaAsfalto */

$this->breadcrumbs=array(
	'Papeletas de Asfalto'=>array('index'),
	Trabajo::model()->findByPk($model->trabajo_id)->numero=>array('trabajos/view','id'=>$model->trabajo_id),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Papeleta de Asfalto', 'url'=>array('create','t'=>$model->trabajo_id)),
);


?>

<h1>Papeletas de Asfalto de trabajo: <?php echo Trabajo::model()->findByPk($model->trabajo_id)->numero; ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'papeleta-asfalto-grid',

	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'columns'=>array(
		//'id',
		array(
			'name'=>'trabajo_search',
			'value'=>'$data->trabajo->numero',
			'filter'=>CHtml::activeTextField($model, 'trabajo_search')
			),
		array(
			'name'=>'proveedor_search',
			'value'=>'$data->proveedor->nombre',
			'filter'=>CHtml::activeTextField($model, 'proveedor_search')
			),
		'codigo',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'cantidad',
		array(
			'name'=>'procedencia',
			'type'=>'raw',
			'value'=>'PapeletaAsfalto::model()->checkProcedencia($data->procedencia)',
			'filter'=>array(1=>'Asfalto Propio',2=>'Convenio IVT', 3=>'Convenio Comercial', 4=>'Convenio Ciudadano'),
			),
		array(
			'class'=>'CButtonColumn',
			'deleteConfirmation'=>"js: 'Realmente desea eliminar este registro?'",
			'template'=>'{update}{delete}',
			'buttons'=>array('update' => array('label'=>'Actualizar', 'url'=>'Yii::app()->createUrl("papeletasAsfalto/update",array("id"=>$data->id,"t"=>1))'),
								'delete' => array('label'=>'Eliminar', 'url'=>'Yii::app()->createUrl("papeletasAsfalto/delete",array("id"=>$data->id,"t"=>1))')				
				 ),
		),
	),
)); ?>
