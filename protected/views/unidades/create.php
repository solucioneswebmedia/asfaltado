<?php
/* @var $this UnidadesController */
/* @var $model Unidad */

$this->breadcrumbs=array(
	'Unidades'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Unidades', 'url'=>array('index')),
);
?>

<h1>Crear Unidad</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>