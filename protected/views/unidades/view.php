<?php
/* @var $this UnidadesController */
/* @var $model Unidad */

$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Crear Unidad', 'url'=>array('create')),
	array('label'=>'Actualizar Unidad', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Unidad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea borrar esta unidad?')),
	array('label'=>'Administrar Unidades', 'url'=>array('index')),
);
?>

<h1>Ver Unidad <?php echo $model->nombre_unidad; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'nombre_unidad',
	),
)); ?>
