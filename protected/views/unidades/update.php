<?php
/* @var $this UnidadesController */
/* @var $model Unidad */

$this->breadcrumbs=array(
	'Unidades'=>array('index'),
	$model->nombre_unidad=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Unidad', 'url'=>array('create')),
	array('label'=>'Ver Unidad', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Unidades', 'url'=>array('index')),
);
?>

<h1>Actualizar Unidad: <?php echo $model->nombre_unidad; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>