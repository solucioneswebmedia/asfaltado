<?php
/* @var $this MaterialesController */
/* @var $model Material */

$this->breadcrumbs=array(
	'Materiales'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Material', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#material-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Materiales</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'material-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'descripcion',
		array(
			'name'=>'proveedor_id',
			'value'=>'$data->proveedores->nombre'
			),
		'precio',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
