<?php
/* @var $this MaterialesController */
/* @var $model Material */

$this->breadcrumbs=array(
	'Materiales'=>array('index'),
	$model->descripcion,
);

$this->menu=array(
	array('label'=>'Nuevo Material', 'url'=>array('create')),
	array('label'=>'Modificar Material', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Material', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Materiales', 'url'=>array('index')),
);
?>

<h1>Ver <?php echo $model->descripcion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'descripcion',
		array(
			'name'=>'proveedor_id',
			'value'=>$model->proveedores->nombre,
			),
		'precio',
	),
)); ?>
