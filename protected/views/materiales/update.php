<?php
/* @var $this MaterialesController */
/* @var $model Material */

$this->breadcrumbs=array(
	'Materiales'=>array('index'),
	$model->descripcion=>array('view','id'=>$model->id),
	'Modificar',
);

$this->menu=array(
	array('label'=>'Nuevo Material', 'url'=>array('create')),
	array('label'=>'Ver Material', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Materiales', 'url'=>array('index')),
);
?>

<h1>Modificar Material <?php echo $model->descripcion; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>