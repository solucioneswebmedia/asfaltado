<?php
/* @var $this MaterialesController */
/* @var $model Material */

$this->breadcrumbs=array(
	'Materiales'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Materiales', 'url'=>array('index')),
);
?>

<h1>Nuevo Material</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>