<?php
/* @var $this MaquinariasController */
/* @var $model Maquinaria */

$this->breadcrumbs=array(
	'Maquinarias'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Maquinaria', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#maquinaria-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Maquinarias</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'maquinaria-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',		
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
