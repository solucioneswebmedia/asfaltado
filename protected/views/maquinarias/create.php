<?php
/* @var $this MaquinariasController */
/* @var $model Maquinaria */

$this->breadcrumbs=array(
	'Maquinarias'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Maquinarias', 'url'=>array('index')),
);
?>

<h1>Crear Maquinaria</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>