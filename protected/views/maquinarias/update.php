<?php
/* @var $this MaquinariasController */
/* @var $model Maquinaria */

$this->breadcrumbs=array(
	'Maquinarias'=>array('index'),
	$model->codigo=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Crear Maquinaria', 'url'=>array('create')),
	array('label'=>'Ver Maquinaria', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Maquinarias', 'url'=>array('index')),
);
?>

<h1>Actualizar Maquinaria <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>