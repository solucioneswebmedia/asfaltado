<?php
/* @var $this MaquinariasController */
/* @var $model Maquinaria */

$this->breadcrumbs=array(
	'Maquinarias'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Crear Maquinaria', 'url'=>array('create')),
	array('label'=>'Actualizar Maquinaria', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Maquinaria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea borrar esta maquinaria?')),
	array('label'=>'Administrar Maquinarias', 'url'=>array('index')),
);
?>

<h1>Ver detalles de <?php echo $model->descripcion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
	),
)); ?>
