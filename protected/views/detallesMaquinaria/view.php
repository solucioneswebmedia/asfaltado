<?php
/* @var $this DetallesMaquinariaController */
/* @var $model DetalleMaquinaria */

$this->breadcrumbs=array(
	'Detalles de Maquinaria'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Maquinaria', 'url'=>array('create')),
	array('label'=>'Actualizar Detalle de Maquinaria', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Detalle de Maquinaria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Detalles de Maquinaria', 'url'=>array('index')),
);
?>

<h1>Ver Detalle de Maquinaria #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'unidades_id',
		'trabajo_id',
		'maquinaria_existente_id',
		'dia_trabajo',
		'extra',
	),
)); ?>
