<?php
/* @var $this DetallesMaquinariaController */
/* @var $model DetalleMaquinaria */

$this->breadcrumbs=array(
	'Detalles de Maquinaria'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Detalles de Maquinaria', 'url'=>array('index')),
);
?>

<h1>Nuevo Detalle de Maquinaria</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>