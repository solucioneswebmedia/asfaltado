<?php
/* @var $this DetallesMaquinariaController */
/* @var $model DetalleMaquinaria */

$this->breadcrumbs=array(
	'Detalles de Maquinaria'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Maquinaria', 'url'=>array('create')),
	array('label'=>'Ver Detalle de Maquinaria', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Detalles de Maquinaria', 'url'=>array('index')),
);
?>

<h1>Actualizar Detalle de Maquinaria <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>