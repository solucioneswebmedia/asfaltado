<?php
/* @var $this DetallesMaquinariaController */
/* @var $data DetalleMaquinaria */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidades_id')); ?>:</b>
	<?php echo CHtml::encode($data->unidades_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trabajo_id')); ?>:</b>
	<?php echo CHtml::encode($data->trabajo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maquinaria_existente_id')); ?>:</b>
	<?php echo CHtml::encode($data->maquinaria_existente_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dia_trabajo')); ?>:</b>
	<?php echo CHtml::encode($data->dia_trabajo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extra')); ?>:</b>
	<?php echo CHtml::encode($data->extra); ?>
	<br />


</div>