<?php
/* @var $this DetallesMaquinariaController */
/* @var $model DetalleMaquinaria */

$this->breadcrumbs=array(
	'Detalles de Maquinaria'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Maquinaria', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#detalle-maquinaria-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Detalles de Maquinaria</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detalle-maquinaria-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'unidades_id',
		'trabajo_id',
		'maquinaria_existente_id',
		'dia_trabajo',
		'extra',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
