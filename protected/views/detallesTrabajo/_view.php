<?php
/* @var $this DetallesTrabajoController */
/* @var $data DetalleTrabajo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trabajo_id')); ?>:</b>
	<?php echo CHtml::encode($data->trabajo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dia_trabajo')); ?>:</b>
	<?php echo CHtml::encode($data->dia_trabajo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ancho')); ?>:</b>
	<?php echo CHtml::encode($data->ancho); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('largo')); ?>:</b>
	<?php echo CHtml::encode($data->largo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('espesor')); ?>:</b>
	<?php echo CHtml::encode($data->espesor); ?>
	<br />


</div>