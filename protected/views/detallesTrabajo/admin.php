<?php
/* @var $this DetallesTrabajoController */
/* @var $model DetalleTrabajo */

$this->breadcrumbs=array(
	'Detalles de Trabajo'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Trabajo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#detalle-trabajo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Detalles de Trabajos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'detalle-trabajo-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'trabajo_id',
		'dia_trabajo',
		'ancho',
		'largo',
		'espesor',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
