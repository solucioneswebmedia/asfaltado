<?php
/* @var $this DetallesTrabajoController */
/* @var $model DetalleTrabajo */

$this->breadcrumbs=array(
	'Detalles de Trabajo'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Detalles de Trabajo', 'url'=>array('index')),
);
?>

<h1>Nuevo Detalle de Trabajo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>