<?php
/* @var $this DetallesTrabajoController */
/* @var $model DetalleTrabajo */

$this->breadcrumbs=array(
	'Detalles de Trabajo'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Trabajo', 'url'=>array('create')),
	array('label'=>'Actualizar Detalle de Trabajo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Detalle de Trabajo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Detalles de Trabajo', 'url'=>array('index')),
);
?>

<h1>Ver Detalle de Trabajo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'trabajo_id',
		'dia_trabajo',
		'ancho',
		'largo',
		'espesor',
	),
)); ?>
