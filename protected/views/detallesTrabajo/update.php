<?php
/* @var $this DetallesTrabajoController */
/* @var $model DetalleTrabajo */

$this->breadcrumbs=array(
	'Detalles de Trabajo'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nuevo Detalle de Trabajo', 'url'=>array('create')),
	array('label'=>'Ver Detalle de Trabajo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Detalles de Trabajo', 'url'=>array('index')),
);
?>

<h1>Actualizar Detalle de Trabajo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>