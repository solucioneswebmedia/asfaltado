<?php
/* @var $this MaterialesRestantesController */
/* @var $model MaterialRestante */

$this->breadcrumbs=array(
	'Material Restante'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Material Restante', 'url'=>array('index')),
);
?>

<h1>Nuevo Material Restante</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>