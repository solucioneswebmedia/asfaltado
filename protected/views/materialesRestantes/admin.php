<?php
/* @var $this MaterialesRestantesController */
/* @var $model MaterialRestante */

$this->breadcrumbs=array(
	'Material Restante'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Material Restante', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#material-restante-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Material Restante</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'material-restante-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'proveedor.nombre',
		'cantidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
