<?php
/* @var $this MaterialesRestantesController */
/* @var $model MaterialRestante */

$this->breadcrumbs=array(
	'Material Restante'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Material Restante', 'url'=>array('create')),
	array('label'=>'Actualizar Material Restante', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Material Restante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Material Restante', 'url'=>array('index')),
);
?>

<h1>Ver Material Restante #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'proveedor.nombre',
		'cantidad',
	),
)); ?>
