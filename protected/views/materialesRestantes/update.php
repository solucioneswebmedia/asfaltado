<?php
/* @var $this MaterialesRestantesController */
/* @var $model MaterialRestante */

$this->breadcrumbs=array(
	'Material Restante'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nuevo Material Restante', 'url'=>array('create')),
	array('label'=>'Ver Material Restante', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Material Restante', 'url'=>array('index')),
);
?>

<h1>Actualizar Material Restante <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>