<?php
/* @var $this RolesController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->tipo_usuario,
);

$this->menu=array(
	array('label'=>'Crear Rol', 'url'=>array('create')),
	array('label'=>'Actualizar Rol', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Rol', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro de querer eliminar este Rol?')),
	array('label'=>'Administrar Roles', 'url'=>array('index')),
);
?>

<h1>Ver Rol #<?php echo $model->tipo_usuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tipo_usuario',
	),
)); ?>
