<?php
/* @var $this RolesController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->tipo_usuario=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Rol', 'url'=>array('create')),
	array('label'=>'Ver Rol', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Roles', 'url'=>array('admin')),
);
?>

<h1>Actualizar Rol <?php echo $model->tipo_usuario; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>