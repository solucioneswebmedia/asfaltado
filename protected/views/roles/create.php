<?php
/* @var $this RolesController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Roles', 'url'=>array('index')),
);
?>

<h1>Crear Rol</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>