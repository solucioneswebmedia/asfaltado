<?php
/* @var $this RolesController */
/* @var $model Rol */

$this->breadcrumbs=array(
	'Roles'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Rol', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#rol-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Roles</h1>

<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rol-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'tipo_usuario',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
