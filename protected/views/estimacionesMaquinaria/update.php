<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $model EstimacionMaquinaria */

$this->breadcrumbs=array(
	'Estimaciones de Maquinaria'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nueva Estimaci&oacute;n de Maquinaria', 'url'=>array('create')),
	array('label'=>'Ver Estimaci&oacute;n de Maquinaria', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Estimaciones de Maquinaria', 'url'=>array('index')),
);
?>

<h1>Actualizar Estimaci&oacute;n de Maquinaria <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>