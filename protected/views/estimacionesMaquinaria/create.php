<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $model EstimacionMaquinaria */

$this->breadcrumbs=array(
	'Estimaciones de Maquinaria'=>array('index'),
	'Nueva',
);

$this->menu=array(
	array('label'=>'Administrar Estimaciones de Maquinaria', 'url'=>array('index')),
);
?>

<h1>Nueva Estimaci&oacute;n de Maquinaria</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>