<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $model EstimacionMaquinaria */

$this->breadcrumbs=array(
	'Estimaciones de Maquinaria'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Estimaci&oacute;n de Maquinaria', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estimacion-maquinaria-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Estimaciones de Maquinaria</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'estimacion-maquinaria-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'inspecciones_id',
		'maquinaria_id',
		'cantidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
