<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $model EstimacionMaquinaria */

$this->breadcrumbs=array(
	'Estimaciones de Maquinaria'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva Estimaci&oacute;n de Maquinaria', 'url'=>array('create')),
	array('label'=>'Actualizar Estimaci&oacute;n de Maquinaria', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Estimaci&oacute;n de Maquinaria', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar EstimacionMaquinaria', 'url'=>array('index')),
);
?>

<h1>Ver Estimaci&oacute;n de Maquinaria #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//s'id',
		'inspecciones_id',
		'maquinaria_id',
		'cantidad',
	),
)); ?>
