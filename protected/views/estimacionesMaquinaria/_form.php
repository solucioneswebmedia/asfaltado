<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $model EstimacionMaquinaria */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'estimacion-maquinaria-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'inspecciones_id'); ?>
		<?php echo $form->dropDownList($model,'inspecciones_id', CHtml::listData(Inspeccion::model()->findAll(), 'id', 'numero' ) ); ?>
		<?php echo $form->error($model,'inspecciones_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maquinaria_id'); ?>
		<?php echo $form->dropDownList($model,'maquinaria_id', CHtml::listData(Maquinaria::model()->findAll(), 'id', 'descripcion' ) ); ?>
		<?php echo $form->error($model,'maquinaria_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->