<?php
/* @var $this EstimacionesMaquinariaController */
/* @var $data EstimacionMaquinaria */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inspecciones_id')); ?>:</b>
	<?php echo CHtml::encode($data->inspecciones_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maquinaria_id')); ?>:</b>
	<?php echo CHtml::encode($data->maquinaria_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />


</div>