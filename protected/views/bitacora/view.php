<?php
/* @var $this BitacoraController */
/* @var $model Bitacora */

$this->breadcrumbs=array(
	'Bitacora'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Consultar Bitacora', 'url'=>array('index')),
);
?>

<h1>Ver Bitacora #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'tabla',
		'registro',
		'usuario',
		'tipo',
		'timestamp',
	),
)); ?>
