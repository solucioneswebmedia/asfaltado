<?php
/* @var $this BitacoraController */
/* @var $model Bitacora */

$this->breadcrumbs=array(
	'Bitacora'=>array('index'),
	'Consultar',
);

?>


<h1>Registro de actividades</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bitacora-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'tabla',
		'registro',
		'usuario',
		'tipo',
		'timestamp',
	),
)); ?>
