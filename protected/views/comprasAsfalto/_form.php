<?php
/* @var $this ComprasAsfaltoController */
/* @var $model CompraAsfalto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'compra-asfalto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'proveedor_id'); ?>
		<?php echo $form->dropDownList($model,'proveedor_id', CHtml::listData(Proveedor::model()->findAll(), 'id', 'nombre' ) ); ?>
		<?php echo $form->error($model,'proveedor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'name'=>'CompraAsfalto[fecha]',
				'id'=>'CompraAsfalto_fecha',
				//'value'=>(isset($model->fecha)&& $model->fecha!='')?date('d/m/Y',strtotime($model->fecha)):'',
				'value'=>(isset($model->fecha))?date("d-m-Y", strtotime( $model->fecha)):date("d-m-Y"),
				'language'=> 'es',
				'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
				),
				'options'=> array(
					'dateFormat'=>'dd-mm-yy',
					'altField' => '',
					'altFormat' => 'dd-mm-yy',
				),
			));
		?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->