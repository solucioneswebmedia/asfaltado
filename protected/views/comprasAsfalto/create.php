<?php
/* @var $this ComprasAsfaltoController */
/* @var $model CompraAsfalto */

$this->breadcrumbs=array(
	'Compra de Asfalto'=>array('index'),
	'Nueva',
);

$this->menu=array(
	array('label'=>'Administrar Compras de Asfalto', 'url'=>array('index')),
);
?>

<h1>Nueva Compra de Asfalto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>