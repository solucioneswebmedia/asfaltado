<?php
/* @var $this ComprasAsfaltoController */
/* @var $model CompraAsfalto */

$this->breadcrumbs=array(
	'Compra de Asfalto'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Compra de Asfalto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#compra-asfalto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Compras de Asfalto</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'compra-asfalto-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'name'=>'proveedor_search',
			'value'=>'$data->proveedor->nombre',
			'filter'=>CHtml::activeTextField($model, 'proveedor_search')
			),
		array(
			'name'=>'fecha',
			'header'=>'Fecha', // Este es el label
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'cantidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
