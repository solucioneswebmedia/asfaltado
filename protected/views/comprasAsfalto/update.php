<?php
/* @var $this ComprasAsfaltoController */
/* @var $model CompraAsfalto */

$this->breadcrumbs=array(
	'Compra de Asfalto'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nueva Compra de Asfalto', 'url'=>array('create')),
	array('label'=>'Ver Compra de Asfalto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Compras de Asfalto', 'url'=>array('index')),
);
?>

<h1>Actualizar Compra de Asfalto <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>