<?php
/* @var $this ComprasAsfaltoController */
/* @var $model CompraAsfalto */

$this->breadcrumbs=array(
	'Compra de Asfalto'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva Compra de Asfalto', 'url'=>array('create')),
	array('label'=>'Actualizar Compra de Asfalto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Compra de Asfalto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Compras de Asfalto', 'url'=>array('index')),
);
?>

<h1>Ver detalles de Compra de Asfalto</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'proveedor.nombre',
		array(
            'name'=>'fecha',
            'value'=>date("d-m-Y", strtotime( $model->fecha)),
            ),
		'cantidad',
	),
)); ?>
