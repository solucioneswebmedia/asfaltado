<?php
/* @var $this FotosInspeccionController */
/* @var $model FotoInspeccion */

$this->breadcrumbs=array(
	'Fotos Inspecciones'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva Foto de Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Actualizar  Foto de Inspecci&oacute;n', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Foto de Inspecci&oacute;n', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar Fotos Inspecciones', 'url'=>array('index')),
);
?>

<h1>Ver Foto de Inspecci&oacute;n #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'inspeccion.numero',
		'ruta',
	),
)); ?>
