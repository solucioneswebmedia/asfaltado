<?php
/* @var $this FotosInspeccionController */
/* @var $model FotoInspeccion */

$this->breadcrumbs=array(
	'Fotos Inspecciones'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nueva Foto Inspecci&oacute;n', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#foto-inspeccion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Fotos Inspecciones</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'foto-inspeccion-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'inspeccion.numero',
		'ruta',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
