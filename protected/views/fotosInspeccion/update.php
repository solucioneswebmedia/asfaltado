<?php
/* @var $this FotosInspeccionController */
/* @var $model FotoInspeccion */

$this->breadcrumbs=array(
	'Fotos Inspecciones'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Nueva Foto de Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Ver Foto de Inspecci&oacute;n', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Fotos Inspecciones', 'url'=>array('index')),
);
?>

<h1>Actualizar Foto de Inspecci&oacute;n <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>