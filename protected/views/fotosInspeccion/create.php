<?php
/* @var $this FotosInspeccionController */
/* @var $model FotoInspeccion */

$this->breadcrumbs=array(
	'Fotos Inspecciones'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Fotos Inspeccion', 'url'=>array('index')),
);
?>

<h1>Nueva Foto de Inspecci&oacute;n</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>