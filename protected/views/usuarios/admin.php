<?php
/* @var $this UsuariosController */
/* @var $model Usuario */

$this->breadcrumbs=array(
	'Usuarios'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Usuario', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#usuario-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Usuarios</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'usuario-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'usuario',
		array(
			'name'=>'tipo_search',
			'value'=>'$data->rol->tipo_usuario',
			'filter'=>CHtml::activeTextField($model, 'tipo_search')
			),
		//'pass',
		'cedula',
		'nombres',
		
		'apellidos',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
