<?php
/* @var $this InspeccionesController */
/* @var $data Inspeccion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('solicitud_inspeccion_id')); ?>:</b>
	<?php echo CHtml::encode($data->solicitud_inspeccion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_obra')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_obra); ?>
	<br />

	<?php 
	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recomendaciones')); ?>:</b>
	<?php echo CHtml::encode($data->recomendaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pto_ref')); ?>:</b>
	<?php echo CHtml::encode($data->pto_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('material_bacheo')); ?>:</b>
	<?php echo CHtml::encode($data->material_bacheo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('material')); ?>:</b>
	<?php echo CHtml::encode($data->material); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('croquis')); ?>:</b>
	<?php echo CHtml::encode($data->croquis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_adm')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_adm); ?>
	<br />

	<b><?php /* echo CHtml::encode($data->getAttributeLabel('coordenadas')); ?>:</b>
	<?php echo CHtml::encode($data->coordenadas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('semana')); ?>:</b>
	<?php echo CHtml::encode($data->semana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prioridad')); ?>:</b>
	<?php echo CHtml::encode($data->prioridad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realizada')); ?>:</b>
	<?php echo CHtml::encode($data->realizada); ?>
	<br />

	*/ ?>

</div>