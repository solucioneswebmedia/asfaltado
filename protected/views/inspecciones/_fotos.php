<?php
$fotosFormConfig = array(
  'elements'=>array(        
    'ruta'=>array(
                  'type'=>'file',
                  'visible'=>true, //Important!
            ),
    ));

$this->widget('ext.multimodelform.MultiModelForm',array(
        'tableView' => true,
        'id' => 'id_fotos', //the unique widget id
        'addItemText'=>'agregar nueva foto',
        'removeText'=>'eliminar',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $fotosFormConfig, //the form configuration array
        'model' => $images, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedImages,
 
        //array of member instances loaded from db
        'data' => $images->findAll('inspecciones_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>