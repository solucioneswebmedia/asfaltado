<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'inspeccion-grid',
    'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
    'dataProvider'=>$model->getPersonal(),
    'filter'=>null,
    'columns'=>array(
        //'id',
        'personal.descripcion',
        'cantidad',
        'num_dias',
    ),
)); ?>