<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'inspeccion-grid',
    'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
    'dataProvider'=>$model->getComputos(),
    'filter'=>null,
    'columns'=>array(
        //'id',
        'cantidad',
        'ancho',
        'largo',
        'espesor'
    ),
)); ?>