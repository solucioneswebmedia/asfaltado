<?php

$diasFormConfig = array(
  'elements'=>array(
    'fecha'=>array(        
        'type'=>'zii.widgets.jui.CJuiDatePicker',
        'language'=>'es',
        'value'=>date("d-m-Y"),
        'options'=>array(
            'showAnim'=>'fold',
            'options'=> array(
                'dateFormat'=>'dd-mm-yy',
                'altFormat'=>'dd-mm-yy',
                )
            )
        )
    ) // Fin de array elements
  );

$this->widget('ext.multimodelform.MultiModelForm',array(
    'tableView' => true,
    'jsAfterNewId' => MultiModelForm::afterNewIdDatePicker($diasFormConfig['elements']['fecha']),
    'addItemText'=>'<i class="fa fa-plus fa-fw"></i>Agregar Nuevo',
    'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
    'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $diasFormConfig, //the form configuration array
        'model' => $dias, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedDias,

        //array of member instances loaded from db
        'data' => $dias->findAll('inspecciones_id=:groupId', array(':groupId'=>$model->id)),
        ));
?>