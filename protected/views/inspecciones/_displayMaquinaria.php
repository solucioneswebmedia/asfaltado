<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'inspeccion-grid',
    'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
    'dataProvider'=>$model->getMaquinaria(),
    'filter'=>null,
    'columns'=>array(
        //'id',
        'maquinaria.descripcion',
        'cantidad',
    ),
)); ?>