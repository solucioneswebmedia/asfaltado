<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'inspeccion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		)); ?>

		<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

		<?php echo $form->errorSummary(array_merge(array($model),$validatedMetricos,$validatedMaquinas,$validatedPersonal)); ?>
		<?php if ($model->isNewRecord) { ?>
		<div class="row">
			<?php echo $form->labelEx($model,'solicitud_inspeccion_id'); ?>
			<?php echo $form->dropDownList($model,'solicitud_inspeccion_id', CHtml::listData(SolicitudInspeccion::model()->findAll("realizada=0"), 'id', 'numero' ) ); ?>
			<?php echo $form->error($model,'solicitud_inspeccion_id'); ?>
		</div>
		<?php }else{
			?>
			<?php echo $form->labelEx($model,'solicitud_inspeccion_id'); ?>
			<?php echo $form->dropDownList($model,'solicitud_inspeccion_id', CHtml::listData(SolicitudInspeccion::model()->findAll(), 'id', 'numero' ) ); ?>
			<?php echo $form->error($model,'solicitud_inspeccion_id'); ?>
		<?php }
		?>
		<div class="row">
			<?php echo $form->labelEx($model,'usuario_id'); ?>
			<?php echo Yii::app()->user->id,$form->hiddenField($model,'usuario_id',array('value'=>Usuario::model()->find("usuario='".Yii::app()->user->id."'")->id)); ?>
			<?php echo $form->error($model,'usuario_id'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'parroquia'); ?>
			<?php echo $form->dropDownList($model,'parroquia', array('SAN JUAN BAUTISTA'=>'SAN JUAN BAUTISTA', 
				'PEDRO MARIA MORANTES'=>'PEDRO MARIA MORANTES',
				'LA CONCORDIA'=>'LA CONCORDIA',
				'SAN SEBASTIAN'=>'SAN SEBASTIAN',
				'FRANCISCO ROMERO LOBO'=>'FRANCISCO ROMERO LOBO') );
				?>
				<?php echo $form->error($model,'parroquia'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'fecha'); ?>
				<?php
				if (!$model->isNewRecord){
					$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'model' => $model,
						'name'=>'Inspeccion[fecha]',
						'id'=>'Inspeccion_fecha',
				//'value'=>(isset($model->fecha)&& $model->fecha!='')?date('d/m/Y',strtotime($model->fecha)):'',
						'value'=>(isset($model->fecha))?date("d-m-Y", strtotime( $model->fecha)):date("d-m-Y"),
						'language'=> 'es',
						'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
					),
						'options'=> array(
							'dateFormat'=>'dd-mm-yy',
							'altField' => '',
							'altFormat' => 'dd-mm-yy',
							),
						)); 
				}
				else
				{
					$model->fecha=date('d-m-Y');
					echo $model->fecha,$form->hiddenField($model,'fecha',array('value'));
				}
				?>
				<?php echo $form->error($model,'fecha'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'tipo_obra'); ?>
				<?php echo $form->textField($model,'tipo_obra',array('size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'tipo_obra'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'asunto'); ?>
				<?php echo $form->textField($model,'asunto',array('size'=>60,'maxlength'=>200)); ?>
				<?php echo $form->error($model,'asunto'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'problematica'); ?>
				<?php echo $form->textField($model,'problematica',array('size'=>200,'maxlength'=>200)); ?>
				<?php echo $form->error($model,'problematica'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'descripcion'); ?>
				<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>200)); ?>
				<?php echo $form->error($model,'descripcion'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'recomendaciones'); ?>
				<?php echo $form->textField($model,'recomendaciones',array('size'=>60,'maxlength'=>200)); ?>
				<?php echo $form->error($model,'recomendaciones'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'pto_ref'); ?>
				<?php echo $form->textField($model,'pto_ref',array('size'=>60,'maxlength'=>100)); ?>
				<?php echo $form->error($model,'pto_ref'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'material_bacheo'); ?>
				<?php echo $form->textField($model,'material_bacheo',array('size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'material_bacheo'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'tipo_adm'); ?>
				<?php echo $form->dropDownList($model,'tipo_adm', array('ADMINISTRACION DIRECTA'=>'ADMINISTRACION DIRECTA', 																	
				'OBRA CONTRATADA'=>'OBRA CONTRATADA') ); ?>
				<?php echo $form->error($model,'tipo_adm'); ?>
			</div>

			<?php if (!($model->isNewRecord)) { ?>

			<div class="row">
				<?php echo $form->labelEx($model,'realizada'); ?>
				<?php echo $form->checkBox($model,'realizada'); ?>
				<?php echo $form->error($model,'realizada'); ?>
			</div>

			
			<?php } ?>
			<div class="row">
				<?php
			// INICIO MULTIMODEL
				$this->widget('zii.widgets.jui.CJuiAccordion', array(
					'panels'=>array(
						'C&oacute;mputos M&eacute;tricos'=>$this->renderPartial('_metricos', array('model'=>$model,'metrico'=>$metrico,'validatedMetricos'=>$validatedMetricos), true),
						'Estimaci&oacute;n de Maquinaria'=>$this->renderPartial('_maquinaria', array('model'=>$model,'maq'=>$maq,'validatedMaquinas'=>$validatedMaquinas), true),
						'Estimaci&oacute;n de Personal'=>$this->renderPartial('_personal', array('model'=>$model,'per'=>$per,'validatedPersonal'=>$validatedPersonal), true),
						//'Fotograf&iacute;as'=>$this->renderPartial('_displayFotos',array('model'=>$model), true),
						'Dias para Programaci&oacute;n'=>$this->renderPartial('_dias', array('model'=>$model,'dias'=>$dias,'validatedDias'=>$validatedDias), true),
			// panel 5 contains the content rendered by a partial view
			//'panel 5'=>$this->renderPartial('_partial',null,true),
						),
		// additional javascript options for the accordion plugin
					'options'=>array(
						'animated'=>'bounceslide',
						'heightStyle'=>'content' ))); ?>
					</div>

					<div class="row buttons">
						<br/>
						<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
					</div>

					<?php $this->endWidget(); ?>

</div><!-- form -->