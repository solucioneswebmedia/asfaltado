<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Programaci&oacute;n de Inspecciones', 'url'=>array('programacion')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inspeccion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Inspecciones</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inspeccion-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'numero',
		array(
			'name'=>'solicitud_search',
			'value'=>'(is_null($data->solicitudInspeccion))?("S/N"):($data->solicitudInspeccion->numero)',
			'filter'=>CHtml::activeTextField($model, 'solicitud_search')
			),
		'parroquia',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'tipo_obra',
		'descripcion',
		/*'recomendaciones',
		'pto_ref',
		'material_bacheo',
		*/
		'tipo_adm',
		array(
            'name'=>'realizada',
            'header'=>'Realizada',
            'filter'=>array('1'=>'Si','0'=>'No'),
            'value'=>'($data->realizada=="1")?("Si"):("No")'),
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
