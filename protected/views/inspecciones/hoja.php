<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	'Programacion'=>array('programacion'),
	'Hoja de Programacion',
);

$this->menu=array(
	array('label'=>'Administrar Inspecciones', 'url'=>array('index')),
	array('label'=>'Programaci&oacute;n de Inspecciones', 'url'=>array('programacion')),
);

?>

<h1>Exportar hoja de programación</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'programacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary(array_merge(array($model))); ?>


		<p>Seleccione la semana de la que desea obtener la programación:</p>
		<p><?php  			
			$objSemanas = Inspeccion::model()->findAllBySql("SELECT DISTINCT semana FROM inspecciones");
			foreach ($objSemanas as $obj) {
				$arraySemanas[]=array('id'=>$obj->semana, 'semana'=>$obj->semana);
			}		
			echo $form->dropDownList($model,'semana', CHtml::listData($arraySemanas,'id', 'semana')); 
		?></p>
		<p><?php echo $form->error($model,'semana'); ?></p>
	
		<?php 
			$this->widget('zii.widgets.jui.CJuiButton',array(
					    'buttonType'=>'submit',
					    'name'=>'submit',
					    'caption'=>'Aceptar',
					    'htmlOptions'=>array(
	        				'style'=>'background:#468847;color:#ffffff;'					
					    ))
			);
		 ?>
	

<?php $this->endWidget(); ?>
