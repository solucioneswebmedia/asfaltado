<script>
	function pregunta(){
	for(i=0; ele=document.eliminarForm.elements[i]; i++){
		if(ele.type=='checkbox'){
			if (ele.checked){
			    if (confirm('¿Estas seguro de eliminar esta(s) fotografía(s)?')){
			       return true;
			    }
			}
		}
	}
	return false;
}
</script>
<?php
$this->widget('ext.multiupload.EAjaxUpload.EAjaxUpload',
	array(
		'id'=>'uploadFile',
		'config'=>array(
			'action'=>Yii::app()->createUrl('inspecciones/upload/'.$model->id),
			'allowedExtensions'=>array("jpg","jpeg","png"),
               'sizeLimit'=>1000*1024*1024,// maximum file size in bytes
               'minSizeLimit'=>1*1024,
               'auto'=>true,
               'multiple' => false,
               'onComplete'=>"js:function(id, fileName, responseJSON){ location.reload(); }",
               'messages'=>array(
               	'typeError'=>"{file} es un tipo de archivo no válido. Solamente los tipos {extensions} son permitidos.",
               	'sizeError'=>"{file} es muy pesado, las dimensiones máximas son {sizeLimit}.",
               	'minSizeError'=>"{file} es muy pequeño, las dimensiones minimas son {minSizeLimit}.",
               	'emptyError'=>"{file} esta vacio, selecciona de nuevo.",
               	'onLeave'=>"Los archivos se estan cargando, si cancela ahora la carga se interrumpirá."
               	),
               'showMessage'=>"js:function(message){ alert(message); }"
               )

));
?>
<form name=eliminarForm action="<?php echo Yii::app()->baseUrl,"/".'inspecciones/eliminar'?>" method="post" onsubmit="return pregunta()">
	
	<?php 
	foreach ($model->getFotos() as $foto) {
		?>
		
		<a class="link-fotos" href="<?php echo Yii::app()->baseUrl,"/",$foto->ruta; ?>" ><img src="<?php echo Yii::app()->baseUrl,"/",$foto->ruta; ?>" style="height:100px;width:auto;"/></a> 
		<input type="checkbox" value="<?php echo $foto->ruta; ?>" name="<?php echo $foto->id; ?>" >

		<?php
		
	}
	?>
	<br/><br/>
	<div style="direction: ltr; position: relative; overflow: hidden;" class="qq-upload-button">
		<input style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;" type="submit">Borrar Sel.
	</div>
</form>
<?php 

$this->widget("ext.magnific-popup.EMagnificPopup", array('target'=>'.link-fotos'));
?>
