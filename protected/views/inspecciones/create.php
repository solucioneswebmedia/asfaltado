<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Inspecciones', 'url'=>array('index')),
);
?>

<h1>Crear Inspecci&oacute;n</h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'metrico'=>$metrico,
	'validatedMetricos'=>$validatedMetricos,
	'maq'=>$maq,
	'validatedMaquinas'=>$validatedMaquinas,
	'per'=>$per,
	'validatedPersonal'=>$validatedPersonal,
	'dias'=>$dias,
	'validatedDias'=>$validatedDias,
	)); ?>