<?php
//esta funcion toma los modelos (registros) devueltos por findAll() y crea un arreglo del tipo $arreglo[category_id]=category_name
$maquinas=array(""=>'Seleccione...') + CHtml::listData(Maquinaria::model()->findAll(),'id', 'descripcion');  
$maquinasFormConfig = array(
  'elements'=>array(
    'maquinaria_id'=>array(
        'type'=>'dropdownlist',
        'items'=>$maquinas,
        ),
    'cantidad'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),        
    ));

$this->widget('ext.multimodelform.MultiModelForm',array(
        'tableView' => true,
        'id' => 'id_maquinas', //the unique widget id
        'addItemText'=>'<i class="fa fa-plus fa-fw"></i>Agregar Nuevo',
        'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $maquinasFormConfig, //the form configuration array
        'model' => $maq, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedMaquinas,
 
        //array of member instances loaded from db
        'data' => $maq->findAll('inspecciones_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>