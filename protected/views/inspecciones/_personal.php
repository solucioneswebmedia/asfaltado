<?php
$personals=array(""=>'Seleccione...') + CHtml::listData(Personal::model()->findAll(),'id', 'descripcion');  
$personalFormConfig = array(
  'elements'=>array(
    'personal_id'=>array(
        'type'=>'dropdownlist',
        'items'=>$personals,
        ),
    'cantidad'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'num_dias'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),        
    ));

$this->widget('ext.multimodelform.MultiModelForm',array(
        'tableView' => true,
        'id' => 'id_personal', //the unique widget id
        'addItemText'=>'<i class="fa fa-plus fa-fw"></i>Agregar Nuevo',
        'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $personalFormConfig, //the form configuration array
        'model' => $per, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedPersonal,
 
        //array of member instances loaded from db
        'data' => $per->findAll('inspecciones_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>