<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	'Programacion',
);

$this->menu=array(
	array('label'=>'Administrar Inspecciones', 'url'=>array('index')),
	array('label'=>'Exportar Programación', 'url'=>array('hoja')),
);

?>

<h1>Programaci&oacute;n de inspecciones</h1>

<?php 
	$semanas=array();
	for ($i=Inspeccion::model()->getSemanaMinima()->semana; $i<53; $i++){
		if($i==0){
			$i++;
		}
		$semanas[$i]=$i;
	}
	$cuadrillas=CHtml::listData(Personal::model()->findAll(),'id', 'descripcion');
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inspeccion-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->getInspeccionesNuevas(),
	'filter'=>$model,
	'columns'=>array(
		'numero',
		/*array(
			'name'=>'solicitud_search',
			'value'=>'$data->solicitudInspeccion->numero',
			'filter'=>CHtml::activeTextField($model, 'solicitud_search')
			),*/
		//'usuario_usuario',
		'parroquia',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'descripcion',
		array(
			'class'=>'editable.EditableColumn',
			'name'=>'semana',
			'editable'=> array(
				'title'=>'Seleccionar semana',
				'type'=>'select',
				'attribute'=>'semana',
				'url'=>$this->createUrl('inspecciones/semana'),
				'source'=>$semanas,
				/*'options' => array( //custom display
					'display' => 'js: function(value, sourceData) {
					var selected = $.grep(sourceData, function(o){ return value == o.value; });
					if((selected!="")){
						$(this).text(selected[0].text);
					}else{
						$(this).text("Seleccione semana");	
					}
					}'
					),*/
				),
			),
		array(
			'class'=>'editable.EditableColumn',
			'name'=>'cuadrilla',
			'editable'=> array(
				'title'=>'Seleccionar cuadrilla',
				'type'=>'select',
				'attribute'=>'cuadrilla',
				'source'=>Editable::source(Personal::model()->findAllBySql('SELECT * FROM personal WHERE descripcion LIKE "Cuadrilla%"'),'id', 'descripcion'),
				'url'=>$this->createUrl('inspecciones/cuadrilla'),
				'options' => array( //custom display
					'success' => 'js: function(response, newValue) {
						var msg="";
						response=JSON.parse(response);
					    if(response.exito==true) ;
					    else 
					    {
					    	$.each(response.errors,function (k,v){msg+=v+\',\';});
					    	return msg;
					    }
					}'
					),
				),
			),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',
		),
	),
));
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.es.js', CClientScript::POS_END);
?>

