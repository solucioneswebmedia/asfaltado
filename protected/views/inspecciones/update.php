<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	$model->numero=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Ver Inspecci&oacute;n', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Inspecciones', 'url'=>array('index')),
);
?>

<h1>Actualizar Inspecci&oacute;n <?php echo $model->numero; ?></h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'metrico'=>$metrico,
	'validatedMetricos'=>$validatedMetricos,
	'maq'=>$maq,
	'validatedMaquinas'=>$validatedMaquinas,
	'per'=>$per,
	'validatedPersonal'=>$validatedPersonal,
	'dias'=>$dias,
	'validatedDias'=>$validatedDias,
	'images'=>$images,
	'validatedImages'=>$validatedImages,
	)); ?>