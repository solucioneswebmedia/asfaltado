<?php
$computosFormConfig = array(
  'elements'=>array(
    'cantidad'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'ancho'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'largo'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),        
    'espesor'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    ));

$this->widget('multimodel.MultiModelForm',array(
        'tableView' => true,
        'id' => 'id_metricos', //the unique widget id
        'addItemText'=>'<i class="fa fa-plus fa-fw"></i>Agregar Nuevo',
        'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $computosFormConfig, //the form configuration array
        'model' => $metrico, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedMetricos,
 
        //array of member instances loaded from db
        'data' => $metrico->findAll('inspecciones_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>