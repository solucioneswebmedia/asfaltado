<?php 
   function formato($value){
    return number_format($value,2,",",".");
  }

  function incrustarImagen($ruta){
    $basePath=Yii::app()->params['imagePath'];
    if(file_exists($basePath.$ruta)){
      $content=file_get_contents($basePath.$ruta);
      $encoded=base64_encode($content);
      $extension=pathinfo($ruta, PATHINFO_EXTENSION);
    }
    return "data:image/".strtolower($extension).";base64,".$encoded;
  }
?>
<style type="text/css">
<!--
.titulo-fnormal {font-size: 12px; font-weight: bold;}
.titulo-fpeq {font-size: 10px; font-weight: bold;}
.fnormal {font-size: 12px;}
.fpeq {font-size: 10px;}
.num {}
-->
</style>
<div align="center"><img src="<?php echo incrustarImagen("header.jpg"); ?>" width="593" height="52" vspace="3"/></div>
<br />
<table width="546" border="1" cellspacing="0">
  <tr>
    <td width="593" colspan="8" bordercolor="#000000" bgcolor="#CCCCCC"><div align="center"><b>INFORME DE INSPECCI&Oacute;N</b></div></td>
  </tr>
  <tr>
    <td valign="middle" colspan="2"><div align="center" class="titulo-fnormal">N&deg; DE INSPECCI&Oacute;N</div></td>
    <td valign="middle" colspan="2"><div align="center" class="fnormal"><?php echo $model->numero; ?></div></td>
    <td valign="middle" colspan="2"><div align="center" class="titulo-fnormal">FECHA REALIZADA INSPECCI&Oacute;N</div></td>
    <td valign="middle" colspan="2"><div class"fnormal"><?php echo $model->fecha; ?></div></td>
  </tr>
  <tr>
    <td rowspan="2" valign="middle" colspan="2"><div align="center" class="fnormal"><b>Asunto:</b> <?php echo $model->asunto;?> </div></td>
    <td valign="middle" colspan="2"><div align="center" class="titulo-fnormal">SOLICITUD DE:</div></td>
    <td rowspan="2" valign="middle" colspan="2"><div align="center" class="titulo-fnormal">SOLICITADO POR: </div></td>
    <td rowspan="2" valign="middle" colspan="2"><div align="center" class="fnormal"><?php echo $solicit->nombre_solicitante; ?></div></td>
  </tr>
  <tr>
    <td valign="middle" colspan="2"><div align="center" class="fnormal"><?php echo $solicit->numero; ?></div></td>
  </tr>
  <tr>
    <td colspan="4" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">DIRECCI&Oacute;N</div></td>
  <td colspan="4" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">PARROQUIA</div></td>
  </tr>
  <tr>
    <td colspan="4" valign="middle"><div align="center" class="fnormal"><?php echo $solicit->direccion; ?></div></td>
  <td colspan="4" valign="middle"><div align="center" class="fnormal"><?php echo $model->parroquia; ?></div></td>
  </tr>
  <tr>
    <td colspan="8" bgcolor="#CCCCCC"><div align="center"><b>DESCRIPCI&Oacute;N</b></div></td>
  </tr>
  <tr>
    <td colspan="3" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">PROBLEM&Aacute;TICA</div></td>
    <td colspan="2" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">TIPO DE REPARACI&Oacute;N</div></td>
    <td colspan="3" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">DESCRIPCI&Oacute;N DEL TRABAJO</div></td>
  </tr>
  <tr>
    <td colspan="3" valign="middle"><div align="center" class="fpeq"><?php echo $model->problematica; ?></div></td>
    <td colspan="2" valign="middle"><div align="center" class="fpeq"><?php echo $model->tipo_obra; ?></div></td>
    <td colspan="3" valign="middle" ><div align="center" class="fpeq"><?php echo $model->descripcion; ?></div></td>
  </tr>
  <tr>
    <td colspan="8" bgcolor="#CCCCCC"><div align="center" class="titulo-fnormal">AREA A TRATAR PARA LA REALIZACI&Oacute;N DE LOS TRABAJOS EN CUANTO A C&Oacute;MPUTOS M&Eacute;TRICOS</div></td>
  </tr>
  <tr>
    <td class="titulo-fnormal"><div align="center">N&deg;</div></td>
    <td class="titulo-fnormal"><div align="center">ANCHO</div></td>
    <td class="titulo-fnormal"><div align="center">LARGO</div></td>
    <td class="titulo-fnormal"><div align="center">ESPESOR</div></td>
    <td class="titulo-fnormal"><div align="center">M2</div></td>
    <td class="titulo-fnormal"><div align="center">M3</div></td>
    <td class="titulo-fnormal"><div align="center">TON</div></td>
    <td class="titulo-fnormal"><div align="center">LTS RC250</div></td>
  </tr>
  <?php
    $tm2=0; $tm3=0; $tton=0; $tlts=0; 
    foreach($estComputos as $computo):
      $tm2+=$computo->m2; 
      $tm3+=$computo->m3; 
      $tton+=$computo->ton; 
      $tlts+=$computo->lts; 
      ?>
    <tr>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->cantidad); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->ancho); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->largo); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->espesor); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->m2); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->m3); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->ton); ?></div></td>
      <td class="fnormal"><div align="center" class="num"> <?php echo formato($computo->lts); ?></div></td>
    </tr>
    <?php endforeach; 
    ?>
    <tr>
      <?php for ($i=0;$i<8;$i++){?>
      <td></td>
      <?php }?>
    </tr>
    <tr>
      <td></td><td></td><td></td>
      <td bgcolor="#8db4e2">
        <div align="center" class="titulo-fnormal">TOTAL</div>
      </td>
      <td bgcolor="#8db4e2">
        <div align="center" class="titulo-fnormal"><?php echo $tm2;?></div>
      </td>
      <td bgcolor="#8db4e2">
        <div align="center" class="titulo-fnormal"><?php echo $tm3;?></div>
      </td>
      <td bgcolor="#8db4e2">
        <div align="center" class="titulo-fnormal"><?php echo $tton;?></div>
      </td>
      <td bgcolor="#8db4e2">
        <div align="center" class="titulo-fnormal"><?php echo $tlts;?></div>
      </td>
    </tr>
    <tr>
      <td colspan="8" bgcolor="#CCCCCC"><div align="center" class="titulo-fnormal">MATERIALES A UTILIZAR PARA LA COLOCACI&Oacute;N DEL PAVIMENTO FLEXIBLE</div></td>
    </tr>
    <tr>
      <td colspan="4" class="titulo-fnormal"><div align="center">DESCRIPCI&Oacute;N</div></td>
      <td class="fnormal"><div align="center">UND.</div></td>
      <td class="fnormal"><div align="center">CANTIDAD</div></td>
      <td class="fnormal"><div align="center">PRECIO</div></td>
      <td class="fnormal"><div align="center">SUB-TOTAL</div></td>
    </tr>
    <tr>
      <td colspan="4" class="titulo-fnormal"><div align="center">ASFALTO CALIENTE</div></td>
      <td class="fnormal"><div align="center">TON</div></td>
      <td class="fnormal"><div align="center"><?php echo $tton;?></div></td>
      <td class="fnormal"><div align="center">0</div></td>
      <td class="fnormal"><div align="center">0</div></td>
    </tr>
    <tr>
      <td colspan="4" class="titulo-fnormal"><div align="center">RC-250</div></td>
      <td class="fnormal"><div align="center">LTS</div></td>
      <td class="fnormal"><div align="center"><?php echo $tlts; ?></div></td>
      <td class="fnormal"><div align="center">0</div></td>
      <td class="fnormal"><div align="center">0</div></td>
    </tr>
    <tr>
      <td colspan="6" class="titulo-fnormal"><div align="center"></div></td>
      <td bgcolor="#8db4e2" class="titulo-fnormal"><div align="center">TOTAL</div></td>
      <td bgcolor="#8db4e2" class="titulo-fnormal"><div align="center"><?php echo 0; ?></div></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">MAQINARIA Y EQUIPOS</div></td>
      <td colspan="3" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">PERSONAL</div></td>
      <td colspan="2" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">DIAS DE TRABAJO</div></td>
    </tr>
    <tr>
      <td colspan="3" valign="middle"><div align="center" class="fnormal">
      <?php foreach ($estMaquinaria as $maquinaria): 
        echo $maquinaria->descripcion.'. ';
      endforeach;
      ?></div></td>
      <td colspan="3" valign="middle"><div align="center" class="fnormal">
      <?php $i=0; 
      foreach ($estPersonal as $personal): 
        echo $cantPersonal[$i]->cantidad.' '.$personal->descripcion.'. ';
        $i++;
      endforeach;
      ?></div></td>
      <td colspan="2" valign="middle" ><div align="center" class="fnormal"></div></td>
    </tr>
    <tr>
      <td colspan="3" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">CROQUIS DE UBICACI&Oacute;N</div></td>
      <td colspan="5" bgcolor="#CCCCCC" valign="middle"><div align="center" class="titulo-fnormal">MEMORIA FOTOGR&Aacute;FICA</div></td>
    </tr>
    <tr rowspan="5">
      <td colspan="3" height="220"></td> <!-- IMPRIMIR IMAGEN DE CROQUIS -->
      <td colspan="5" height="220"></td> <!-- COLOCAR MEMORIA FOTOGRAFICA -->
    </tr>
</table>