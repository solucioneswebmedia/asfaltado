<?php
/* @var $this InspeccionesController */
/* @var $model Inspeccion */

$this->breadcrumbs=array(
	'Inspecciones'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Crear Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Actualizar Inspecci&oacute;n', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Inspecci&oacute;n', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Realmente desea eliminar esta inspección? ATENCIÓN: Eliminar los datos implica la perdida de datos como cómputos, estimaciones y fotografías asociadas.')),
	array('label'=>'Administrar Inspecciones', 'url'=>array('index')),
	array('label'=>'Programaci&oacute;n de Inspecciones', 'url'=>array('programacion')),
	array('label'=>'Informe de Inspección', 'url'=>array('excel', 'id'=>$model->id)),
);
?>

<h1>Ver Inspecci&oacute;n #<?php echo $model->numero; ?></h1>

<?php
	$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'numero',
		//array(
		//	'name'=>'solicitudInspeccion',
		//	'value'=>(is_null($model->solicitudInspeccion))?"S/N":$model->solicitudInspeccion->numero,),
		//'usuario_usuario',
		'parroquia',
		array('name'=>'direccion',
				'value'=>$model->solicitudInspeccion->direccion,
			),
		//'fecha',
		array(
            'name'=>'fecha',
            'value'=>date("d-m-Y", strtotime( $model->fecha)),
                ),
		'tipo_obra',
		'asunto',
		'problematica',
		'descripcion',
		'recomendaciones',
		'pto_ref',
		'material_bacheo',
		array(
			'name'=>'croquis',
			'value'=>($model->croquis=="")?"No Asignado":"<a class=\"link-fotos\" href=\"".Yii::app()->baseUrl.'/'.$model->croquis."\" ><img src=\"".Yii::app()->baseUrl.'/'.$model->croquis."\" style=\"height:100px;width:auto;\"/></a> <a href=\"eliminarc?id=".$model->id."\">Borrar</a>",
			'type'=>"html",
			),
		'tipo_adm',
		'latitud',
		'longitud',
		'semana',
		array(
			'name'=>'cuadrilla',
			'value'=>(is_null($model->cuadrilla))?"No Asignada":Personal::model()->findByPk($model->cuadrilla)->descripcion,
			),
		array(
			'name'=>'realizada',
			'value'=>($model->realizada=="1")?"Si":"No",
			),
	),
)); ?>
<h2>Detalles</h2>
<?php
$test="test";
$this->widget('zii.widgets.jui.CJuiAccordion', array(
		'panels'=>array(
			'C&oacute;mputos M&eacute;tricos'=>$this->renderPartial('_displayMetricos', array('model'=>$model), true),
			'Estimaci&oacute;n de Maquinaria'=>$this->renderPartial('_displayMaquinaria', array('model'=>$model), true),
			'Estimaci&oacute;n de Personal'=>$this->renderPartial('_displayPersonal', array('model'=>$model), true),
			'Fotograf&iacute;as'=>$this->renderPartial('_displayFotos',array('model'=>$model), true),
			'Dias programados de ejecución'=>$this->renderPartial('_displayDias', array('model'=>$model), true),
			// panel 5 contains the content rendered by a partial view
			//'panel 5'=>$this->renderPartial('_partial',null,true),
		),
		// additional javascript options for the accordion plugin
		'options'=>array(
			'animated'=>'bounceslide',
			'heightStyle'=>'content',
		),
	));
?>
