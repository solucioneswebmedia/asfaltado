<?php
/* @var $this SolicitudesInspeccionController */
/* @var $data SolicitudInspeccion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_solicitante')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_solicitante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_contacto')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_contacto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('procedencia')); ?>:</b>
	<?php echo CHtml::encode($data->procedencia); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('semana')); ?>:</b>
	<?php echo CHtml::encode($data->semana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prioridad')); ?>:</b>
	<?php echo CHtml::encode($data->prioridad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realizada')); ?>:</b>
	<?php echo CHtml::encode($data->realizada); ?>
	<br />

	*/ ?>

</div>