<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'solicitud-inspeccion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'numero'); ?>
		<?php echo $form->textField($model,'numero',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_solicitante'); ?>
		<?php echo $form->textField($model,'nombre_solicitante',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nombre_solicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'name'=>'SolicitudInspeccion[fecha]',
				'id'=>'SolicitudInspeccion_fecha',
				//'value'=>(isset($model->fecha)&& $model->fecha!='')?date('d/m/Y',strtotime($model->fecha)):'',
				'value'=>(isset($model->fecha))?date("d-m-Y", strtotime( $model->fecha)):date("d-m-Y"),
				'language'=> 'es',
				'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
				),
				'options'=> array(
					'dateFormat'=>'dd-mm-yy',
					'altField' => '',
					'altFormat' => 'dd-mm-yy',
				),
			));
		?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_contacto'); ?>
		<?php echo $form->textField($model,'telefono_contacto',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'telefono_contacto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procedencia'); ?>
		<?php echo $form->textArea($model,'procedencia',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'procedencia'); ?>
	</div>
	<?php if (!($model->isNewRecord)){?>

	<div class="row">
		<?php echo $form->labelEx($model,'realizada'); ?>
		<?php echo $form->checkBox($model,'realizada'); ?>
		<?php echo $form->error($model,'realizada'); ?>
	</div>
	<?php } ?>
	<div class="row buttons">
		<br/>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->