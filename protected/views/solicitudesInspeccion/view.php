<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Solicitudes Inspeccion'=>array('index'),
	$model->numero,
);

$this->menu=array(
	array('label'=>'Crear Solicitud de Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Actualizar Solicitud de Inspecci&oacute;n', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Solicitud de Inspecci&oacute;n', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar esta solicitud?')),
	array('label'=>'Administrar Solicitudes de Inspecci&oacute;n', 'url'=>array('index')),
);
?>

<h1>Ver Solicitud de Inspecci&oacute;n <?php echo $model->numero; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'numero',
		'nombre_solicitante',
		'direccion',
		array(
            'name'=>'fecha',
            'value'=>date("d-m-Y", strtotime( $model->fecha)),
            ),
		'telefono_contacto',
		'procedencia',
		/*'semana',
		'prioridad',
		'realizada',*/
	),
)); ?>
