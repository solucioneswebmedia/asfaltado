<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Solicitudes de Inspeccion'=>array('index'),
	'Programacion',
);

$this->menu=array(
	array('label'=>'Administrar Solicitudes de Inspecci&oacute;n', 'url'=>array('index')),
);

?>

<h1>Programaci&oacute;n de solicitudes de inspecci&oacute;n</h1>

<?php 

	$semanas=array();
	for ($i=SolicitudInspeccion::model()->getSemanaMinima()->semana; $i<53; $i++){
		if($i==0){
			$i++;
		}
		$semanas[$i]=$i;
	}
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'solicitud-inspeccion-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->getSolicitudesNuevas(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'numero',
		//'nombre_solicitante',
		'direccion',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		//'telefono_contacto',		
		'procedencia',
		array(
			'class'=>'editable.EditableColumn',
			'name'=>'semana',
			'editable'=> array(
				'title'=>'Seleccionar semana',
				'type'=>'select',
				'attribute'=>'semana',
				'url'=>$this->createUrl('solicitudesInspeccion/semana'),
				'source'=>$semanas,
				/*'options' => array( //custom display
					'display' => 'js: function(value, sourceData) {
					var selected = $.grep(sourceData, function(o){ return value == o.value; });
					if((selected!="")){
						$(this).text(selected[0].text);
					}else{
						$(this).text("Seleccione semana");	
					}
					}'
					),*/
				),
			),
		array(
			'class'=>'editable.EditableColumn',
			'name'=>'prioridad',
			'editable'=> array(
				'title'=>'Seleccionar Fecha',
				'type'=>'date',
				'attribute'=>'prioridad',
				'url'=>$this->createUrl('solicitudesInspeccion/prioridad'),
				'format'      => 'yyyy-mm-dd', //format in which date is expected from model and submitted to server
        		'viewformat'  => 'dd/mm/yyyy', //format in which date is displayed
				'options' => array(
					'datepicker' => array('language' => 'es'), 
					'success' => 'js: function(response, newValue) {
						var msg="";
						response=JSON.parse(response);
					    if(response.exito==true) ;
					    else 
					    {
					    	$.each(response.errors,function (k,v){msg+=v+\',\';});
					    	return msg;
					    }
					}'
					),
				),
			),
	),
)); 
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap-datepicker.es.js', CClientScript::POS_END);
?>
