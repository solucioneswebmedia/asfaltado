<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Solicitudes Inspeccion'=>array('index'),
	$model->numero=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Solicitud de Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Ver Solicitud de Inspecci&oacute;n', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Solicitud de Inspecci&oacute;n', 'url'=>array('index')),
);
?>

<h1>Actualizar Solicitud de Inspecci&oacute;n <?php echo $model->numero; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>