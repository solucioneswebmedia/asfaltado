<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Solicitudes Inspeccion'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Solicitudes de Inspecci&oacute;n', 'url'=>array('index')),
);
?>

<h1>Crear Solicitud de Inspecci&oacute;n</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>