<?php
/* @var $this SolicitudesInspeccionController */
/* @var $model SolicitudInspeccion */

$this->breadcrumbs=array(
	'Solicitudes de Inspeccion'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Solicitud de Inspecci&oacute;n', 'url'=>array('create')),
	array('label'=>'Programaci&oacute;n de Solicitudes de Inspecci&oacute;n', 'url'=>array('programacion'), 'visible'=>!(Yii::app()->user->isOficina()),),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#solicitud-inspeccion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Solicitudes de Inspecci&oacute;n</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'solicitud-inspeccion-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'numero',
		'nombre_solicitante',
		'direccion',
		array(
			'name'=>'fecha',
			'header'=>'Fecha', // Este es el label
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		'telefono_contacto',		
		'procedencia',/*
		'semana',
		'prioridad',
		*/
		array(
            'name'=>'realizada',
            'header'=>'Realizada',
            'filter'=>array('1'=>'Si','0'=>'No'),
            'value'=>'($data->realizada=="1")?("Si"):("No")'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
