<?php
/* @var $this ComputosMetricosController */
/* @var $model ComputoMetrico */

$this->breadcrumbs=array(
	'Computos Metricos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo C&oacute;mputo M&eacute;trico', 'url'=>array('create')),
	array('label'=>'Actualizar C&oacute;mputo M&eacute;trico', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar C&oacute;mputo M&eacute;trico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea eliminar este registro?')),
	array('label'=>'Administrar C&oacute;mputos M&eacute;tricos', 'url'=>array('index')),
);
?>

<h1>Ver C&oacute;mputo M&eacute;trico #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'inspecciones_id',
		'ancho',
		'largo',
		'espesor',
	),
)); ?>
