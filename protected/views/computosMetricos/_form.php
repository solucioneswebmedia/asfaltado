<?php
/* @var $this ComputosMetricosController */
/* @var $model ComputoMetrico */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'computo-metrico-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'inspecciones_id'); ?>
		<?php echo $form->dropDownList($model,'inspecciones_id', CHtml::listData(Inspeccion::model()->findAll(), 'id', 'numero' ) ); ?>
		<?php echo $form->error($model,'inspecciones_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ancho'); ?>
		<?php echo $form->textField($model,'ancho'); ?>
		<?php echo $form->error($model,'ancho'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'largo'); ?>
		<?php echo $form->textField($model,'largo'); ?>
		<?php echo $form->error($model,'largo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'espesor'); ?>
		<?php echo $form->textField($model,'espesor'); ?>
		<?php echo $form->error($model,'espesor'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->