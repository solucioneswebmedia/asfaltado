<?php
/* @var $this ComputosMetricosController */
/* @var $model ComputoMetrico */

$this->breadcrumbs=array(
	'Computos Metricos'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar C&oacute;mputos M&eacute;tricos', 'url'=>array('index')),
);
?>

<h1>Nuevo C&oacute;mputo M&eacute;trico</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>