<?php
/* @var $this ComputosMetricosController */
/* @var $model ComputoMetrico */

$this->breadcrumbs=array(
	'Computos Metricos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo C&oacute;mputo M&eacute;trico', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#computo-metrico-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Computos Metricos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'computo-metrico-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'inspecciones_id',
		'ancho',
		'largo',
		'espesor',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
