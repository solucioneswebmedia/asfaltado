<?php
/* @var $this ComputosMetricosController */
/* @var $model ComputoMetrico */

$this->breadcrumbs=array(
	'Computos Metricos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	
	array('label'=>'Nuevo C&oacute;mputo M&eacute;trico', 'url'=>array('create')),
	array('label'=>'Ver C&oacute;mputo M&eacute;trico', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar C&oacute;mputos M&eacute;tricos', 'url'=>array('admin')),
);
?>

<h1>Actualizar C&oacute;mputo M&eacute;trico <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>