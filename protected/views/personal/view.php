<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Personal'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Nuevo Personal', 'url'=>array('create')),
	array('label'=>'Actualizar Personal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Personal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Desea borrar este Personal?')),
	array('label'=>'Administrar Personal', 'url'=>array('index')),
);
?>

<h1>Ver detalle de <?php echo $model->descripcion; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
	),
)); ?>
