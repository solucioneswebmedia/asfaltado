<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Personal'=>array('index'),
	'Nuevo',
);

$this->menu=array(
	array('label'=>'Administrar Personal', 'url'=>array('index')),
);
?>

<h1>Nuevo Personal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>