<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Personal'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Nuevo Personal', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#personal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Personal</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'personal-grid',
	'dataProvider'=>$model->search(),
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'codigo',
		'descripcion',
		'precio',
		'unidades.nombre_unidad',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
