<?php
/* @var $this PersonalController */
/* @var $model Personal */

$this->breadcrumbs=array(
	'Personal'=>array('index'),
	$model->codigo=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Nuevo Personal', 'url'=>array('create')),
	array('label'=>'Ver Personal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar Personal', 'url'=>array('index')),
);
?>

<h1>Actualizar Personal <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>