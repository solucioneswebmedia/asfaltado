<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */

$this->breadcrumbs=array(
	'Trabajos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Administrar Trabajos', 'url'=>array('index')),
);
?>

<h1>Crear Trabajo</h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'metrico'=>$metrico,
	'validatedMetricos'=>$validatedMetricos,
	'maq'=>$maq,
	'validatedMaquinas'=>$validatedMaquinas,
	'per'=>$per,
	'validatedPersonal'=>$validatedPersonal,
	)); ?>