<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */

$this->breadcrumbs=array(
	'Trabajos'=>array('index'),
	'Hoja de Resultados',
);

$this->menu=array(
	array('label'=>'Administrar Obras', 'url'=>array('index')),
);

?>

<h1>Exportar hoja de resultados</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'programacion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary(array_merge(array($model))); ?>


		<p>Seleccione la semana de la que desea obtener la hoja de resultados:</p>
		<p><?php
			//$objYear = Trabajo::model()->findAllBySql("SELECT DISTINCT YEAR(fecha) AS fecha FROM trabajo ORDER BY fecha DESC");

			$objSemanas = Trabajo::model()->findAllBySql("SELECT DISTINCT WEEK(fecha,3) as semana FROM trabajo WHERE YEAR(fecha)=YEAR(NOW()) ORDER BY semana DESC");
			foreach ($objSemanas as $obj) {
				$arraySemanas[]=array('id'=>$obj->semana, 'semana'=>$obj->semana);
			}		
			echo $form->dropDownList($model,'semana', CHtml::listData($arraySemanas,'id', 'semana')); 
		?></p>
		<p><?php echo $form->error($model,'semana'); ?></p>
	
		<?php 
			$this->widget('zii.widgets.jui.CJuiButton',array(
					    'buttonType'=>'submit',
					    'name'=>'submit',
					    'caption'=>'Aceptar',
					    'htmlOptions'=>array(
	        				'style'=>'background:#468847;color:#ffffff;'					
					    ))
			);
		 ?>
	

<?php $this->endWidget(); ?>
