<?php
/* @var $this TrabajoController */
/* @var $model Trabajo */
/* @var $form CActiveForm */

	$this->breadcrumbs=array(
		'Trabajos'=>array('index'),
		$model->numero=>array('view','id'=>$model->id),
		'Asignar Inversion',
	);

	$this->menu=array(
		array('label'=>'Ver Trabajo', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Administrar Trabajos', 'url'=>array('index')),
	);
?>

<h1>Asignar inversión a Trabajo # <?php echo $model->numero; ?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trabajo-inversion-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'suministro'); ?>
		<?php echo $form->textField($model,'suministro'); ?>
		<?php echo $form->error($model,'suministro'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'transporte'); ?>
		<?php echo $form->textField($model,'transporte'); ?>
		<?php echo $form->error($model,'transporte'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'colocacion'); ?>
		<?php echo $form->textField($model,'colocacion'); ?>
		<?php echo $form->error($model,'colocacion'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Asignar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->