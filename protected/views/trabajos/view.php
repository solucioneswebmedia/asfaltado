<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */

$this->breadcrumbs=array(
	'Trabajos'=>array('index'),
	$model->numero,
);
$this->menu=array(
	array('label'=>'Crear Trabajo', 'url'=>array('create'), 'visible'=>!(Yii::app()->user->isJefe()) ),
	array('label'=>'Actualizar Trabajo', 'url'=>array('update', 'id'=>$model->id), 'visible'=>!(Yii::app()->user->isJefe()), ),
	array('label'=>'Borrar Trabajo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Realmente desea eliminar este trabajo? ATENCIÓN: Eliminar los datos implica la perdida de datos como cómputos, detalle de trabajo y fotografías asociadas.'), 'visible'=>!(Yii::app()->user->isJefe()) ),
	//array('label'=>'Asignar inversi&oacute;n', 'url'=>array('inversion', 'id'=>$model->id), 'visible'=>!(Yii::app()->user->isJefe()), ),
	array('label'=>'Administrar Trabajos', 'url'=>array('index')),
	array('label'=>'Informe Técnico', 'url'=>array('tecnico', 'id'=>$model->id)),
	array('label'=>'Informe de Cierre', 'url'=>array('cierre', 'id'=>$model->id)),
	array('label'=>'Papeletas de Asfalto', 'url'=>array('/papeletasAsfalto/asfalto', 'id'=>$model->id)),
);
?>

<h1>Ver Obra #<?php echo $model->numero; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'numero',
		'inspecciones.numero',
		//'usuario.usuario',
		array(
			'name'=>'usuario',
			'value'=>$model->usuario->nombres.' '.$model->usuario->apellidos,
			),
		array(
            'name'=>'fecha',
            'value'=>date("d-m-Y", strtotime( $model->fecha)),
                ),
		'denominacion',
		'problematica',
		'tipo_reparacion',
		'descripcion',
		'beneficiario',
		'observaciones',
		//'suministro',
		//'transporte',
		//'colocacion',
		'inversion',
		'realizada:boolean',
	),
)); ?>
<h2>Detalles</h2>
<?php
$test="test";
$this->widget('zii.widgets.jui.CJuiAccordion', array(
		'panels'=>array(
			'C&oacute;mputos M&eacute;tricos'=>$this->renderPartial('_displayMetricos', array('model'=>$model), true),
			'Detalle de Maquinaria'=>$this->renderPartial('_displayMaquinaria', array('model'=>$model), true),
			'Detalle de Personal'=>$this->renderPartial('_displayPersonal', array('model'=>$model), true),
			'Papeletas de Asfalto'=>$this->renderPartial('_displayPapeletas', array('model'=>$model), true),
			'Fotograf&iacute;as'=>$this->renderPartial('_displayFotos',array('model'=>$model), true),
			// panel 5 contains the content rendered by a partial view
			//'panel 5'=>$this->renderPartial('_partial',null,true),
		),
		// additional javascript options for the accordion plugin
		'options'=>array(
			'animated'=>'bounceslide',
			'heightStyle'=>'content',
		),
	));
?>
