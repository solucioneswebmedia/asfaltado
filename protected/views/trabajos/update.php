<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */

$this->breadcrumbs=array(
	'Trabajos'=>array('index'),
	$model->numero=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Crear Trabajo', 'url'=>array('create')),
	array('label'=>'Ver Trabajo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Asignar inversi&oacute;n', 'url'=>array('inversion', 'id'=>$model->id),'visible'=>!( Yii::app()->user->isJefe() ), ),
	array('label'=>'Administrar Trabajos', 'url'=>array('index')),
);
?>

<h1>Actualizar Trabajo <?php echo $model->numero; ?></h1>

<?php $this->renderPartial('_form', array(
	'model'=>$model,
	'metrico'=>$metrico,
	'validatedMetricos'=>$validatedMetricos,
	'maq'=>$maq,
	'validatedMaquinas'=>$validatedMaquinas,
	'per'=>$per,
	'validatedPersonal'=>$validatedPersonal,
	)); ?>