<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */

$this->breadcrumbs=array(
	'Trabajos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	array('label'=>'Crear Trabajo', 'url'=>array('create')),
	array('label'=>'Hoja de Resultados', 'url'=>array('resultados')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#trabajo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Administrar Trabajos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'trabajo-grid',
	'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'numero',
		//'inspecciones.numero',
		array(
			'name'=>'inspeccion_search',
			'value'=>'$data->inspecciones->numero',
			'filter'=>CHtml::activeTextField($model, 'inspeccion_search')
			),
		//'usuario_usuario',
		array(
			'name'=>'fecha',
			'header'=>'Fecha',
			'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
		),
		
		'denominacion',
		'beneficiario',
		/*'observaciones',
		'inversion',
		*/
		array(
            'name'=>'realizada',
            'header'=>'Realizada',
            'filter'=>array('1'=>'Si','0'=>'No'),
            'value'=>'($data->realizada=="1")?("Si"):("No")'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
