<?php
$computosFormConfig = array(
  'elements'=>array(
    'cantidad'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'ancho'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'largo'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),        
    'espesor'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),
    'dia_trabajo'=>array(        
        'type'=>'zii.widgets.jui.CJuiDatePicker',
        'language'=>'es',
        'value'=>date("d-m-Y"),
        'options'=>array(
            'showAnim'=>'fold',
            'options'=> array(
                'dateFormat'=>'dd-mm-yy',
                'altFormat' => 'dd-mm-yy',
                )
            )
        ),
    
   ) //fin de array elements
  );

$this->widget('multimodel.MultiModelForm',array(
    'tableView' => true,
    'jsAfterNewId' => MultiModelForm::afterNewIdDatePicker($computosFormConfig['elements']['dia_trabajo']),
    'addItemText'=>'<i class="fa fa-plus fa-fw"></i> Agregar Nuevo',
    'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
    'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $computosFormConfig, //the form configuration array
        'model' => $metrico, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedMetricos,

        //array of member instances loaded from db
        'data' => $metrico->findAll('trabajo_id=:groupId', array(':groupId'=>$model->id)),
        ));
?>