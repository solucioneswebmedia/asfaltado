<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'trabajo-grid',
    'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
    'dataProvider'=>$model->getPapeletas(),
    'filter'=>null,
    'columns'=>array(
        //'id',
        'codigo',
        'cantidad',
        'proveedor.nombre',
        array(
            'name'=>'fecha',
            'header'=>'Fecha',
            'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->fecha))'
        ),
    ),
)); ?>