<?php
$maquinas=array(""=>'Seleccione...') + CHtml::listData(MaquinariaExistente::model()->findAll(),'id', 'descripcion');  
$units=array(""=>'Seleccione...') + CHtml::listData(Unidad::model()->findAll(),'id', 'nombre_unidad');  
$maquinasFormConfig = array(
  'elements'=>array(
    'maquinaria_existente_id'=>array(
        'type'=>'dropdownlist',
        'items'=>$maquinas,
        ),
    'dia_trabajo'=>array(
        'type'=>'zii.widgets.jui.CJuiDatePicker',
        'language'=>'es',
        'value'=>date("d-m-Y"),
        'options'=>array(
            'showAnim'=>'fold',
            'options'=> array(
                'dateFormat'=>'dd-mm-yy',
                'altField' => '',
                'altFormat' => 'dd-mm-yy',
                )
            )
        ),
    'unidades_id'=>array(
        'type'=>'dropdownlist',
        'items'=>$units,
        ),  
    'extra'=>array(
        'type'=>'text',
        'maxlength'=>10,
        ),          
    ));

$this->widget('ext.multimodelform.MultiModelForm',array(
        'tableView' => true,
        'jsAfterNewId' => MultiModelForm::afterNewIdDatePicker($maquinasFormConfig['elements']['dia_trabajo']),
        'addItemText'=>'<i class="fa fa-plus fa-fw"></i> Agregar Nuevo',
        'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $maquinasFormConfig, //the form configuration array
        'model' => $maq, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedMaquinas,
 
        //array of member instances loaded from db
        'data' => $maq->findAll('trabajo_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>