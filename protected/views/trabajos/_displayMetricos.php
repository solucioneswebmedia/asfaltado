<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'trabajo-grid',
    'htmlOptions'=>array('class'=>'table table-hover table-condensed'),
    'dataProvider'=>$model->getComputos(),
    'filter'=>null,
    'columns'=>array(
        //'id',
        'cantidad',
        'ancho',
        'largo',
        'espesor',
        //'dia_trabajo',
        array(
            'name'=>'dia_trabajo',
            'header'=>'Dia de Trabajo',
            'value'=>'Yii::app()->dateFormatter->format("dd-MM-yyyy",strtotime($data->dia_trabajo))'
        ),
    ),
)); ?>