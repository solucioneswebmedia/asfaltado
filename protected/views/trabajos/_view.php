<?php
/* @var $this TrabajosController */
/* @var $data Trabajo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inspecciones_id')); ?>:</b>
	<?php echo CHtml::encode($data->inspecciones_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proveedor_id')); ?>:</b>
	<?php echo CHtml::encode($data->proveedor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('denominacion')); ?>:</b>
	<?php echo CHtml::encode($data->denominacion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('beneficiario')); ?>:</b>
	<?php echo CHtml::encode($data->beneficiario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inversion')); ?>:</b>
	<?php echo CHtml::encode($data->inversion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('realizada')); ?>:</b>
	<?php echo CHtml::encode($data->realizada); ?>
	<br />

	*/ ?>

</div>