<?php
/* @var $this TrabajosController */
/* @var $model Trabajo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trabajo-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son necesarios.</p>

	<?php  echo $form->errorSummary(array_merge(array($model),$validatedMetricos,$validatedMaquinas,$validatedPersonal)); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'numero'); ?>
		<?php echo $form->textField($model,'numero',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'inspecciones_id'); ?>
		<?php echo $form->dropDownList($model,'inspecciones_id', CHtml::listData(Inspeccion::model()->findAll(), 'id', 'numero' ) ); ?>
		<?php echo $form->error($model,'inspecciones_id'); ?>
	</div>

	<div class="row">
			<?php echo $form->labelEx($model,'usuario_id'); ?>
			<?php echo Yii::app()->user->id,$form->hiddenField($model,'usuario_id',array('value'=>Usuario::model()->find("usuario='".Yii::app()->user->id."'")->id)); ?>
			<?php echo $form->error($model,'usuario_id'); ?>
		</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model' => $model,
				'name'=>'Trabajo[fecha]',
				'id'=>'Trabajo_fecha',
				//'value'=>(isset($model->fecha)&& $model->fecha!='')?date('d/m/Y',strtotime($model->fecha)):'',
				'value'=>(isset($model->fecha))?date("d-m-Y", strtotime( $model->fecha)):date("d-m-Y"),
				'language'=> 'es',
				'htmlOptions' => array(
					'size' => '10',         // textField size
					'maxlength' => '10',    // textField maxlength
				),
				'options'=> array(
					'dateFormat'=>'dd-mm-yy',
					'altField' => '',
					'altFormat' => 'dd-mm-yy',
				),
			));
		?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'denominacion'); ?>
		<?php echo $form->textArea($model,'denominacion',array('rows'=>6, 'cols'=>100)); ?>
		<?php echo $form->error($model,'denominacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textArea($model,'direccion',array('rows'=>6, 'cols'=>100)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'problematica'); ?>
		<?php echo $form->textArea($model,'problematica',array('rows'=>6, 'cols'=>100)); ?>
		<?php echo $form->error($model,'problematica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_reparacion'); ?>
		<?php echo $form->textArea($model,'tipo_reparacion',array('rows'=>6, 'cols'=>100)); ?>
		<?php echo $form->error($model,'tipo_reparacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>100)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'beneficiario'); ?>
		<?php echo $form->textField($model,'beneficiario',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'beneficiario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model,'observaciones',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
	</div>
	<?php if (!($model->isNewRecord)) { ?>
		<div class="row">
			<?php echo $form->labelEx($model,'realizada'); ?>
			<?php echo $form->checkBox($model,'realizada'); ?>
			<?php echo $form->error($model,'realizada'); ?>
		</div>
	<?php
	}	
	// INICIO MULTIMODEL
	$sample_text='Libera res publica est status civitatis et aetas';
    $this->widget('zii.widgets.jui.CJuiAccordion', array(
		'panels'=>array(
			'C&oacute;mputos M&eacute;tricos'=>$this->renderPartial('_metricos', array('model'=>$model,'metrico'=>$metrico,'validatedMetricos'=>$validatedMetricos), true),
			'Detalle de Maquinaria'=>$this->renderPartial('_maquinaria', array('model'=>$model,'maq'=>$maq,'validatedMaquinas'=>$validatedMaquinas), true),
			'Detalle de Personal'=>$this->renderPartial('_personal', array('model'=>$model,'per'=>$per,'validatedPersonal'=>$validatedPersonal), true),
			//'Fotograf&iacute;as'=>$sample_text,
			// panel 5 contains the content rendered by a partial view
			//'panel 5'=>$this->renderPartial('_partial',null,true),
		),
		// additional javascript options for the accordion plugin
		'options'=>array(
			'animated'=>'bounceslide',
			'heightStyle'=>'content',
		),
	));


	?>
	<div class="row buttons">
		<br />
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->