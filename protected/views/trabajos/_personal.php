<?php
$personals=array(""=>'Seleccione...') + CHtml::listData(Personal::model()->findAll(),'id', 'descripcion');  
$personalFormConfig = array(
  'elements'=>array(
    'personal_id'=>array(
        'type'=>'dropdownlist',
        'items'=>$personals,
        ),
    'cantidad'=>array(
        'type'=>'text',
        'maxlength'=>5,
        ),  
    'dia_trabajo'=>array(
        'type'=>'zii.widgets.jui.CJuiDatePicker',
        'language'=>'es',
        'value'=>date("d-m-Y"),
        'options'=>array(
            'showAnim'=>'fold',
            'options'=> array(
                'dateFormat'=>'dd-mm-yy',
                'altField' => '',
                'altFormat' => 'dd-mm-yy',
                )
            )
        ),        
    ));

$this->widget('ext.multimodelform.MultiModelForm',array(
        'tableView' => true,
        'jsAfterNewId' => MultiModelForm::afterNewIdDatePicker($personalFormConfig['elements']['dia_trabajo']),
        'addItemText'=>'<i class="fa fa-plus fa-fw"></i> Agregar Nuevo',
        'removeText'=>'<span class="fa-stack fa-lg"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-remove fa-stack-1x fa-inverse"></i></span>',
        'removeConfirm'=>'¿Estas seguro?',
        'formConfig' => $personalFormConfig, //the form configuration array
        'model' => $per, //instance of the form model 
        //if submitted not empty from the controller,
        //the form will be rendered with validation errors
        'validatedItems' => $validatedPersonal,
 
        //array of member instances loaded from db
        'data' => $per->findAll('trabajo_id=:groupId', array(':groupId'=>$model->id)),
    ));
?>