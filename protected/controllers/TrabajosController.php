<?php

class TrabajosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'expression'=>'Yii::app()->user->isAdministrador() || Yii::app()->user->isJefe() || Yii::app()->user->isCoordinador()',
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','create','update','inversion','tecnico','cierre','resultados','eliminar','upload'),
				'expression'=>'Yii::app()->user->isAdministrador() || Yii::app()->user->isCoordinador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Trabajo;
		$metrico= new DetalleTrabajo;
		$maq= new DetalleMaquinaria;
		$per= new DetallePersonal;
		$validatedMetricos=array();
		$validatedMaquinas=array();
		$validatedPersonal=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Trabajo']))
		{
			$model->attributes=$_POST['Trabajo'];
			if((MultiModelForm::validate($metrico,$validatedMetricos,$deleteMetricos) && 
				MultiModelForm::validate($maq,$validatedMaquinas,$deleteMaquinas) &&
				MultiModelForm::validate($per,$validatedPersonal,$deletePersonal)) && 
				$model->save() ){
				//Procesamos computos metricos
				
				$masterValues = array ('trabajo_id'=>$model->id);
				if (MultiModelForm::save($metrico,$validatedMetricos,$deleteMetricos,$masterValues) && 
					MultiModelForm::save($maq,$validatedMaquinas,$deleteMaquinas,$masterValues) &&
					MultiModelForm::save($per,$validatedPersonal,$deletePersonal, $masterValues) ){
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			else{
				echo 'error';
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'metrico'=>$metrico,
			'validatedMetricos'=>$validatedMetricos,
			'maq'=>$maq,
			'validatedMaquinas'=>$validatedMaquinas,
			'per'=>$per,
			'validatedPersonal'=>$validatedPersonal,

		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$metrico= new DetalleTrabajo;
		$maq= new DetalleMaquinaria;
		$per= new DetallePersonal;
		$validatedMetricos=array();
		$validatedMaquinas=array();
		$validatedPersonal=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Trabajo']))
		{
			$model->attributes=$_POST['Trabajo'];
			$masterValues = array ('trabajo_id'=>$model->id);
	        if( //Save the master model after saving valid members
	        	( MultiModelForm::save($metrico,$validatedMetricos,$deleteMetricos,$masterValues) && 
	        		MultiModelForm::save($maq,$validatedMaquinas,$deleteMaquinas,$masterValues) &&
	        		MultiModelForm::save($per,$validatedPersonal,$deletePersonal,$masterValues) ) &&
	        	$model->save() ){
	        	$this->redirect(array('view','id'=>$model->id));
	        }
		}

		$this->render('update',array(
			'model'=>$model,
			'metrico'=>$metrico,
			'validatedMetricos'=>$validatedMetricos,
			'maq'=>$maq,
			'validatedMaquinas'=>$validatedMaquinas,
			'per'=>$per,
			'validatedPersonal'=>$validatedPersonal,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Trabajo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Trabajo']))
			$model->attributes=$_GET['Trabajo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionInversion($id)
	{
	    $model=$this->loadModel($id);

	    // uncomment the following code to enable ajax-based validation
	    /*
	    if(isset($_POST['ajax']) && $_POST['ajax']==='trabajo-inversion-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }
	    */

	    if(isset($_POST['Trabajo']))
	    {
	        $model->attributes=$_POST['Trabajo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		
	    }
	    $this->render('inversion',array(
	    	'model'=>$model
	    ));
	}

	public function actionResultados(){
	    $model=new Trabajo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_POST['Trabajo'])){
			// INICIO SCRIPT EXCEL
			$numSemana=$_POST['Trabajo']['semana'];
			$semana=array('data'=>$numSemana);
			
			// La primera posicion es el lunes de esa semana
	        $arrayFechas[0]=array('data'=>date('Y-m-d',strtotime(date("Y").'W'.$numSemana)));
	        for ($i=0; $i<4; $i++){
	        	$arrayFechas[$i+1]=array('data'=>date( 'Y-m-d' , strtotime('+1 day',strtotime($arrayFechas[$i]['data']))));
	        }
	        $total=0;
	        foreach ($arrayFechas as $f) {
	        	$objObras=DetalleTrabajo::model()->findAllBySql("SELECT DISTINCT trabajo_id FROM detalle_trabajo WHERE dia_trabajo='".$f['data']."'");
	        	if ($objObras!=null){
		        	foreach ($objObras as $o) {
		        			$arrayMaq=[]; $arrayPer=[];
		        			$obra=Trabajo::model()->findByPk($o->trabajo_id);
		        			$objMaq=MaquinariaExistente::model()->findAllBySql("SELECT m.id,m.descripcion FROM maquinaria_existente m
									JOIN detalle_maquinaria d ON d.maquinaria_existente_id=m.id
									WHERE d.trabajo_id=".$o->trabajo_id." AND d.dia_trabajo='".$f['data']."'");
		        			foreach ($objMaq as $maq ) {
		        				$arrayMaq[]=$maq->descripcion;
		        			}
		        			// Para obtener la cantidad de personal se va a sustituir en el objeto el campo id por esa cantidad
		        			$objPer=Personal::model()->findAllBySql("SELECT p.descripcion,d.cantidad as id FROM personal p
									JOIN detalle_personal d ON d.personal_id=p.id
									WHERE d.trabajo_id=".$o->trabajo_id." AND d.dia_trabajo='".$f['data']."'");
		        			foreach ($objPer as $per ) {
		        				$arrayPer[]=$per->id." ".$per->descripcion;
		        			}
		        			$totalDia=PapeletaAsfalto::model()->findBySql("SELECT SUM(cantidad) AS cantidad 
		  														FROM papeleta_asfalto
																WHERE trabajo_id=".$o->trabajo_id." AND fecha='".$f['data']."' AND proveedor_id<>4")->cantidad;
		        		
		        			$total+=$totalDia;
		        			$hoy=new DateTime("now");
		        			$arrayResultado[]=array( 'dia'=>strtoupper(Yii::app()->dateFormatter->format("eeee", $f['data'])),
		        								'fecha'=>date("d/m/Y",strtotime($f['data'])),
		        								'sitio'=>$obra->direccion,
		        								'desc'=>$obra->tipo_reparacion,
		        								'totalMaterial'=>$totalDia,
		        								'maquinaria'=>implode(", ", $arrayMaq),
		        								'personal'=>implode("\n", $arrayPer),
		        								'porcentaje'=>($hoy>=date_create($f['data']))?100:'EN EJECUCIÓN',
		        				);
		        		}
		        }else{
		        	$arrayResultado[]=array( 'dia'=>strtoupper(Yii::app()->dateFormatter->format("eeee", $f['data'])),
		        								'fecha'=>date("d/m/Y",strtotime($f['data'])),
		        								'sitio'=>' ',
		        								'desc'=>' ',
		        								'totalMaterial'=>' ',
		        								'maquinaria'=>' ',
		        								'personal'=>' ',
		        								'porcentaje'=>'' ,
		        				);
		        }
	        }

	        
	        $r = new YiiReport(array('template'=> 'RESULTADOS-SEMANA.xls'));
	        $r->load(array(
	                array(
	                    'id' => 'semana',
	                    'data' => $semana,
	                ),
	                array(
	                    'id' => 'res',
	                    'data' => $arrayResultado,
	                    'repeat'=>true,
                    	'minRows'=>1,
	                ),
	                array(
	                	'id' => 'total',
	                    'data' => array('data'=>$total),
	                	)	             	                
	            )
	        );
	        $phpExcel = $r->getPHPExcel();
		    $sheet = $phpExcel->getActiveSheet();
		    $sheet->getRowDimension(1)->setRowHeight(45);	
	  		$basePath=Yii::app()->params['imagePath'];
	  		$ruta=$basePath.'header.jpg';
		    $objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('Logo');
		    $objDrawing->setDescription('Logo');
		    $objDrawing->setPath($ruta);
		    $objDrawing->setHeight(52);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('A1');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
		    
		    echo $r->render('excel5', 'RESULTADOS-SEMANA-'.$numSemana);
			// FIN SCRIPT EXCEL
		}
		$this->render('resultado',array(
			'model'=>$model,
			));
	
	}

 	public function actionTecnico($id){
    	
    	$model=$this->loadModel($id);	
		$objInspeccion=Inspeccion::model()->findByPk($model->inspecciones_id);
		//var_dump($objInspeccion);
		//exit();
		$objInspeccion->cuadrilla=Personal::model()->findByPk($objInspeccion->cuadrilla)->descripcion;
		$objSolicitud=SolicitudInspeccion::model()->findByPk($objInspeccion->solicitud_inspeccion_id);
		$objComputos=DetalleTrabajo::model()->findAll(array('condition'=>"trabajo_id = $id"));

		$tton=0; $tlts=0; 
		foreach ($objComputos as $c) {
			$tton+=$c->ancho*$c->largo*$c->cantidad*$c->espesor*2.33;
			$tlts+=$c->ancho*$c->largo*$c->cantidad*1.5; 
		}
		$totales=array('totalton'=>$tton,
					'totallts'=>$tlts,
					);
		$detMaquinaria=MaquinariaExistente::model()->findAllBySql("SELECT DISTINCT descripcion FROM detalle_maquinaria d JOIN maquinaria_existente m ON d.maquinaria_existente_id=m.id WHERE d.trabajo_id=$id;");
		$arrayMaquinaria=array();
		foreach ($detMaquinaria as $m) {
			$arrayMaquinaria[]=array('data'=>$m->descripcion,'qty'=>1);
		}
		$detPersonal=DetallePersonal::model()->findAllBySql("SELECT DISTINCT personal_id, cantidad FROM detalle_personal  WHERE trabajo_id=$id;");
		
		$arrayPersonal=array(); 
		foreach ($detPersonal as $p) {
			$arrayPersonal[]=$p->cantidad.' '.$p->personal->descripcion;
		} 

		$objFechas=DetalleTrabajo::model()->findAllBySql("SELECT DISTINCT dia_trabajo FROM detalle_trabajo WHERE trabajo_id=$id");
		foreach ($objFechas as $f) {
			$arrayFechas[]=Yii::app()->dateFormatter->format("dd/MM/yyyy", $f->dia_trabajo);			
		}
        $r = new YiiReport(array('template'=> 'INFORME-TECNICO.xls'));
        $r->load(array(
                array(
                    'id' => 'obra',
                    'data' => $model->attributes,
                ),
                array(
                    'id'=>'solic',
                    'data'=>$objSolicitud->attributes,
                ),
                array(
                    'id'=>'inspe',
                    'data'=>$objInspeccion->attributes,
                ),
                array(
                    'id'=>'total',
                    'data'=>$totales,
                ),
                array(
                    'id'=>'detMaq',
                    'data'=>$arrayMaquinaria,
                    'repeat'=>true,
                    'minRows'=>1,
                ),
                array(
                    'id'=>'detPer',
                    'data'=>array('data'=>implode(", ", $arrayPersonal)),
                ),
                array(
                    'id'=>'fechas',
                    'data'=>array('data'=>implode(", ", $arrayFechas)),
                ),
            )
        );
        $phpExcel = $r->getPHPExcel();
	    $sheet = $phpExcel->getActiveSheet();
	    $sheet->getRowDimension(1)->setRowHeight(45);	
  		$basePath=Yii::app()->params['imagePath'];
  		$ruta=$basePath.'header.jpg';
	    $objDrawing = new PHPExcel_Worksheet_Drawing();
	    $objDrawing->setName('Logo');
	    $objDrawing->setDescription('Logo');
	    $objDrawing->setPath($ruta);
	    $objDrawing->setHeight(52);//Pixeles de altura de logo
	    $objDrawing->setCoordinates('A1');
	    $objDrawing->setResizeProportional(true);
	    $phpExcel->setActiveSheetIndex(0);
	    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
        
	    //Croquis
	    if(isset($objInspeccion->croquis) && $objInspeccion->croquis!=""){
	    	$filePath=substr($objInspeccion->croquis, 7);
	    	$objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('Croquis');
		    $objDrawing->setDescription('Croquis');
		    $objDrawing->setPath(Yii::app()->params['imagePath'].$filePath);
		    $objDrawing->setHeight(400);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('J2');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
	    }

 		$objFotos=FotoTrabajo::model()->findAll("trabajo_id=$id");
	    foreach ($objFotos as $foto) {
	    	$filePath=substr($foto->ruta, 7);
	    	$objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('FotoTrabajo');
		    $objDrawing->setDescription('FotoTrabajo');
		    $objDrawing->setPath(Yii::app()->params['imagePath'].$filePath);
		    $objDrawing->setHeight(400);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('J2');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
	    }

        echo $r->render('excel5', 'INFORME-TECNICO-'.$model->numero);
        //echo $r->render('excel2007', 'Students');
        //echo $r->render('pdf', 'Students');*/
        
    }//actionExcel method end

    public function actionCierre($id){
    	
    	$model=$this->loadModel($id);
		$objInspeccion=Inspeccion::model()->findByPk($model->inspecciones_id);
		$objSolicitud=SolicitudInspeccion::model()->findByPk($objInspeccion->solicitud_inspeccion_id);
		$objComputos=DetalleTrabajo::model()->findAll(array('condition'=>"trabajo_id = $id"));
		$numerus=0; $totalMat=0;
		$objProveedores=Proveedor::model()->findAllBySql("SELECT DISTINCT p.id AS id,p.nombre AS nombre FROM proveedor p JOIN papeleta_asfalto pa ON pa.proveedor_id=p.id WHERE trabajo_id=$id AND p.nombre<>'ASFADIL';");
		$arrayMaterial=array();

		foreach ($objProveedores as $prov ) {
			$numerus+=1;
			$varPrecio=Material::model()->find( array('condition'=>'proveedor_id=:proveedorId',
													'params'=>array(':proveedorId'=>$prov->id)) )->precio;
			$varCantidad=PapeletaAsfalto::model()->findBySql(
										"SELECT SUM(cantidad) as cantidad FROM papeleta_asfalto WHERE trabajo_id=$id AND proveedor_id=$prov->id")->cantidad;
			$totalMat+=floatval($varCantidad*$varPrecio);
			$arrayMaterial[]=array('nombre'=>$objInspeccion->material_bacheo.' ('.$prov->nombre.')',
									'indice'=>$numerus,
									'cantidad'=>$varCantidad,
									'precio'=>$varPrecio,
									'dias'=>intval( PapeletaAsfalto::model()->count( array(
													'condition'=> 'trabajo_id=:trabajoId AND proveedor_id=:proveedorId',
													'params'=>array( ':trabajoId'=>$id,
																		'proveedorId'=>$prov->id)
													) 
												)
										),
									'total'=>floatval($varCantidad*$varPrecio),
									);
		}

		$varPrecio=Material::model()->find( array('condition'=>'proveedor_id=:proveedorId',
													'params'=>array(':proveedorId'=>4)) )->precio;
		$varCantidad=PapeletaAsfalto::model()->findBySql(
									"SELECT SUM(cantidad) as cantidad FROM papeleta_asfalto WHERE trabajo_id=$id AND proveedor_id=4")->cantidad;
		$numerus+=1;
		$totalMat+=floatval($varCantidad*$varPrecio);
		$arrayMaterial[]=array('nombre'=>'ASFALTO LIQUIDO RC-250',
								'indice'=>$numerus,
								'cantidad'=>$varCantidad,
								'precio'=>$varPrecio,
								'dias'=>intval( PapeletaAsfalto::model()->count( array(
												'condition'=> 'trabajo_id=:trabajoId AND proveedor_id=:proveedorId',
												'params'=>array( ':trabajoId'=>$id,
																	'proveedorId'=>4)
												) 
											)
									),
								'total'=>floatval($varCantidad*$varPrecio),
								);
		$detMaquinaria=MaquinariaExistente::model()->findAllBySql("SELECT DISTINCT m.id AS id,descripcion,precio FROM detalle_maquinaria d JOIN maquinaria_existente m ON d.maquinaria_existente_id=m.id WHERE d.trabajo_id=$id;");
		$arrayMaquinaria=array();
		foreach ($detMaquinaria as $m) {
			$numerus+=1;
			$countDias=intval( DetalleMaquinaria::model()->count(
									array('condition'=>"trabajo_id=:trabajoId AND maquinaria_existente_id=:maqId",
									'params'=>array(':trabajoId'=>$id,
														':maqId'=>$m->id)
									)
								)
			);
			
			$arrayMaquinaria[]=array('data'=>$m->descripcion,
								'indice'=>$numerus,
								'qty'=>1,
								'totalDias'=>$countDias,
								'precio'=>$m->precio,
								'totalMaq'=>floatval($m->precio)*$countDias,
			);
			$totalMat+=floatval($m->precio)*$countDias;
		}
		$objPersonal=Personal::model()->findAllBySql("SELECT DISTINCT p.id as id, p.descripcion as descripcion, p.precio as precio FROM personal p JOIN detalle_personal d ON d.personal_id=p.id  WHERE trabajo_id=$id;");
		$arrayPersonal=array(); $totalPer=0;
		foreach ($objPersonal as $p) {
			$numerus+=1;
			$varDias=intval( DetallePersonal::model()->count( array( 'condition'=>"trabajo_id=:trabajoId AND personal_id=:perId",
																'params'=>array(':trabajoId'=>$id,
																				':perId'=>$p->id)
																)
														) 
			);
			$varCantidad=intval(DetallePersonal::model()->findBySql("SELECT DISTINCT cantidad FROM detalle_personal WHERE personal_id=$p->id AND trabajo_id=$id;")->cantidad
				);			
			$totalPer+=$varDias*$p->precio*$varCantidad;
			$arrayPersonal[]=array('indice'=>$numerus,
								'nombre'=>$p->descripcion,
								'dias'=>$varDias,
								'cantidad'=>$varCantidad,
								'precio'=>$p->precio,
								'total'=>$p->precio*$varDias*$varCantidad,
				);
		}

		$arrayTotales=array( 'subtotalMat'=>$totalMat,
								'ivaMat'=>floatval($totalMat*0.12),
								'totalConIva'=>floatval($totalMat*1.12),
								'totalPer'=>$totalPer,
								'totalObra'=>$totalPer+floatval($totalMat*1.12),
			);
		$model->inversion=floatval($arrayTotales['totalObra']);
		$model->fecha=$objComputos[0]->dia_trabajo;
		$model->fecha_fin=$objComputos[count($objComputos)-1]->dia_trabajo;
		$model->save();
		$model->fecha=strtoupper(Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' yyyy", $model->fecha));
		$model->fecha_fin=strtoupper(Yii::app()->dateFormatter->format("dd 'de' MMMM 'de' yyyy", $model->fecha_fin));
        $objUser=Usuario::model()->findByPk($model->usuario_id);
        $objUser->nombres=strtoupper($objUser->nombres);
        $objUser->apellidos=strtoupper($objUser->apellidos);
        $r = new YiiReport(array('template'=> 'INFORME-CIERRE-OBRA.xls'));
        $r->load(array(
                array(
                    'id' => 'obra',
                    'data' => $model->attributes,
                ),
                array(
                    'id'=>'solic',
                    'data'=>$objSolicitud->attributes,
                ),
                array(
                    'id'=>'inspe',
                    'data'=>$objInspeccion->attributes,
                ),
                array(
                    'id'=>'totales',
                    'data'=>$arrayTotales,
                ),
                array(
                    'id'=>'material',
                    'data'=>$arrayMaterial,
                    'repeat'=>true,
                    'minRows'=>1,
                ),
                array(
                    'id'=>'detMaq',
                    'data'=>$arrayMaquinaria,
                    'repeat'=>true,
                    'minRows'=>1,
                ),
                array(
                    'id'=>'detPer',
                    'data'=>$arrayPersonal,
                    'repeat'=>true,
                    'minRows'=>1,
                ),
                array(
                    'id'=>'user',
                    'data'=>$objUser->attributes,
                ),
            )
        );
        $phpExcel = $r->getPHPExcel();
	    $sheet = $phpExcel->getActiveSheet();
	    $sheet->getRowDimension(1)->setRowHeight(45);	
  		$basePath=Yii::app()->params['imagePath'];
  		$ruta=$basePath.'header.jpg';
	    $objDrawing = new PHPExcel_Worksheet_Drawing();
	    $objDrawing->setName('Logo');
	    $objDrawing->setDescription('Logo');
	    $objDrawing->setPath($ruta);
	    $objDrawing->setHeight(52);//Cambias por los pixeles de altura de tu logo
	    $objDrawing->setCoordinates('A1');
	    $objDrawing->setResizeProportional(true);
	    $phpExcel->setActiveSheetIndex(0);
	    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
        
        echo $r->render('excel5', 'INFORME-CIERRE-OBRA-'.$model->numero);
        //echo $r->render('excel2007', 'Students');
        //echo $r->render('pdf', 'Students');*/
        
    }//actionExcel method end

    public function actionUpload($id)
	{
		$model=$this->loadModel($id);	
		Yii::import("ext.multiupload.EAjaxUpload.qqFileUploader");

        $folder=Yii::getPathOfAlias('webroot').'/images/trabajo/';// folder for uploaded files
        $allowedExtensions = array("jpg","jpeg","png");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 100 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        
 
        $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName=$result['filename'];//GETTING FILE NAME
        //$img = CUploadedFile::getInstance($model,'image');

 		$objFoto=new FotoTrabajo;
 		$objFoto->trabajo_id=$model->id;
 		$objFoto->ruta='images/trabajo/'.$fileName;
 		if($objFoto->save()){
 			$result['db']='OK';	
 		}
 		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return;
	}

	public function actionEliminar(){
		$trabajo=0;

		foreach ($_POST as $key => $value) {
			$objFoto=FotoTrabajo::model()->find("ruta='$value'");
			if($objFoto!=null){
				$trabajo=$objFoto->trabajo_id;
				unlink(Yii::getPathOfAlias('webroot')."/".$objFoto->ruta);
				$objFoto->delete();
			}
		}
		$this->render('view',array(
			'model'=>$this->loadModel($trabajo),
			));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Trabajo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Trabajo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Trabajo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='trabajo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
