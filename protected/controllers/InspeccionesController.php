<?php

class InspeccionesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
			);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','upload','eliminar','eliminarc','excel','hoja','view','create','update','admin','delete','programacion','semana','cuadrilla','informe'),
				'expression'=>'Yii::app()->user->isAdministrador() || Yii::app()->user->isJefe() || Yii::app()->user->isCoordinador()',
				),
			array('deny',  // deny all users
				'users'=>array('*'),
				),

			);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Inspeccion;
		$metrico= new ComputoMetrico;
		$maq= new EstimacionMaquinaria;
		$per= new EstimacionPersonal;
		$dias= new DiaInspeccion;
		$validatedMetricos=array();
		$validatedMaquinas=array();
		$validatedPersonal=array();
		$validatedDias=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Inspeccion']))
		{
			$newModel=new Inspeccion;
			$newModel->attributes=$_POST['Inspeccion'];
			$newModel->numero=SolicitudInspeccion::model()->findByPk($newModel->solicitud_inspeccion_id)->numero;
			$model->attributes=$newModel->attributes;
			//var_dump($model);
			//exit();
			if((MultiModelForm::validate($metrico,$validatedMetricos,$deleteMetricos) && 
				MultiModelForm::validate($maq,$validatedMaquinas,$deleteMaquinas) &&
				MultiModelForm::validate($per,$validatedPersonal,$deletePersonal) &&
				MultiModelForm::validate($dias,$validatedDias,$deleteDias) ) && 
				$model->save() ){
				//Procesamos computos metricos
				
				$masterValues = array ('inspecciones_id'=>$model->id);
				if (MultiModelForm::save($metrico,$validatedMetricos,$deleteMetricos,$masterValues) && 
					MultiModelForm::save($maq,$validatedMaquinas,$deleteMaquinas,$masterValues) &&
					MultiModelForm::save($per,$validatedPersonal,$deletePersonal, $masterValues) &&
					MultiModelForm::save($dias,$validatedDias,$deleteDias, $masterValues) ){
					$this->redirect(array('view','id'=>$model->id));
				}
			}
			else{
				echo 'error';
			}
		}
		$this->render('create',array(
			'model'=>$model,
			'metrico'=>$metrico,
			'validatedMetricos'=>$validatedMetricos,
			'maq'=>$maq,
			'validatedMaquinas'=>$validatedMaquinas,
			'per'=>$per,
			'validatedPersonal'=>$validatedPersonal,
			'dias'=>$dias,
			'validatedDias'=>$validatedDias,
			));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$metrico= new ComputoMetrico;
		$maq= new EstimacionMaquinaria;
		$per= new EstimacionPersonal;
		$dias= new DiaInspeccion;
		$validatedMetricos=array();
		$validatedMaquinas=array();
		$validatedPersonal=array();
		$validatedDias=array();
		$images= new FotoInspeccion;
		$validatedImages=array();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Inspeccion']))
		{
			//$newModel=new Inspeccion;
			//$newModel->attributes=$_POST['Inspeccion'];
			//$newModel->numero=SolicitudInspeccion::model()->findByPk($newModel->solicitud_inspeccion_id)->numero;
			//$newModel->id=$model->id;
			//$model->attributes=$newModel->attributes;
			//the value for the foreign key 'groupid'
			$masterValues = array ('inspecciones_id'=>$model->id);
	        if( //Save the master model after saving valid members
	        	( MultiModelForm::save($metrico,$validatedMetricos,$deleteMetricos,$masterValues) && 
	        		MultiModelForm::save($maq,$validatedMaquinas,$deleteMaquinas,$masterValues) &&
	        		MultiModelForm::save($per,$validatedPersonal,$deletePersonal,$masterValues) && 
	        		MultiModelForm::save($dias,$validatedDias,$deleteDias, $masterValues) ) &&
	        	$model->save() ){
	        	$this->redirect(array('view','id'=>$model->id));
	        }
    }

    $this->render('update',array(
    	'model'=>$model,
		'metrico'=>$metrico,
		'validatedMetricos'=>$validatedMetricos,
		'maq'=>$maq,
		'validatedMaquinas'=>$validatedMaquinas,
		'per'=>$per,
		'validatedPersonal'=>$validatedPersonal,
		'dias'=>$dias,
		'validatedDias'=>$validatedDias,
		'images'=>$images,
		'validatedImages'=>$validatedImages,
    	));
}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Inspeccion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Inspeccion']))
			$model->attributes=$_GET['Inspeccion'];

		$this->render('admin',array(
			'model'=>$model,
			));
	}

	public function actionHoja()
	{
	    $model=new Inspeccion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_POST['Inspeccion'])){
			$model->attributes=$_POST['Inspeccion'];
			// INICIO SCRIPT EXCEL
			// semana es $model->semana
			$semana=array('data'=>$model->semana);
			// La primera posicion es el lunes de esa semana
			$cuadrillas=['Cuadrilla 1','Cuadrilla 2','Cuadrilla TP4'];
	        $arrayFechas[0]=date('Y-m-d',strtotime(date("Y").'W'.$model->semana)); 			
	        for ($i=0; $i<5; $i++){
	        	foreach ($cuadrillas as $cuadrilla) {
	        		$tempInspeccion=Inspeccion::model()->findBySql(
	        		"SELECT i.id,i.solicitud_inspeccion_id,i.descripcion FROM dia_inspeccion d 
	        		JOIN inspecciones i ON i.id=d.inspecciones_id 
	        		JOIN personal p ON i.cuadrilla=p.id 
	        		WHERE d.fecha='".$arrayFechas[$i]."' AND p.descripcion='".$cuadrilla."'");
	        		$objPlan[]=array('desc'=>is_null($tempInspeccion)?'NO SE ENCONTRARON DATOS':strtoupper($tempInspeccion->descripcion),
	        						'sitio'=>is_null($tempInspeccion)?'NO SE ENCONTRARON DATOS':strtoupper(SolicitudInspeccion::model()->findByPk($tempInspeccion->solicitud_inspeccion_id)->direccion),
	        		);
	        	}
	        	
	        	$arrayFechas[$i+1]=date( 'Y-m-d' , strtotime('+1 day',strtotime($arrayFechas[$i])));
	        }
	        $staticFechas=array('lunes'=>Yii::app()->dateFormatter->format("dd/MM/yyyy",$arrayFechas[0]),
	        					'martes'=>Yii::app()->dateFormatter->format("dd/MM/yyyy",$arrayFechas[1]),
	        					'miercoles'=>Yii::app()->dateFormatter->format("dd/MM/yyyy",$arrayFechas[2]),
	        					'jueves'=>Yii::app()->dateFormatter->format("dd/MM/yyyy",$arrayFechas[3]),
	        					'viernes'=>Yii::app()->dateFormatter->format("dd/MM/yyyy",$arrayFechas[4]),
	        );
	        $arrayProg=array('luness1'=>$objPlan[0]['sitio'],
				        	'lunesd1'=>$objPlan[0]['desc'],
				        	'luness2'=>$objPlan[1]['sitio'],
				        	'lunesd2'=>$objPlan[1]['desc'],
				        	'lunesstp4'=>$objPlan[2]['sitio'],
				        	'lunesdtp4'=>$objPlan[2]['desc'],
				        	'martess1'=>$objPlan[3]['sitio'],
				        	'martesd1'=>$objPlan[3]['desc'],
				        	'martess2'=>$objPlan[4]['sitio'],
				        	'martesd2'=>$objPlan[4]['desc'],
				        	'martesstp4'=>$objPlan[5]['sitio'],
				        	'martesdtp4'=>$objPlan[5]['desc'],
				        	'miercoless1'=>$objPlan[6]['sitio'],
				        	'miercolesd1'=>$objPlan[6]['desc'],
				        	'miercoless2'=>$objPlan[7]['sitio'],
				        	'miercolesd2'=>$objPlan[7]['desc'],
				        	'miercolesstp4'=>$objPlan[8]['sitio'],
				        	'miercolesdtp4'=>$objPlan[8]['desc'],
				        	'juevess1'=>$objPlan[9]['sitio'],
				        	'juevesd1'=>$objPlan[9]['desc'],
				        	'juevess2'=>$objPlan[10]['sitio'],
				        	'juevesd2'=>$objPlan[10]['desc'],
				        	'juevesstp4'=>$objPlan[11]['sitio'],
				        	'juevesdtp4'=>$objPlan[11]['desc'],
				        	'vierness1'=>$objPlan[12]['sitio'],
				        	'viernesd1'=>$objPlan[12]['desc'],
				        	'vierness2'=>$objPlan[13]['sitio'],
				        	'viernesd2'=>$objPlan[13]['desc'],
				        	'viernesstp4'=>$objPlan[14]['sitio'],
				        	'viernesdtp4'=>$objPlan[14]['desc'],
	        	);
	        $r = new YiiReport(array('template'=> 'PROGRAMACION-SEMANA.xls'));
	        $r->load(array(
	                array(
	                    'id' => 'semana',
	                    'data' => $semana,
	                ),	             
	                array(
	                    'id'=>'staticFechas',
	                    'data'=>$staticFechas,
	                ),
	                array(
	                	'id'=>'plan',
	                	'data'=>$arrayProg,
	                ),	                
	            )
	        );
	        $phpExcel = $r->getPHPExcel();
		    $sheet = $phpExcel->getActiveSheet();
		    $sheet->getRowDimension(1)->setRowHeight(45);	
	  		$basePath=Yii::app()->params['imagePath'];
	  		$ruta=$basePath.'header.jpg';
		    $objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('Logo');
		    $objDrawing->setDescription('Logo');
		    $objDrawing->setPath($ruta);
		    $objDrawing->setHeight(52);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('A1');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());

		    echo $r->render('excel5', 'PROGRAMACION-SEMANA-'.$model->semana);

			// FIN SCRIPT EXCEL
		}

		$this->render('hoja',array(
			'model'=>$model,
			));
	}

	public function actionExcel($id){
        
        $model=$this->loadModel($id);
        if (isset($model->fecha) ){	
				preg_match('#(\d{4})-(\d{2})-(\d{2})#', $model->fecha, $Matches);
				$model->fecha=$Matches[3]."/".$Matches[2]."/".$Matches[1];
		}
		$solicit=SolicitudInspeccion::model()->findByPk($model->solicitud_inspeccion_id);
		$computos=ComputoMetrico::model()->findAll(array('condition'=>"inspecciones_id = $id"));
		$arrayComputos=array();
		$tm2=0; $tm3=0; $tton=0; $tlts=0; 
		foreach ($computos as $c) {
			$c->m2=$c->ancho*$c->largo*$c->cantidad;
			$c->m3=$c->ancho*$c->largo*$c->cantidad*$c->espesor;
			$c->ton=$c->m3*2.33;
			$c->lts=$c->m2*1.5;
			$tm2+=$c->m2; 
		    $tm3+=$c->m3; 
		    $tton+=$c->ton; 
		    $tlts+=$c->lts;
			$arrayComputos[]=$c->attributes;
		}
		$totales=array('totalm2'=>$tm2,
					'totalm3'=>$tm3,
					'totalton'=>$tton,
					'totallts'=>$tlts,
					);

		$estMaquinaria=Maquinaria::model()->findAllBySql("SELECT descripcion FROM estimacion_maquinaria e JOIN maquinaria m ON e.maquinaria_id=m.id WHERE e.inspecciones_id=$id;");
		$arrayMaquinaria=array();
		foreach ($estMaquinaria as $m) {
			$arrayMaquinaria[]=$m->descripcion;
		}

		$estPersonal=EstimacionPersonal::model()->findAllBySql("SELECT * FROM estimacion_personal  WHERE inspecciones_id=$id;");
		$arrayPersonal=array(); 
		foreach ($estPersonal as $p) {
			$arrayPersonal[]=$p->cantidad.' '.$p->personal->descripcion;
		}
        $r = new YiiReport(array('template'=> 'INFORME-INSPECCION.xls'));
        $r->load(array(
                array(
                    'id' => 'inspe',
                    'data' => $model->attributes,
                ),
                array(
                    'id'=>'solic',
                    'data'=>$solicit->attributes,
                ),
                array(
                    'id'=>'comp',
                    'data'=>$arrayComputos,
                    'repeat'=>true,
                    'minRows'=>1,
                ),
                array(
                    'id'=>'total',
                    'data'=>$totales,
                ),
                array(
                    'id'=>'estMaq',
                    'data'=>array('data'=>implode(",", $arrayMaquinaria)),
                ),
                array(
                    'id'=>'estPer',
                    'data'=>array('data'=>implode(",", $arrayPersonal)),
                ),
            )
        );
        $phpExcel = $r->getPHPExcel();
	    $sheet = $phpExcel->getActiveSheet();
	    $sheet->getRowDimension(1)->setRowHeight(45);	
  		$basePath=Yii::app()->params['imagePath'];
  		$ruta=$basePath.'header.jpg';
	    $objDrawing = new PHPExcel_Worksheet_Drawing();
	    $objDrawing->setName('Logo');
	    $objDrawing->setDescription('Logo');
	    $objDrawing->setPath($ruta);
	    $objDrawing->setHeight(52);//Cambias por los pixeles de altura de tu logo
	    $objDrawing->setCoordinates('A1');
	    $objDrawing->setResizeProportional(true);
	    $phpExcel->setActiveSheetIndex(0);
	    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
        
	    //Croquis
	    if(isset($model->croquis) && $model->croquis!=""){
	    	$filePath=substr($model->croquis, 7);
	    	$objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('Croquis');
		    $objDrawing->setDescription('Croquis');
		    $objDrawing->setPath(Yii::app()->params['imagePath'].$filePath);
		    $objDrawing->setHeight(400);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('J2');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
	    }
	    //Fotos de inspecciones
	    $objFotos=FotoInspeccion::model()->findAll("inspecciones_id=$id");
	    foreach ($objFotos as $foto) {
	    	$filePath=substr($foto->ruta, 7);
	    	$objDrawing = new PHPExcel_Worksheet_Drawing();
		    $objDrawing->setName('FotoIns');
		    $objDrawing->setDescription('FotoIns');
		    $objDrawing->setPath(Yii::app()->params['imagePath'].$filePath);
		    $objDrawing->setHeight(400);//Cambias por los pixeles de altura de tu logo
		    $objDrawing->setCoordinates('J2');
		    $objDrawing->setResizeProportional(true);
		    $phpExcel->setActiveSheetIndex(0);
		    $objDrawing->setWorksheet($phpExcel->getActiveSheet());
	    }

        echo $r->render('excel5', 'INFORME-INSPECCION-'.$model->numero);
        //echo $r->render('excel2007', 'Students');
        //echo $r->render('pdf', 'Students');
        
    }//actionExcel method end

	public function actionInforme($id){
		$model=$this->loadModel($id);
		$solicit=SolicitudInspeccion::model()->findByPk($model->solicitud_inspeccion_id);
		$estComputos=ComputoMetrico::model()->findAllBySql("SELECT *, 
			TRUNCATE(ancho*largo*cantidad,2) as m2,
			TRUNCATE(ancho*largo*cantidad*espesor,2) as m3,
			TRUNCATE(ancho*largo*cantidad*espesor*2.33,2) as ton,
			TRUNCATE(ancho*largo*cantidad*1.5,2) as lts FROM computo_metrico WHERE inspecciones_id=$id;");
		$estMaquinaria=Maquinaria::model()->findAllBySql("SELECT descripcion FROM estimacion_maquinaria e JOIN maquinaria m ON e.maquinaria_id=m.id WHERE e.inspecciones_id=$id;");
		$estPersonal=Personal::model()->findAllBySql("SELECT descripcion FROM estimacion_personal e JOIN personal p ON e.personal_id=p.id WHERE e.inspecciones_id=$id;");
		$cantPersonal=EstimacionPersonal::model()->findAllBySql("SELECT cantidad FROM estimacion_personal e JOIN personal p ON e.personal_id=p.id WHERE e.inspecciones_id=$id;");
		$estMaterial=Material::model();
		$usuario=Usuario::model()->findByPk($model->usuario_id);
		$content=$this->renderPartial('_informe', array('model' => $model, 
						'solicit'=>$solicit,
						'estComputos'=>$estComputos,
						'estPersonal'=>$estPersonal,
						'estMaquinaria'=>$estMaquinaria,
						'estMaterial'=>$estMaterial,
						'cantPersonal'=>$cantPersonal,
						'usuario'=>$usuario,  
						), true);
		Yii::app()->request->sendFile('INFORME_INSPECCION_'.$model->numero.'.xls',$content);
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			)); 
	}

	public function actionProgramacion(){
		$model=new Inspeccion;

		if(isset($_GET['Inspeccion']))
			$model->attributes=$_GET['Inspeccion'];
		$model->realizada=1;
		$this->render('programacion',array('model'=>$model));	
	}

	public function actionSemana(){
		if(Yii::app()->request->isPostRequest) {
			if(Inspeccion::model()->updateByPk($_POST['pk'], array('semana' => $_POST['value']))) {
				echo CJSON::encode(array('id' => $_POST['pk']));
			} else {
				$errors = ['Transaccion fallida'];
				echo CJSON::encode(array('errors' => $errors));
			}
		}else {
			throw new CHttpException(400, 'Invalid request');
		}
	}

	public function actionCuadrilla(){
		if(Yii::app()->request->isPostRequest) {
			$model=$this->loadModel($_POST['pk']);
			$errors= array();
			if($model->semana==0){
				$errors[]="No se puede asignar cuadrilla antes de semana";
			}
			/* validacion para que no se repita la prioridad
			else{
				$otros=Inspeccion::model()->count("semana=:semana AND prioridad=:prioridad",
					array(
						':semana'=>$model->semana,
						':prioridad' =>$_POST['value'])
					);


				if($otros>0)
				{
					$errors[]="No se puede repetir prioridad en una semana";
				}

			}*/
			if(count($errors)>0){
				echo CJSON::encode(array('errors' => $errors,'exito'=>false));
			}else{
				if(Inspeccion::model()->updateByPk($_POST['pk'], array('cuadrilla' => $_POST['value']))) {
					echo CJSON::encode(array('id' => $_POST['pk'],'exito'=>true));
				} else {
					$errors = ['transaccion fallida'];
					echo CJSON::encode(array('errors' => $errors,'exito'=>false));
				}
			}

		}else {
			throw new CHttpException(400, 'Invalid request');
		}
	}

	public function actionUpload($id)
	{
		$model=$this->loadModel($id);	
		Yii::import("ext.multiupload.EAjaxUpload.qqFileUploader");

        $folder=Yii::getPathOfAlias('webroot').'/images/inspeccion/';// folder for uploaded files
        $allowedExtensions = array("jpg","jpeg","png");//array("jpg","jpeg","gif","exe","mov" and etc...
        $sizeLimit = 100 * 1024 * 1024;// maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload($folder);
        
 
        $fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
        $fileName=$result['filename'];//GETTING FILE NAME
        //$img = CUploadedFile::getInstance($model,'image');

 		$objFoto=new FotoInspeccion;
 		$objFoto->inspecciones_id=$model->id;
 		$objFoto->ruta='images/inspeccion/'.$fileName;
 		if($objFoto->save()){
 			$result['db']='OK';	
 		}
 		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $return;// it's array
	}

	public function actionEliminar(){
		$inspeccion=0;

		foreach ($_POST as $key => $value) {
			$objFoto=FotoInspeccion::model()->find("ruta='$value'");
			if($objFoto!=null){
				$inspeccion=$objFoto->inspecciones_id;
				unlink(Yii::getPathOfAlias('webroot')."/".$objFoto->ruta);
				$objFoto->delete();
			}
		}
		$this->render('view',array(
			'model'=>$this->loadModel($inspeccion),
			));
	}

	public function actionEliminarc(){
		$objInspeccion=Inspeccion::model()->findByPk($_GET['id']);
		unlink(Yii::getPathOfAlias('webroot')."/".$objInspeccion->croquis);
		$objInspeccion->croquis=null;
		$objInspeccion->save();
		$this->render('view',array(
			'model'=>$this->loadModel($objInspeccion->id),
			));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Inspeccion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Inspeccion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Inspeccion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='inspeccion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
