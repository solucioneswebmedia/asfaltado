<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$inventarios=MaterialRestante::model()->findAll();
		$this->render('index', array('inventarios'=>$inventarios,
										'asfaltoAnual'=>PapeletaAsfalto::model()->getCantidadAnual(),
										'obrasAnual'=>Trabajo::model()->getCantidadAnual(),
										'inspeccionAnual'=>Inspeccion::model()->getCantidadAnual(),
										));
	}

	public function actionEjemplo()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('ejemplo');
	}

	public function actionDatos(){
		if( !(Yii::app()->user->isGuest || Yii::app()->user->isOficina()) ){
			
			if(isset($_POST['fechaInicio'])){
				$fechaMin=null; $fechaMax=null;
				if(preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_POST['fechaInicio'], $Matches)){
					$fechaMin=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
				if(preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_POST['fechaFin'], $Matches)){
					$fechaMax=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
				$asfaltoRango=PapeletaAsfalto::model()->getCantidadRango($fechaMin,$fechaMax);
				$inspeccionRango=Inspeccion::model()->getCantidadRango($fechaMin,$fechaMax);
				$obraRango=Trabajo::model()->getCantidadRango($fechaMin,$fechaMax);
				$inversionAdmRango=Trabajo::model()->getInversionAdmRango($fechaMin,$fechaMax);
				$inversionContRango=Trabajo::model()->getInversionContRango($fechaMin,$fechaMax);
				$this->render('datos',array(
					'fechaInicio'=>$_POST['fechaInicio'],
					'fechaFin'=>$_POST['fechaFin'],
					'result'=>true,
					'asfaltoRango'=>$asfaltoRango,
					'inspeccionRango'=>$inspeccionRango,
					'obraRango'=>$obraRango,
					'inversionAdmRango'=>$inversionAdmRango,
					'inversionContRango'=>$inversionContRango,
					 ));

			}else{
				$this->render('datos',array('result'=>false));
			}

		}else{
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}