<?php

class SolicitudesInspeccionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow authenticated users perform these actions
				'actions'=>array('index','view','prioridad','create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('programacion','semana','prioridad'),
				'expression'=>'Yii::app()->user->isAdministrador() || Yii::app()->user->isJefe() || Yii::app()->user->isCoordinador()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SolicitudInspeccion;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SolicitudInspeccion']))
		{
			$model->attributes=$_POST['SolicitudInspeccion'];
			$model->fecha_registro=date("Y-m-d");
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SolicitudInspeccion']))
		{
			$model->attributes=$_POST['SolicitudInspeccion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new SolicitudInspeccion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SolicitudInspeccion']))
			$model->attributes=$_GET['SolicitudInspeccion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionProgramacion(){
		$model=new SolicitudInspeccion;

		if(isset($_GET['SolicitudInspeccion']))
			$model->attributes=$_GET['SolicitudInspeccion'];
		$model->realizada=0;
		$this->render('programacion',array('model'=>$model));	
	}
	
	public function actionSemana(){
		 if(Yii::app()->request->isPostRequest) {
		 	 if(SolicitudInspeccion::model()->updateByPk($_POST['pk'], array('semana' => $_POST['value']))) {
				echo CJSON::encode(array('id' => $_POST['pk']));
			} else {
				$errors = ['Transaccion fallida'];
				echo CJSON::encode(array('errors' => $errors));
			}
			}else {
			throw new CHttpException(400, 'Invalid request');
		}
	}

	public function actionPrioridad(){
		 if(Yii::app()->request->isPostRequest) {
		 	$model=$this->loadModel($_POST['pk']);
		 	$errors= array();
		 	if($model->semana==0){
		 		$errors[]="No se puede asignar prioridad antes de semana";
		 	}
		 	/*
		 	else{
			 	$otros=SolicitudInspeccion::model()->count("semana=:semana AND prioridad=:prioridad",
			 							array(
			 								':semana'=>$model->semana,
			 								':prioridad' =>$_POST['value'])
			 							);
		 		if($otros>0)
		 		{
		 			$errors[]="No se puede repetir prioridad en una semana";
		 		}
			 	
			 }
			 */
			 if(count($errors)>0){
			 	echo CJSON::encode(array('errors' => $errors,'exito'=>false));
			 }else{
			 	if(SolicitudInspeccion::model()->updateByPk($_POST['pk'], array('prioridad' => $_POST['value']))) {
					echo CJSON::encode(array('id' => $_POST['pk'],'exito'=>true));
				} else {
					$errors = ['Transaccion fallida'];
					echo CJSON::encode(array('errors' => $errors,'exito'=>false));
				}
			 }
			 	
			}else {
			throw new CHttpException(400, 'Invalid request');
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SolicitudInspeccion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SolicitudInspeccion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SolicitudInspeccion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='solicitud-inspeccion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
