<?php

class ServiciosController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionAuth(){
		if(isset($_POST['usuario']) && isset($_POST['clave']) ){
			$usuario=$_POST['usuario'];
			$clave=$_POST['clave'];
			$objUsuario= Usuario::model()->findBySql(
				"SELECT * FROM usuario WHERE usuario='".$usuario."' AND pass='".$clave."'");
			if( isset($objUsuario) ){
				$bit=new Bitacora;
				$bit->tabla="usuario";
				$bit->registro="";
				$bit->usuario=$_POST['usuario'];
				$bit->tipo="Inicio Sesion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				$objUsuario->token=md5(time()+$objUsuario->usuario);
				$datetime = new DateTime('tomorrow');
				$objUsuario->date=$datetime->format('Y-m-d');
				if($objUsuario->save()){
					$return=['success'=>'1','id'=>$objUsuario->id];
					$return['credencial']=['token'=>$objUsuario->token,'date'=>$objUsuario->date];				
				}else{
					$return=['success'=>'-1'];				
				}
			}else{
				$return=['success'=>'0'];
				$return['credencial']=['token'=>-1,'date'=>-1];
			}
			$return['action']='login';
			$this->renderJSON($return);	
		}
	}

	public function actionTest(){
		if(isset($_GET['token'])){
			$objAuth=Usuario::model()->find("token='".$_GET['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="detalles";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				if(strcmp($_GET['tabla'],"detalle_computos")==0){
					$new=new DetalleTrabajo;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_GET['numero']."'")->id;
					$new->cantidad=$_GET['cantidad'];
					$new->ancho=$_GET['ancho'];
					$new->largo=$_GET['largo'];
					$new->espesor=$_GET['espesor'];
					$new->dia_trabajo=$_GET['dia_trabajo'];
				} else if(strcmp($_GET['tabla'],"detalle_maquinaria")==0){
					$new = new DetalleMaquinaria;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_GET['numero']."'")->id;
					$new->maquinaria_existente_id=$_GET['id_maq'];
					$new->dia_trabajo=$_GET['dia_trabajo'];
					//$new->unidades_id=$_GET['unidades_id'];
					//$new->extra=$_GET['extra'];
				}else if(strcmp($_GET['tabla'],"detalle_personal")==0){
					$new = new DetallePersonal;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_GET['numero']."'")->id;
					$new->personal_id=$_GET['id_per'];
					$new->cantidad=$_GET['cantidad'];
					$new->dia_trabajo=$_GET['dia_trabajo'];
				}else {
					$new = new PapeletaAsfalto;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_GET['numero']."'")->id;
					$new->fecha=$_GET['dia_trabajo'];
					$new->codigo=$_GET['codigo'];
					$new->proveedor_id=$_GET['proveedor_id'];
					$new->procedencia=$_GET['procedencia'];
					$new->cantidad=$_GET['cantidad'];
				}
				
				if(1){
					$return=['success'=>'1'];
					$return['clientData']=['idRowClient'=>$_GET['idRowClient'],'tablaClient'=>$_GET['tabla']];
				}else{
					$return=['status'=>'0'];
				}
				
			}else{
				$return=['success'=>'-1'];
			}
		}
		$this->renderJSON($return);	
	}

	public function actionInspeccion(){
		if(isset($_POST['token'])){
			$objAuth=Usuario::model()->find("token='".$_POST['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="inspecciones";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				$new=new Inspeccion;
				$new->numero=$_POST['numero'];
				$objSolicitud=SolicitudInspeccion::model()->find("numero='".$new->numero."'");
				if(isset($objSolicitud)){
					$new->solicitud_inspeccion_id=$objSolicitud->id;
					$objSolicitud->direccion=$_POST['direccion'];
					$objSolicitud->realizada=1;
					$objSolicitud->save();
				}else{
					$objSolicitud=new SolicitudInspeccion;
					$objSolicitud->numero=$_POST['numero'];
					$objSolicitud->direccion=$_POST['direccion'];
					if($objSolicitud->save() ){
						$new->solicitud_inspeccion_id=$objSolicitud->id;	
					}
				}
				$new->usuario_id=$objAuth->id;
				$new->asunto=$_POST['asunto'];
				$new->parroquia=$_POST['parroquia'];
				$new->fecha=$_POST['fecha'];
				$new->tipo_obra=$_POST['tipo_obra'];
				$new->descripcion=$_POST['descripcion'];
				$new->problematica=$_POST['problematica'];
				$new->recomendaciones=$_POST['recomendaciones'];
				$new->pto_ref=$_POST['pto_ref'];
				$new->material_bacheo=$_POST['material_bacheo'];
				$new->tipo_adm=$_POST['tipo_adm'];
				$new->latitud=$_POST['latitud'];
				$new->longitud=$_POST['longitud'];
				$new->realizada=1;
				if($new->save()){
					$return=['success'=>'1','id'=>$new->id];
				}else{
					$return=['status'=>'0'];
				}
				
			}else{
				$return=['success'=>'-1'];
			}
		}
		$this->renderJSON($return);	
	}

	public function actionEstimaciones(){
		if(isset($_POST['token'])){
			$objAuth=Usuario::model()->find("token='".$_POST['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="estimaciones";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				if(strcmp($_POST['tabla'],"computos_metricos")==0){
					$new=new ComputoMetrico;
					$new->inspecciones_id=Inspeccion::model()->find("numero='".$_POST['numero']."'")->id;
					$new->cantidad=$_POST['cantidad'];
					$new->ancho=$_POST['ancho'];
					$new->largo=$_POST['largo'];
					$new->espesor=$_POST['espesor'];
				} else if(strcmp($_POST['tabla'],"estimacion_maquinaria")==0){
					$new = new EstimacionMaquinaria;
					$new->inspecciones_id=Inspeccion::model()->find("numero='".$_POST['numero']."'")->id;
					$new->maquinaria_id=$_POST['id_maq'];
					$new->cantidad=$_POST['cantidad'];
				}else{
					$new = new EstimacionPersonal;
					$new->inspecciones_id=Inspeccion::model()->find("numero='".$_POST['numero']."'")->id;
					$new->personal_id=$_POST['id_per'];
					$new->cantidad=$_POST['cantidad'];
					$new->num_dias=$_POST['num_dias'];
				}
				
				if($new->save()){
					$return=['success'=>'1'];
					$return['clientData']=['idRowClient'=>$_POST['idRowClient'],'tablaClient'=>$_POST['tabla']];
				}else{
					$return=['status'=>'0'];
				}
				
			}else{
				$return=['success'=>'-1'];
			}
		}
		$this->renderJSON($return);		
	}

	public function actionObra(){
		if(isset($_POST['token'])){
			$objAuth=Usuario::model()->find("token='".$_POST['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="trabajo";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				$new = new Trabajo;
				$new->numero=$_POST['numero'];
				$new->inspecciones_id=Inspeccion::model()->find("numero='".$_POST['numeroIns']."'")->id;
				$new->usuario_id=$objAuth->id;

				$new->fecha=$_POST['fecha'];
				$new->asunto=$_POST['asunto'];
				$new->denominacion=$_POST['denominacion'];
				$new->direccion=$_POST['direccion'];
				$new->problematica=$_POST['problematica'];
				$new->tipo_reparacion=$_POST['tipo_reparacion'];
				$new->descripcion=$_POST['descripcion'];
				$new->beneficiario=$_POST['beneficiario'];
				$new->observaciones=$_POST['observaciones'];
				$new->realizada=1;
				
				if($new->save()){
					$return=['success'=>'1','id'=>$new->id];
				}else{
					$return=['status'=>'0'];
				}
				
			}else{
				$return=['success'=>'-1'];
			}
		}
		$this->renderJSON($return);	
	}

	public function actionDetalles(){
		if(isset($_POST['token'])){
			$objAuth=Usuario::model()->find("token='".$_POST['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="detalles";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				if(strcmp($_POST['tabla'],"detalle_computos")==0){
					$new=new DetalleTrabajo;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_POST['numero']."'")->id;
					$new->cantidad=$_POST['cantidad'];
					$new->ancho=$_POST['ancho'];
					$new->largo=$_POST['largo'];
					$new->espesor=$_POST['espesor'];
					$new->dia_trabajo=$_POST['dia_trabajo'];
				} else if(strcmp($_POST['tabla'],"detalle_maquinaria")==0){
					$new = new DetalleMaquinaria;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_POST['numero']."'")->id;
					$new->maquinaria_existente_id=$_POST['id_maq'];
					$new->dia_trabajo=$_POST['dia_trabajo'];
					//$new->unidades_id=$_POST['unidades_id'];
					//$new->extra=$_POST['extra'];
				}else if(strcmp($_POST['tabla'],"detalle_personal")==0){
					$new = new DetallePersonal;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_POST['numero']."'")->id;
					$new->personal_id=$_POST['id_per'];
					$new->cantidad=$_POST['cantidad'];
					$new->dia_trabajo=$_POST['dia_trabajo'];
				}else {
					$new = new PapeletaAsfalto;
					$new->trabajo_id=Trabajo::model()->find("numero='".$_POST['numero']."'")->id;
					$new->fecha=$_POST['dia_trabajo'];
					$new->codigo=$_POST['codigo'];
					$new->proveedor_id=$_POST['proveedor_id'];
					$new->procedencia=$_POST['procedencia'];
					$new->cantidad=$_POST['cantidad'];
				}
				
				if($new->save()){
					$return=['success'=>'1'];
					$return['clientData']=['idRowClient'=>$_POST['idRowClient'],'tablaClient'=>$_POST['tabla']];
				}else{
					$return=['status'=>'0'];
				}
				
			}else{
				$return=['success'=>'-1'];
			}
		}
		$this->renderJSON($return);		
	}

	public function actionImages(){ 
		if(isset($_POST['token'])){
			$objAuth=Usuario::model()->find("token='".$_POST['token']."'");
			if(isset($objAuth)){ //falta validar por fecha
				$bit=new Bitacora;
				$bit->tabla="foto";
				$bit->registro="";
				$bit->usuario=$objAuth->usuario;
				$bit->tipo="Recepcion";
				$bit->timestamp=date('Y-m-d H:i:s');
				$bit->save();
				$base=$_POST['data'];
				$binary=base64_decode($base);
				header('Content-Type: bitmap; charset=utf-8');
				if(strcmp($_POST['tipo'],"1")==0){
					$path="images/inspeccion/".$_POST['archivo'].".jpg";
				}else{
					$path="images/trabajo/".$_POST['archivo'].".jpg";
				}

				
				$file = fopen($path, 'wb');

				if(fwrite($file, $binary)!=false){
					fclose($file);
					
					if(strcmp($_POST['tabla'], "inspecciones")==0){
						$objInspeccion=Inspeccion::model()->find("numero='".$_POST['numero']."'");
						$objInspeccion->croquis=$path;	
						if($objInspeccion->save()){
							$return=['status'=>'1'];	
						}
					}else if(strcmp($_POST['tabla'], "foto_inspeccion")==0){
						$objInspeccion=Inspeccion::model()->find("numero='".$_POST['numero']."'");
						$objFoto=new FotoInspeccion;
						$objFoto->inspecciones_id=$objInspeccion->id;
						$objFoto->ruta=$path;
						$objFoto->save();
					}else{
						$objTrabajo=Trabajo::model()->find("numero='".$_POST['numero']."'");
						$objFoto=new FotoTrabajo;
						$objFoto->trabajo_id=$objTrabajo->id;
						$objFoto->ruta=$path;
						$objFoto->save();
					}
					
					
					$return=['success'=>'1'];
					$return['clientData']=['numeroInsClient'=>$_POST['numero'],
												'tablaClient'=>$_POST['tabla'],
												'idClient'=>(strcmp($_POST['tabla'],"inspecciones")!=0)?$_POST['rowId']:-1
												];
				}else{
					$return=['success'=>'0'];	
				}
			}else{
				$return=['success'=>'-1'];	
			}
		}
		$this->renderJSON($return);	
	}

	public function actionRecursos($type=null, $id=null){
		$types=['m'=>'Maquinaria','p'=>'Personal','me'=>'MaquinariaExistente','pr'=>'Proveedor'];
		$return=['action'=>'recursos','result'=>[] ];

		if(!is_null($type) && !isset($types[$type])){
			$return['error']='Error: Undefined type.';
		}else{
			$type=(!is_null($type))?[$type=>$types[$type]]:$types;	
			foreach ($type as $key => $clase) {
				if(!isset($return['result'][$clase])){
					$return['result'][$clase]=[];
				}
				if(is_null($id)){
					$registros=$clase::model()->findAll();
					foreach ($registros as $row) {
						if($key=='pr'){
							$return['result'][$clase][]=$row->attributes;
						}else{
							$arrTemp=$row->attributes;
							$arrTemp['unidad']=$row->unidades->nombre_unidad;
							$return['result'][$clase][]=$arrTemp;
						}
					}
				}else{
					$registros=$clase::model()->findByPk($id);
					if(!is_null($registros)){
						if($key=='pr'){
							$return['result'][$clase][]=$registros->attributes;
						}else{
							$arrTemp=$registros->attributes;
							$arrTemp['unidad']=$registros->unidades->nombre_unidad;
							$return['result'][$clase][]=$arrTemp;
						}
					}
				}	
			}

		}
		$this->renderJSON($return);
	}
	
	public function actionSolicitudes($date=null){

		$return=['action'=>'solicitudes','result'=>[] ];
		foreach ($_GET as $key => $value) {
			$fecha=$key;
		}
		$sql="SELECT * FROM solicitud_inspeccion WHERE fecha_registro>='".$fecha."' AND realizada=0 AND semana<>0";
		$registros=SolicitudInspeccion::model()->findAllBySql($sql);
		foreach ($registros as $row) {
				$return['result'][]=$row->attributes;
		}

		$this->renderJSON($return);
	}

	public function actionProgramacion($date=null){
		$return=['action'=>'programacion','result'=>[] ];
		foreach ($_GET as $key => $value) {
			$fecha=$key;
		}
		$sql="SELECT * FROM inspecciones WHERE fecha>='".$fecha."' AND realizada=1 AND semana<>0";
		$objInspeccion=Inspeccion::model()->findAllBySql($sql);
		foreach ($objInspeccion as $obj) {
			$objSolicitud[]=SolicitudInspeccion::model()->find("id=".$obj->solicitud_inspeccion_id);
		}
		
		for($i=0; $i<count($objInspeccion); $i++) {
				$return['result'][]=array('numero'=>$objInspeccion[$i]->attributes['numero'],
					'fecha'=>$objInspeccion[$i]->attributes['fecha'],
					'direccion'=>$objSolicitud[$i]->attributes['direccion'],
					'parroquia'=>$objInspeccion[$i]->attributes['parroquia'],
					'tipo_obra'=>$objInspeccion[$i]->attributes['tipo_obra'],
					'descripcion'=>$objInspeccion[$i]->attributes['descripcion'],
					'recomendaciones'=>$objInspeccion[$i]->attributes['recomendaciones'],
					'pto_ref'=>$objInspeccion[$i]->attributes['pto_ref'],
					'tipo_adm'=>$objInspeccion[$i]->attributes['tipo_adm'],
					'semana'=>$objInspeccion[$i]->attributes['semana'],
					'cuadrilla'=>$objInspeccion[$i]->attributes['cuadrilla']);
		}
		$this->renderJSON($return);
	}

	public function actionInsert(){
		$id=$_POST['id'];
		if($id>0){
			$objMaq=Maquinaria::model()->findByPk($id);
		}else{
			$objMaq=new Maquinaria;
		}
		$objMaq->attributes=$_POST;
		if($objMaq->save()){
			$return=['status'=>'OK','id'=>$objMaq->id];
		}else{
			$return=['status'=>'FAIL'];
		}
		$return['action']='insert';
		$this->renderJSON($return);
	}
	
}