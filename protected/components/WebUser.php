<?php
class WebUser extends CWebUser {
 
  // Almacena el modelo internamente para no repetir queries mientras dure una sesion.
  private $_model;
 
  // retorna el nombre completo.
  // se accede a ella a traves de  Yii::app()->user->nombre
  function getNombreCompleto(){
    $usuario = $this->loadUser(Yii::app()->user->id);
    return $usuario->nombres;
  }
 
  // esta funcion chequea el ID del rol del usuario 
  function isAdministrador(){ 
  	if (isset(Yii::app()->user->id))
  	{  	
    	$usuario = $this->loadUser(Yii::app()->user->id);
    	return intval($usuario->rol->id) == 1;
  	}
  	return false;
  }

  function isJefe(){ 
    if (isset(Yii::app()->user->id))
    {   
      $usuario = $this->loadUser(Yii::app()->user->id);
      return intval($usuario->rol->id) == 2;
    }
    return false;
  }

  function isCoordinador(){ 
    if (isset(Yii::app()->user->id))
    {   
      $usuario = $this->loadUser(Yii::app()->user->id);
      return (intval($usuario->rol->id) == 3 || intval($usuario->rol->id) == 5);
    }
    return false;
  }

  function isOficina(){ 
    if (isset(Yii::app()->user->id))
    {   
      $usuario = $this->loadUser(Yii::app()->user->id);
      return intval($usuario->rol->id) == 4;
    }
    return false;
  }
 
  // esta funcion retorna el tipo_usuario del rol del usuario
  // se accede a esta funcion llamando a Yii::app()->user->getRol()
  function getRol(){ 
    if (isset(Yii::app()->user->id))
    {   
      $usuario = $this->loadUser(Yii::app()->user->id);
      return $usuario->rol->tipo_usuario;
    }
    return false;
  }
 
// chequea el tipo de usuario y devuelve true o false
  // esta funcion retorna el tipo_usuario del rol del usuario
    function checkRol($tipo_usuario=""){ 
      if (isset(Yii::app()->user->id))
      {   
        $usuario = $this->loadUser(Yii::app()->user->id);
        return $usuario->rol->tipo_usuario==$tipo_usuario;
      }
      return false;
  }
  
  // Load user model.
  protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=Usuario::model()->find('LOWER(usuario)=?',array($id));
        }
        return $this->_model;
    }
}
?>