<?php

/**
 * This is the model class for table "trabajo".
 *
 * The followings are the available columns in table 'trabajo':
 * @property string $id
 * @property string $numero
 * @property string $inspecciones_id
 * @property string $usuario_id
 * @property string $fecha
 * @property string $fecha_fin
 * @property string $denominacion
 * @property string $direccion
 * @property string $problematica
 * @property string $descripcion
 * @property string $tipo_reparacion
 * @property string $beneficiario
 * @property string $observaciones
 * @property double $suministro
 * @property double $transporte
 * @property double $colocacion
 * @property double $inversion
 * @property integer $realizada
 *
 * The followings are the available model relations:
 * @property DetalleMaquinaria[] $detalleMaquinarias
 * @property DetallePersonal[] $detallePersonals
 * @property DetalleTrabajo[] $detalleTrabajos
 * @property FotoTrabajo[] $fotoTrabajos
 * @property PapeletaAsfalto[] $papeletaAsfaltos
 * @property Inspecciones $inspecciones
 * @property Usuario $usuario
 */
class Trabajo extends CActiveRecord
{
	public $inspeccion_search;
	public $proveedor_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trabajo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero, inspecciones_id, usuario_id', 'required'),
			array('realizada', 'numerical', 'integerOnly'=>true),
			array('inversion, suministro, transporte, colocacion', 'numerical'),
			array('numero, inspecciones_id, usuario_id', 'length', 'max'=>10),
			array('beneficiario, problematica,tipo_reparacion, descripcion, direccion', 'length', 'max'=>200),
			array('observaciones, denominacion', 'length', 'max'=>200),
			array('fecha, fecha_fin', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, problematica,tipo_reparacion, descripcion, direccion, inspecciones_id, inspeccion_search, proveedor_search, usuario_usuario, fecha, fecha_fin, denominacion, beneficiario, observaciones, inversion, realizada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detalleMaquinarias' => array(self::HAS_MANY, 'DetalleMaquinaria', 'trabajo_id'),
			'detallePersonals' => array(self::HAS_MANY, 'DetallePersonal', 'trabajo_id'),
			'detalleTrabajos' => array(self::HAS_MANY, 'DetalleTrabajo', 'trabajo_id'),
			'fotoTrabajos' => array(self::HAS_MANY, 'FotoTrabajo', 'trabajo_id'),
			'papeletaAsfaltos' => array(self::HAS_MANY, 'PapeletaAsfalto', 'trabajo_id'),
			'inspecciones' => array(self::BELONGS_TO, 'Inspeccion', 'inspecciones_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Num. de Obra',
			'inspecciones_id' => 'Num. Inspecci&oacute;n',
			'usuario' => 'Realizado por',
			'fecha' => 'Fecha de Inicio',
			'fecha_fin' => 'Fecha de Culminaci&oacute;n',
			'denominacion' => 'Denominaci&oacute;n',
			'direccion' => 'Direcci&oacute;n',
			'problematica'=>'Problem&aacute;tica',
			'tipo_reparacion'=>'Tipo de Reparaci&oacute;n',
			'descripcion'=>'Descripci&oacute;n',
			'beneficiario' => 'Beneficiario',
			'observaciones' => 'Observaciones',
			'inversion' => 'Inversi&oacute;n',
			'suministro' => 'Suministro',
			'transporte' => 'Transporte',
			'colocacion' => 'Colocaci&oacute;n',
			'realizada' => 'Realizada',
			'inspeccion_search' => 'Num. de Inspecci&oacute;n',
			'proveedor_search' => 'Proveedor',
		);
	}

	protected function beforeSave(){
		if (isset($this->fecha) ){	
				if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches)){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
				if(preg_match('#(\d{2})/(\d{2})/(\d{4})#', $this->fecha, $Matches)){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
		}

		//if(isset($this->suministro) && isset($this->transporte) && isset($this->colocacion)){
		//	$this->inversion=$this->suministro + $this->transporte + $this->colocacion;
		//}

		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		//Borrar datos asociados
		$objAsociado=DetalleTrabajo::model()->deleteAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		$objAsociado=DetalleMaquinaria::model()->deleteAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		$objAsociado=DetallePersonal::model()->deleteAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		$objAsociado=PapeletaAsfalto::model()->deleteAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		$objAsociado=Presupuesto::model()->deleteAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		$objAsociado=FotoTrabajo::model()->findAll(array('condition'=>'trabajo_id=:trabajoId',
   														'params'=>array(':trabajoId'=>$this->id)));
		foreach ($objAsociado as $objFoto ) {
			unlink(Yii::getPathOfAlias('webroot')."/".$objFoto->ruta);
			$objFoto->delete();
		}
		// fin de borrar datos asociados

		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('inspecciones');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('inspecciones.numero',$this->inspeccion_search,true);
		$criteria->compare('usuario_id',$this->usuario_id,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('fecha_fin',$this->fecha_fin,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('denominacion',$this->denominacion,true);
		$criteria->compare('problematica',$this->problematica,true);
		$criteria->compare('tipo_reparacion',$this->tipo_reparacion,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('beneficiario',$this->beneficiario,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('inversion',$this->inversion);
		$criteria->compare('t.realizada',$this->realizada);

		return new CActiveDataProvider('Trabajo', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.fecha DESC',
				'attributes'=>array(
					'inspeccion_search'=>array(
						'asc'=>'inspecciones.numero',
						'desc'=>'inspecciones.numero DESC',
					),
					'*',
				),
			),
		));
	}

	public function getMaquinaria(){
		$criteria=new CDbCriteria;
		$criteria->compare('trabajo_id',$this->id,false);	
		return new CActiveDataProvider('DetalleMaquinaria', array(
			'criteria'=>$criteria,
		));
	}

	public function getPersonal(){
		$criteria=new CDbCriteria;
		$criteria->compare('trabajo_id',$this->id,false);	
		return new CActiveDataProvider('DetallePersonal', array(
			'criteria'=>$criteria,
		));
	}

	public function getComputos(){
		$criteria=new CDbCriteria;
		$criteria->compare('trabajo_id',$this->id,false);	
		return new CActiveDataProvider('DetalleTrabajo', array(
			'criteria'=>$criteria,
		));
	}

	public function getPapeletas(){
		$criteria=new CDbCriteria;
		$criteria->compare('trabajo_id',$this->id,false);	
		return new CActiveDataProvider('PapeletaAsfalto', array(
			'criteria'=>$criteria,
		));
	}

	public function getFotos(){
		$criteria=new CDbCriteria;
		$criteria->compare('trabajo_id',$this->id,false);	
		return FotoTrabajo::model()->findAll($criteria);
	}

	public function getCantidadAnual(){
		$obj=$this->findBySql("SELECT COUNT(realizada) AS realizada FROM trabajo WHERE YEAR(fecha)=YEAR(NOW()) AND realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}

	public function getCantidadRango($fechaMin, $fechaMax){
		$obj=$this->findBySql("SELECT COUNT(realizada) AS realizada FROM trabajo WHERE '$fechaMin'<=fecha AND fecha<='$fechaMax' AND realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}

	public function getInversionAdmRango($fechaMin, $fechaMax){
		$obj=$this->findBySql("SELECT SUM(t.inversion) AS realizada FROM trabajo t JOIN inspecciones i ON t.inspecciones_id=i.id WHERE i.tipo_adm='ADMINISTRACION DIRECTA' AND '$fechaMin'<=t.fecha AND t.fecha<='$fechaMax' AND t.realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}

	public function getInversionContRango($fechaMin, $fechaMax){
		$obj=$this->findBySql("SELECT SUM(t.inversion) AS realizada FROM trabajo t JOIN inspecciones i ON t.inspecciones_id=i.id WHERE i.tipo_adm='OBRA CONTRATADA' AND '$fechaMin'<=t.fecha AND t.fecha<='$fechaMax' AND t.realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trabajo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
