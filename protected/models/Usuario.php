<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property string $id
 * @property string $usuario
 * @property string $rol_id
 * @property string $pass
 * @property string $cedula
 * @property string $nombres
 * @property string $apellidos
 * @property string $civ
 * @property string $token
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Inspecciones[] $inspecciones
 * @property Trabajo[] $trabajos
 * @property Rol $rol
 */
class Usuario extends CActiveRecord
{
	public $tipo_search;
	private $_oldValues=[];
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario, rol_id', 'required'),
			array('id, rol_id', 'length', 'max'=>10),
			array('usuario', 'length', 'max'=>15),
			array('pass,token', 'length', 'max'=>80),
			array('cedula,civ', 'length', 'max'=>11),
			array('nombres, apellidos', 'length', 'max'=>50),
			array('pass', 'required', 'on'=>'insert'),
			array('usuario','unique'),
			array('date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, usuario, rol_id, pass, cedula, civ, nombres, apellidos, tipo_search, token, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inspecciones' => array(self::HAS_MANY, 'Inspecciones', 'usuario_id'),
			'trabajos' => array(self::HAS_MANY, 'Trabajo', 'usuario_id'),
			'rol' => array(self::BELONGS_TO, 'Rol', 'rol_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usuario' => 'Usuario',
			'rol_id' => 'Rol',
			'pass' => 'Contrase&ntilde;a',
			'cedula' => 'C&eacute;dula',
			'civ' => 'CIV',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'tipo_search'=> 'Tipo de Usuario',
			'date'=>'Expiraci&oacute;n de token',
			'Token'=>'Token'
		);
	}
	
	protected function beforeSave(){
		if ( $this->isNewRecord || $this->pass!=''){
			$this->pass=md5($this->pass);
		}
		else{
			$this->pass=$this->_oldValues['pass'];
		}
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=$this->usuario;
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		
		$bit->save();
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=$this->usuario;
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}
	
	public function validarPassword($pass){
		return $this->_oldValues['pass']==md5($pass);
	}

	public function afterFind(){
		$this->_oldValues=$this->attributes;
		$this->pass='';
		
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('rol');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('rol.tipo_usuario',$this->tipo_search,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('civ',$this->civ,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('token',$this->token,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider('Usuario', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'tipo_search'=>array(
						'asc'=>'rol.tipo_usuario',
						'desc'=>'rol.tipo_usuario DESC',
					),
					'*',
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
