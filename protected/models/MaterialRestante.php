<?php

/**
 * This is the model class for table "material_restante".
 *
 * The followings are the available columns in table 'material_restante':
 * @property string $id
 * @property string $proveedor_id
 * @property string $cantidad
 *
 * The followings are the available model relations:
 * @property Proveedor $proveedor
 */
class MaterialRestante extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material_restante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proveedor_id', 'required'),
			array('proveedor_id, cantidad', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, proveedor_id, cantidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'proveedor_id' => 'Proveedor',
			'cantidad' => 'Cantidad',
		);
	}

	protected function beforeSave(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();

		return parent::beforeSave();
	}
	
	protected function beforeDelete(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('proveedor_id',$this->proveedor_id,true);
		$criteria->compare('cantidad',$this->cantidad,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaterialRestante the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
