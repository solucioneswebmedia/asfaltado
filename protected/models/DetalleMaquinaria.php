<?php

/**
 * This is the model class for table "detalle_maquinaria".
 *
 * The followings are the available columns in table 'detalle_maquinaria':
 * @property string $id
 * @property string $unidades_id
 * @property string $trabajo_id
 * @property string $maquinaria_existente_id
 * @property string $dia_trabajo
 * @property double $extra
 *
 * The followings are the available model relations:
 * @property MaquinariaExistente $maquinariaExistente
 * @property Trabajo $trabajo
 * @property Unidades $unidades
 */
class DetalleMaquinaria extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detalle_maquinaria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('maquinaria_existente_id', 'required'),
			array('extra', 'numerical'),
			array('unidades_id, trabajo_id, maquinaria_existente_id', 'length', 'max'=>10),
			array('dia_trabajo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, unidades_id, trabajo_id, maquinaria_existente_id, dia_trabajo, extra', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'maquinariaExistente' => array(self::BELONGS_TO, 'MaquinariaExistente', 'maquinaria_existente_id'),
			'trabajo' => array(self::BELONGS_TO, 'Trabajo', 'trabajo_id'),
			'unidades' => array(self::BELONGS_TO, 'Unidades', 'unidades_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unidades_id' => 'Unidad extra',
			'trabajo_id' => 'Num. de Trabajo',
			'maquinaria_existente_id' => 'Descripci&oacute;n',
			'dia_trabajo' => 'Dia de Trabajo',
			'extra' => 'Cant. de Trabajo',
		);
	}

	protected function beforeSave(){
		if (isset($this->dia_trabajo) ){
				if(preg_match('#(\d{2})\/(\d{2})\/(\d{4})#', $this->dia_trabajo, $Matches) ){
					$this->dia_trabajo=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
		}
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('unidades_id',$this->unidades_id,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('maquinaria_existente_id',$this->maquinaria_existente_id,true);
		$criteria->compare('dia_trabajo',$this->dia_trabajo,true);
		$criteria->compare('extra',$this->extra);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleMaquinaria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
