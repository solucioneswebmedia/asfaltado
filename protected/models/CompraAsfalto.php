<?php

/**
 * This is the model class for table "compra_asfalto".
 *
 * The followings are the available columns in table 'compra_asfalto':
 * @property string $id
 * @property string $proveedor_id
 * @property string $fecha
 * @property double $cantidad
 *
 * The followings are the available model relations:
 * @property Proveedor $proveedor
 */
class CompraAsfalto extends CActiveRecord
{
	private $oldCantidad;
	public $proveedor_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compra_asfalto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proveedor_id, fecha, cantidad', 'required'),
			array('cantidad', 'numerical'),
			array('proveedor_id', 'length', 'max'=>10),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, proveedor_id, fecha, cantidad, proveedor_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'proveedor_id' => 'Proveedor',
			'fecha' => 'Fecha',
			'cantidad' => 'Cantidad',
			'proveedor_search' => 'Proveedor',
		);
	}

	protected function afterFind(){
		$this->oldCantidad=$this->cantidad;
		return parent::afterFind();
	}

	protected function beforeSave(){
		if (isset($this->fecha) ){	
				preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches);
				$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
		}
		if($this->isNewRecord){
			$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
			if($objMaterial!=null){
				$newValue=floatval($objMaterial->cantidad)+floatval($this->cantidad);
				$objMaterial->cantidad=$newValue;
				$objMaterial->save();
			}
		}else if($this->cantidad!=$this->oldCantidad){
			$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
			if($objMaterial!=null){
				$diff=floatval($this->cantidad)-floatval($this->oldCantidad);
				$newValue=floatval($objMaterial->cantidad)+$diff;
				$objMaterial->cantidad=$newValue;
				$objMaterial->save();
			}
		}
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
		if($objMaterial!=null){
			$newValue=floatval($objMaterial->cantidad)-floatval($this->cantidad);
			$objMaterial->cantidad=$newValue;
			$objMaterial->save();
		}

		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('proveedor');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('proveedor.nombre',$this->proveedor_search,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('cantidad',$this->cantidad);

		return new CActiveDataProvider('CompraAsfalto', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'proveedor_search'=>array(
						'asc'=>'proveedor.nombre',
						'desc'=>'proveedor.nombre DESC',
					),
					'*',
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompraAsfalto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
