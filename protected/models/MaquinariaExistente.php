<?php

/**
 * This is the model class for table "maquinaria_existente".
 *
 * The followings are the available columns in table 'maquinaria_existente':
 * @property string $id
 * @property string $unidades_id
 * @property string $codigo
 * @property string $descripcion
 * @property double $precio
 * @property integer $extra
 *
 * The followings are the available model relations:
 * @property DetalleMaquinaria[] $detalleMaquinarias
 * @property Unidades $unidades
 */
class MaquinariaExistente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'maquinaria_existente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unidades_id, codigo, descripcion, precio', 'required'),
			array('extra', 'numerical', 'integerOnly'=>true),
			array('precio', 'numerical'),
			array('unidades_id', 'length', 'max'=>10),
			array('codigo', 'length', 'max'=>20),
			array('descripcion', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, unidades_id, codigo, descripcion, precio, extra', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detalleMaquinarias' => array(self::HAS_MANY, 'DetalleMaquinaria', 'maquinaria_existente_id'),
			'unidades' => array(self::BELONGS_TO, 'Unidad', 'unidades_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'unidades_id' => 'Unidades',
			'codigo' => 'C&oacute;digo',
			'descripcion' => 'Descripci&oacute;n',
			'precio' => 'Precio',
			'extra' => 'Extra',
		);
	}

	protected function beforeSave(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();

		return parent::beforeSave();
	}
	
	protected function beforeDelete(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('unidades_id',$this->unidades_id,true);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('extra',$this->extra);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MaquinariaExistente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
