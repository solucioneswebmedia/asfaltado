<?php

/**
 * This is the model class for table "solicitud_inspeccion".
 *
 * The followings are the available columns in table 'solicitud_inspeccion':
 * @property string $id
 * @property string $numero
 * @property string $nombre_solicitante
 * @property string $direccion
 * @property string $fecha
 * @property string $fecha_registro
 * @property string $telefono_contacto
 * @property string $procedencia
 * @property integer $semana
 * @property string $prioridad
 * @property integer $realizada
 *
 * The followings are the available model relations:
 * @property Inspecciones[] $inspecciones
 */
class SolicitudInspeccion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'solicitud_inspeccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero', 'required'),
			array('realizada, semana', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>10),
			array('nombre_solicitante', 'length', 'max'=>30),
			array('direccion', 'length', 'max'=>200),
			array('telefono_contacto', 'length', 'max'=>16),
			array('fecha, procedencia, prioridad, fecha_registro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, nombre_solicitante, direccion, fecha, fecha_registro, telefono_contacto, procedencia, semana, prioridad, realizada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inspecciones' => array(self::HAS_MANY, 'Inspecciones', 'solicitud_inspeccion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'numero' => 'Numero',
			'nombre_solicitante' => 'Nombre del Solicitante',
			'direccion' => 'Direcci&oacute;n',
			'fecha' => 'Fecha',
			'telefono_contacto' => 'Tel&eacute;fono de Cont&aacute;cto',
			'procedencia' => 'C&oacute;digo de Procedencia',
			'semana' => 'Semana',
			'prioridad' => 'Fecha programada',
			'realizada' => 'Realizada',
		);
	}

	protected function beforeSave(){
		if (isset($this->fecha) ){	
				if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches)){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
		}
		
		//log
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		//end log
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('nombre_solicitante',$this->nombre_solicitante,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('telefono_contacto',$this->telefono_contacto,true);
		$criteria->compare('procedencia',$this->procedencia,true);
		$criteria->compare('semana',$this->semana,true);
		//$criteria->compare('prioridad',$this->prioridad,true);
		//$criteria->compare('realizada',$this->realizada,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'fecha DESC',
				)
		));
	}

	public function getSolicitudesNuevas()
	{
		if($this->fecha!=null){
			preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches);
			$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
		}
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('procedencia',$this->procedencia,true);
		//$criteria->compare('semana',$this->semana,true);
		//$criteria->compare('prioridad',$this->prioridad,true);
		$criteria->compare('realizada',$this->realizada,true);
		if($this->fecha!=null){
			preg_match('#(\d{4})-(\d{2})-(\d{2})#', $this->fecha, $Matches);
			$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'fecha DESC',
				)
		));
	}
	
	public function getSemanaMinima(){
		return $this->findBySql('SELECT * FROM solicitud_inspeccion WHERE semana>=0 AND realizada=0 ORDER BY semana ASC LIMIT 1');
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SolicitudInspeccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
