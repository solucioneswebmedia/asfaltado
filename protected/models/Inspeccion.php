<?php

/**
 * This is the model class for table "inspecciones".
 *
 * The followings are the available columns in table 'inspecciones':
 * @property string $id
 * @property string $numero
 * @property string $solicitud_inspeccion_id
 * @property string $usuario_id
 * @property string $asunto
 * @property string $problematica
 * @property string $parroquia
 * @property string $fecha
 * @property string $tipo_obra
 * @property string $descripcion
 * @property string $recomendaciones
 * @property string $pto_ref
 * @property string $material_bacheo
 * @property string $croquis
 * @property string $tipo_adm
 * @property string $latitud
 * @property string $longitud
 * @property string $semana
 * @property string $cuadrilla
 * @property integer $realizada
 *
 * The followings are the available model relations:
 * @property ComputoMetrico[] $computoMetricos
 * @property EstimacionMaquinaria[] $estimacionMaquinarias
 * @property EstimacionPersonal[] $estimacionPersonals
 * @property FotoInspeccion[] $fotoInspeccions
 * @property SolicitudInspeccion $solicitudInspeccion
 * @property Usuario $usuario
 * @property Trabajo[] $trabajos
 */
class Inspeccion extends CActiveRecord
{
	public $solicitud_search;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inspecciones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('numero', 'required'),
			array('realizada', 'numerical', 'integerOnly'=>true),
			array('id, numero, solicitud_inspeccion_id, semana, usuario_id', 'length', 'max'=>10),
			array('parroquia, tipo_adm, cuadrilla', 'length', 'max'=>30),
			array('tipo_obra, material_bacheo', 'length', 'max'=>50),
			array('descripcion, recomendaciones', 'length', 'max'=>200),
			array('pto_ref, croquis, latitud, longitud', 'length', 'max'=>100),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero, solicitud_inspeccion_id, usuario_id, parroquia, fecha, tipo_obra, descripcion, recomendaciones, pto_ref, material_bacheo,  croquis, tipo_adm, latitud, longitud, semana, cuadrilla, realizada, solicitud_search', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'computoMetricos' => array(self::HAS_MANY, 'ComputoMetrico', 'inspecciones_id'),
			'estimacionMaquinarias' => array(self::HAS_MANY, 'EstimacionMaquinaria', 'inspecciones_id'),
			'estimacionPersonals' => array(self::HAS_MANY, 'EstimacionPersonal', 'inspecciones_id'),
			'fotoInspeccions' => array(self::HAS_MANY, 'FotoInspeccion', 'inspecciones_id'),
			'solicitudInspeccion' => array(self::BELONGS_TO, 'SolicitudInspeccion', 'solicitud_inspeccion_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
			'trabajos' => array(self::HAS_MANY, 'Trabajo', 'inspecciones_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero' => 'Inspecci&oacute;n',
			'solicitud_inspeccion_id' => 'Numero',
			'usuario_id' => 'Usuario',
			'parroquia' => 'Parroquia',
			'fecha' => 'Fecha',
			'tipo_obra' => 'Tipo de Obra',
			'descripcion' => 'Descripci&oacute;n',
			'recomendaciones' => 'Recomendaciones',
			'pto_ref' => 'Punto de Ref.',
			'material_bacheo' => 'Material Bacheo',
			'croquis' => 'Croquis',
			'tipo_adm' => 'Tipo Administraci&oacute;n',
			'latitud' => 'Latitud',
			'longitud' => 'Longitud',
			'semana' => 'Semana',
			'cuadrilla' => 'Cuadrilla',
			'realizada' => 'Realizada',
			'solicitud_search' => 'Solicitud',
		);
	}
	
	protected function beforeSave(){

		if (isset($this->fecha) ){	
				if(preg_match('#(\d{2})\/(\d{2})\/(\d{4})#', $this->fecha, $Matches) ){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}else
				if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches) ){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
		}
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeSave();
	}

	protected function beforeDelete(){
		//Borrar datos asociados
		$objAsociado=Trabajo::model()->find(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		if($objAsociado!=null){
			var_dump("No se puede eliminar inspección porque tiene una obra asociada.");
			exit();
		}
		$objAsociado=ComputoMetrico::model()->deleteAll(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		$objAsociado=EstimacionMaquinaria::model()->deleteAll(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		$objAsociado=EstimacionPersonal::model()->deleteAll(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		$objAsociado=DiaInspeccion::model()->deleteAll(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		$objAsociado=FotoInspeccion::model()->findAll(array('condition'=>'inspecciones_id=:inspeccionesId',
   														'params'=>array(':inspeccionesId'=>$this->id)));
		foreach ($objAsociado as $objFoto ) {
			unlink(Yii::getPathOfAlias('webroot')."/".$objFoto->ruta);
			$objFoto->delete();
		}

		$objAsociado=SolicitudInspeccion::model()->find(array('condition'=>'id=:solicitudId',
   														'params'=>array(':solicitudId'=>$this->solicitud_inspeccion_id)));
		if($objAsociado!=null){
			$objAsociado->realizada=0;
			$objAsociado->save();
		}
		// fin de borrar datos asociados

		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('solicitudInspeccion');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('numero',$this->numero,true);
		//$criteria->compare('solicitud_inspeccion_id',$this->solicitud_inspeccion_id,true);
		$criteria->compare('solicitudInspeccion.numero',$this->solicitud_search,true);
		$criteria->compare('usuario_id',$this->usuario_id,true);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('tipo_obra',$this->tipo_obra,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('recomendaciones',$this->recomendaciones,true);
		$criteria->compare('pto_ref',$this->pto_ref,true);
		$criteria->compare('material_bacheo',$this->material_bacheo,true);
		$criteria->compare('croquis',$this->croquis,true);
		$criteria->compare('tipo_adm',$this->tipo_adm,true);
		$criteria->compare('latitud',$this->latitud,true);
		$criteria->compare('longitud',$this->longitud,true);
		$criteria->compare('semana',$this->semana,true);
		$criteria->compare('cuadrilla',$this->cuadrilla,true);
		$criteria->compare('realizada',$this->realizada);

		return new CActiveDataProvider('Inspeccion', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'t.fecha DESC',
				'attributes'=>array(
					'solicitud_search'=>array(
						'asc'=>'solicitudInspeccion.numero',
						'desc'=>'solicitudInspeccion.numero DESC',
					),
					'*',
				),
			),
		));
	}

	public function getInspeccionesNuevas()
	{
		if($this->fecha!=null){
			preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches);
			$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
		}
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;

		$criteria->compare('numero',$this->numero,true);
		//$criteria->compare('solicitud_inspeccion_id',$this->solicitud_inspeccion_id,true);
		$criteria->compare('solicitudInspeccion.numero',$this->solicitud_search,true);
		$criteria->compare('usuario_id',$this->usuario_id,true);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('tipo_obra',$this->tipo_obra,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('recomendaciones',$this->recomendaciones,true);
		$criteria->compare('pto_ref',$this->pto_ref,true);
		$criteria->compare('material_bacheo',$this->material_bacheo,true);
		$criteria->compare('croquis',$this->croquis,true);
		$criteria->compare('tipo_adm',$this->tipo_adm,true);
		$criteria->compare('latitud',$this->latitud,true);
		$criteria->compare('longitud',$this->longitud,true);
		//$criteria->compare('semana',$this->semana,true);
		$criteria->compare('realizada',$this->realizada);
		if($this->fecha!=null){
			preg_match('#(\d{4})-(\d{2})-(\d{2})#', $this->fecha, $Matches);
			$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'fecha DESC',
				)
		));
	}


	public function getMaquinaria(){
		$criteria=new CDbCriteria;
		$criteria->compare('inspecciones_id',$this->id,false);	
		return new CActiveDataProvider('EstimacionMaquinaria', array(
			'criteria'=>$criteria,
		));
	}

	public function getPersonal(){
		$criteria=new CDbCriteria;
		$criteria->compare('inspecciones_id',$this->id,false);	
		return new CActiveDataProvider('EstimacionPersonal', array(
			'criteria'=>$criteria,
		));
	}

	public function getComputos(){
		$criteria=new CDbCriteria;
		$criteria->compare('inspecciones_id',$this->id,false);	
		return new CActiveDataProvider('ComputoMetrico', array(
			'criteria'=>$criteria,
		));
	}

	public function getDias(){
		$criteria=new CDbCriteria;
		$criteria->compare('inspecciones_id',$this->id,false);	
		return new CActiveDataProvider('DiaInspeccion', array(
			'criteria'=>$criteria,
		));
	}

	public function getFotos(){
		$criteria=new CDbCriteria;
		$criteria->compare('inspecciones_id',$this->id,false);	
		return FotoInspeccion::model()->findAll($criteria);
	}

	public function getSemanaMinima(){
		return $this->findBySql('SELECT * FROM inspecciones WHERE semana>=0 AND realizada=0 ORDER BY semana ASC LIMIT 1');
	}

	public function getCantidadAnual(){
		$obj=$this->findBySql("SELECT COUNT(realizada) AS realizada FROM inspecciones WHERE YEAR(fecha)=YEAR(NOW()) AND realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}

	public function getCantidadRango($fechaMin, $fechaMax){
		$obj=$this->findBySql("SELECT COUNT(realizada) AS realizada FROM inspecciones WHERE '$fechaMin'<=fecha AND fecha<='$fechaMax' AND realizada=1");
		if($obj->realizada!=null){
			return $obj->realizada;
		}else{
			return 0;
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inspeccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
