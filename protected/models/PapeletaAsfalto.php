<?php

/**
 * This is the model class for table "papeleta_asfalto".
 *
 * The followings are the available columns in table 'papeleta_asfalto':
 * @property string $id
 * @property string $trabajo_id
 * @property string $proveedor_id
 * @property string $codigo
 * @property string $fecha
 * @property double $cantidad
 * @property string $procedencia
 *
 * The followings are the available model relations:
 * @property Trabajo $trabajo
 * @property Proveedor $proveedor
 */
class PapeletaAsfalto extends CActiveRecord
{
	public $trabajo_search;
	public $proveedor_search;
	private $oldCantidad;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'papeleta_asfalto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trabajo_id, proveedor_id, codigo', 'required'),
			array('cantidad', 'numerical'),
			array('trabajo_id, proveedor_id, codigo', 'length', 'max'=>10),
			array('procedencia', 'length', 'max'=>5),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, trabajo_search, proveedor_search, trabajo_id, proveedor_id, codigo, fecha, cantidad, procedencia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trabajo' => array(self::BELONGS_TO, 'Trabajo', 'trabajo_id'),
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trabajo_id' => 'Num. de Trabajo',
			'proveedor_id' => 'Proveedor',
			'codigo' => 'C&oacute;digo',
			'fecha' => 'Fecha',
			'cantidad' => 'Cantidad de Asfalto',
			'trabajo_search' => 'Num. de Trabajo',
			'proveedor_search' => 'Proveedor',
			'procedencia'=>'Procedencia',
		);
	}

	protected function beforeSave(){
		if (isset($this->fecha) ){
				if(preg_match('#(\d{2})\/(\d{2})\/(\d{4})#', $this->fecha, $Matches) ){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}else if(preg_match('#(\d{2})-(\d{2})-(\d{4})#', $this->fecha, $Matches) ){
					$this->fecha=$Matches[3]."-".$Matches[2]."-".$Matches[1];
				}
		}
		if($this->isNewRecord){
			$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
			if($objMaterial!=null){
				$newValue=floatval($objMaterial->cantidad)-floatval($this->cantidad);
				$objMaterial->cantidad=$newValue;
				$objMaterial->save();
			}
		}else if($this->cantidad!=$this->oldCantidad){
			$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
			if($objMaterial!=null){
				$diff=floatval($this->cantidad)-floatval($this->oldCantidad);
				$newValue=floatval($objMaterial->cantidad)-$diff;
				$objMaterial->cantidad=$newValue;
				$objMaterial->save();
			}
		}
		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo=($this->isNewRecord)?"Insercion":"Modificacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeSave();
	}

	protected function afterFind(){
		$this->oldCantidad=$this->cantidad;
		return parent::afterFind();
	}

	protected function beforeDelete(){
		$objMaterial=MaterialRestante::model()->find("proveedor_id=$this->proveedor_id");
		if($objMaterial!=null){
			$newValue=floatval($objMaterial->cantidad)+floatval($this->cantidad);
			$objMaterial->cantidad=$newValue;
			$objMaterial->save();
		}

		$bit=new Bitacora;
		$bit->tabla=$this->tableName();
		$bit->registro=implode(";", $this->attributes);
		$bit->usuario=Yii::app()->user->id;
		$bit->tipo="Eliminacion";
		$bit->timestamp=date('Y-m-d H:i:s');
		$bit->save();
		return parent::beforeDelete();
	}

	public function checkProcedencia($value){
		if ($value==1)
			return 'Asfalto Propio';
		else if($value==2)
			return 'Convenio IVT';
		else if($value==3)
			return 'Convenio Comercial';
		else if($value==3)
			return 'Convenio Ciudadano';
		else
			return null;
	}

	public function getCantidadMensual($value){
		$sql="SELECT SUM(cantidad) AS cantidad FROM papeleta_asfalto WHERE MONTH(fecha)=$value AND YEAR(fecha)=YEAR(NOW())";
		$obj=$this->findBySql($sql);
		if($obj->cantidad!=null){
			return $obj->cantidad;
		}else{
			return 0;
		}
	}

	public function getCantidadAnual(){
		$obj=$this->findBySql("SELECT SUM(cantidad) AS cantidad FROM papeleta_asfalto WHERE YEAR(fecha)=YEAR(NOW())");
		if($obj->cantidad!=null){
			return $obj->cantidad;
		}else{
			return 0;
		}
	}

	public function getCantidadRango($fechaMin, $fechaMax){
		$obj=$this->findBySql("SELECT SUM(cantidad) AS cantidad FROM papeleta_asfalto WHERE '$fechaMin'<=fecha AND fecha<='$fechaMax'");
		if($obj->cantidad!=null){
			return $obj->cantidad;
		}else{
			return 0;
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('trabajo','proveedor');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,false);
		$criteria->compare('proveedor_id',$this->proveedor_id,true);
		$criteria->compare('trabajo.numero',$this->trabajo_search,true);
		$criteria->compare('proveedor.nombre',$this->proveedor_search,true);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('t.fecha',$this->fecha,true);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('procedencia',$this->procedencia);

		return new CActiveDataProvider('PapeletaAsfalto', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'attributes'=>array(
					'trabajo_search'=>array(
						'asc'=>'trabajo.numero',
						'desc'=>'trabajo.numero DESC',
					),
					'proveedor_search'=>array(
						'asc'=>'proveedor.nombre',
						'desc'=>'proveedor.nombre DESC',
					),
					'*',
				),
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PapeletaAsfalto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
